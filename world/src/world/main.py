import subprocess
import sys

import terminaltables  # type: ignore

from .world import date_command, get_timezone

default_zones = [
    "America/Los_Angeles",
    "Europe/Budapest",
    "Europe/Helsinki",
    "Europe/Zurich",
    "UTC",
]

if sys.stdout.isatty():
    TableFactory = terminaltables.SingleTable
else:
    TableFactory = terminaltables.GithubFlavoredMarkdownTable


def _convert_timezone(timestamp, tz=get_timezone()):
    return (
        subprocess.run(
            ["date", f'--date=TZ="{tz}" {timestamp}'],
            stdout=subprocess.PIPE,
        )
        .stdout.decode()
        .strip()
    )


def get_all_timezones():
    return (
        subprocess.run(["timedatectl", "list-timezones"], stdout=subprocess.PIPE)
        .stdout.decode()
        .splitlines()
    )


def main():
    timestamp = None
    zones = []
    if args := sys.argv[1:]:
        zones = []
        for timezone_name in get_all_timezones():
            zones.extend(timezone_name for text in args if text in timezone_name)
        if timestamps := [text for text in args if " " in text]:
            timestamp = _convert_timezone(timestamps[-1])
    zones = zones or default_zones
    if timestamp:
        zones = [get_timezone(), *zones]
    table = TableFactory(
        [("Timezone", "local time")]
        + [(name, date_command(timestamp=timestamp, tz=name)) for name in zones],
    )
    print(table.table)
