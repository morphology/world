import subprocess


def get_timedatectl_properties(*names):
    timedatectl_args = [f"--property={name}" for name in names] if names else ["--all"]
    properties = {}
    for line in (
        subprocess.run(
            ["timedatectl", "show", *timedatectl_args],
            stdout=subprocess.PIPE,
        )
        .stdout.decode()
        .splitlines()
    ):
        k, _, v = line.partition("=")
        properties[k.strip()] = v.strip()
    return properties


def get_timezone():
    return get_timedatectl_properties("Timezone")["Timezone"]


def date_command(timestamp=None, tz=None):
    # Only pass tz if timestamp is timezone-unaware
    date_args = []
    if timestamp:
        date_args.append(f"--date={timestamp}")
    return (
        subprocess.run(
            ["date", *date_args],
            env={"TZ": str(tz)} if tz else {},
            stdout=subprocess.PIPE,
        )
        .stdout.decode()
        .strip()
    )
