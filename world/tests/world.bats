load 'lib/bats-support/load'
load 'lib/bats-assert/load'

deps="date timedatectl"
target="python -m world"

@test "Should take an argument for timezone arithmetic" {
  type $deps || skip "Unsupported platform"
  run $target '00:00 UTC'
  assert_success
  assert_line --partial '00:00 UTC'
}
