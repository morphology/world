#! /bin/sh
docker run --gpus all --init \
    -v "/etc/localtime:/etc/localtime" \
    -v "/Windows_Home:/Windows_Home" \
    -v "huggingface_cache:/root/.cache/huggingface" \
    -v "pip_cache:/root/.cache/pip" \
    -v "uv_cache:/root/.cache/uv" \
    -P "$@" \
    python3 -u src/stable-diffusion-webui/launch.py \
    --allow-code --api --api-server-stop --listen --theme=dark --xformers \
    --medvram-sdxl
