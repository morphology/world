# Install directory without trailing slash
install_dir="/usr/local"
if [ -n "$CONDA_PREFIX" ]; then
	export VIRTUAL_ENV="$CONDA_PREFIX"
	export venv_dir="$CONDA_PREFIX"
fi
export COMMANDLINE_ARGS="--medvram --listen --api --xformers --enable-insecure-extension-access"
