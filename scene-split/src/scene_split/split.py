import argparse
import csv
from datetime import datetime
import logging
import re
import shutil
import subprocess
import sys
import tempfile
from collections import defaultdict
from dataclasses import dataclass, astuple, asdict
from pathlib import Path

from modernize_images.ffprobe import ffprobe
from rich.progress import Progress

from . import ffmpeg
from . import lightworks
from ._version import __version__
from .screencap import screencap_within_ffmpeg_call

logger = logging.getLogger(__name__)


def _split_video(
    video_path, media_info, screencap_path, splits, tmpdir, verbose=__debug__
):
    screencap_path = Path(screencap_path)
    tmpdir = Path(tmpdir)
    (tmpdir / "segments").mkdir()

    extra_video_stream_arguments = []
    if examine_media(media_info):
        extra_video_stream_arguments += ["-codec", "copy"]
    else:
        logger.error("Transcoding: look for format errors")
    with screencap_within_ffmpeg_call(
        media_info, screencap_path, tmpdir
    ) as screencap_arguments:
        return ffmpeg.segment_video(
            video_path,
            splits,
            tmpdir / "segments",
            media_info=media_info,
            extra_ffmpeg_arguments=[
                *screencap_arguments,
                *extra_video_stream_arguments,
            ],
            verbose=verbose,
        )


def split_lightworks(
    video_path,
    lightworks_project_path,
    output_dir,
    screencap_path=None,
    verbose=__debug__,
):
    lightworks_name_re = re.compile(r"Cue Marker (\d+)")

    video_path = Path(video_path)
    output_dir = Path(output_dir)
    screencap_path = screencap_path or output_dir / f"{video_path.name}.JPEG"
    if not output_dir.is_dir():
        raise OSError(f"'{output_dir}/' does not exist")
    project = lightworks.Project(lightworks_project_path)
    media_info = ffprobe(video_path)
    fps = float(media_info.get_frame_rate())
    duration = media_info.get_duration()

    cues = list(project.use_cues(real_duration=duration.total_seconds(), real_fps=fps))
    splits = [timestamp for timestamp, _ in cues]
    assert splits.pop(0) == 0.0
    assert splits[0] > 0.0

    progress = Progress(transient=True)
    with tempfile.TemporaryDirectory(ignore_cleanup_errors=True) as tmpdir, progress:
        tmpdir = Path(tmpdir)
        produced_segments = _split_video(
            video_path, media_info, screencap_path, splits, tmpdir, verbose=verbose
        )
        assert len(cues) == len(produced_segments)

        n_errors = 0
        creation_time = media_info.get_creation_time()
        if creation_time == datetime(1970, 1, 1, 0, 0):
            creation_time = None
        for (cue_timestamp, cue_attrs), (
            (segment_start, segment_end),
            segment_attrs,
        ) in progress.track(list(zip(cues, produced_segments)), description="Moving"):
            if cue_attrs is None:
                continue
            path = segment_attrs["path"]

            if not -10 < segment_start - cue_timestamp < 10:
                logger.error(
                    f"{cue_timestamp}, {cue_attrs} differs from {segment_start}, {segment_end}, {segment_attrs}"
                )
                n_errors += 1

            dest = output_dir
            if creation_time:
                dest /= f"{creation_time:%Y%m%d}"
            label = cue_attrs.get("description") or cue_attrs.get("name").strip()
            if not lightworks_name_re.match(label):
                dest /= label.replace(" ", "_")
            dest.mkdir(parents=True, exist_ok=True)
            shutil.move(path, dest / path.name.replace(" ", "_"))
    return n_errors == 0


class HintTable:
    path = Path.home() / ".scene-split.csv"

    @dataclass
    class CueHint:
        lead_in: float
        lead_out: float
        scene_score: float
        target_scene_duration: float

        @classmethod
        def from_row(cls, row):
            row = [float(v) if v.strip() else None for v in row]
            return cls(*row)

        def to_dict(self):
            return {k: v for k, v in asdict(self).items() if v is not None}

    def __init__(self):
        self.hints = defaultdict(list)
        if not self.path.exists():
            self.path.open("w")
        self.load()

    def load(self):
        with self.path.open() as fi:
            reader = csv.reader(fi)
            for hash_entry, path, *row in reader:
                hint = self.CueHint.from_row(row)
                self.hints[hash_entry].append(hint)

    def append(self, media_info, hint=None):
        hint = hint or HintTable.CueHint(None, None, None, None)
        with self.path.open("a") as fo:
            writer = csv.writer(fo)
            writer.writerows(
                [
                    (h[1], media_info.get_path(), *astuple(hint))
                    for h in media_info.get_data_hashes()
                ]
            )

    def search(self, media_info):
        for codec_type, hash_value in media_info.get_data_hashes():
            if hash_value in self.hints:
                yield from self.hints.get(hash_value)


def _select_cues(video_path, media_info):
    video_path = Path(video_path)
    duration = media_info.get_duration()
    duration_s = duration.total_seconds()

    timestamp_attrs = [
        (location.pts_time, attrs)
        for location, attrs in ffmpeg.detect_scenes(video_path)
    ]

    blackdetect_ends = [
        float(attrs["lavfi.black_end"])
        for _, attrs in timestamp_attrs
        if "lavfi.black_end" in attrs
    ]
    blackdetect_starts = [
        float(attrs["lavfi.black_start"])
        for _, attrs in timestamp_attrs
        if "lavfi.black_start" in attrs
    ]
    logger.debug("blackdetect_ends=%r", blackdetect_ends)
    logger.debug("blackdetect_starts=%r", blackdetect_starts)

    if blackdetect_ends[0] != 0.0:
        blackdetect_ends.insert(0, 0.0)
    if len(blackdetect_ends) == len(blackdetect_starts) + 1:
        if blackdetect_starts[-1] != duration_s:
            blackdetect_starts.append(duration_s)
        else:
            raise ValueError("Incompatible blackdetect_ends and blackdetect_starts")
    assert 0 <= len(blackdetect_starts) - len(blackdetect_ends) <= 1
    span_info = {s: {} for s in zip(blackdetect_ends, blackdetect_starts)}

    n_spans = len(span_info)
    max_splits = 99 - n_spans
    for order, ((start, end), info) in enumerate(sorted(span_info.items())):
        relative_order = info["relative_order"] = min((order, n_spans - order), key=abs)
        if -1 <= relative_order <= 1:
            start_delay, end_delay = +3, -3
            target_scene_duration = 4
        else:
            start_delay, end_delay = +10, -10
            target_scene_duration = 150
        if start == end:
            # maybe 0., 0. is the only catch here
            continue
        this_span_n_splits = int(
            ((end + end_delay) - (start + start_delay)) / target_scene_duration
        )
        if this_span_n_splits > max_splits:
            this_span_n_splits = max_splits
        elif this_span_n_splits <= 0:
            continue

        this_span_timestamp_attrs = [
            (timestamp, attrs)
            for timestamp, attrs in timestamp_attrs
            if start + start_delay <= timestamp <= end + end_delay
        ]
        if this_span_timestamp_attrs:
            top_splits = sorted(
                this_span_timestamp_attrs,
                key=lambda pair: -float(pair[1]["lavfi.scd.score"]),
            )[:this_span_n_splits]
            if top_splits:
                info["splits"] = sorted(timestamp for timestamp, _ in top_splits)

    selected_timestamps = []
    for (start, end), info in span_info.items():
        selected_timestamps.append(start)
        if "splits" in info:
            selected_timestamps.extend(info["splits"])
        selected_timestamps.append(end)
    if selected_timestamps[0] != 0.0:
        selected_timestamps.insert(0, 0.0)
    if selected_timestamps[-1] != duration_s:
        selected_timestamps.append(duration_s)
    return selected_timestamps


def split_scene_score(video_path, output_dir, verbose=__debug__):
    video_path = Path(video_path)
    output_dir = Path(output_dir)
    screencap_path = output_dir / f"{video_path.name}.JPEG"
    if not output_dir.is_dir():
        raise OSError(f"'{output_dir}/' does not exist")

    media_info = ffprobe(video_path)
    cues = _select_cues(video_path, media_info)

    progress = Progress(transient=True)
    with tempfile.TemporaryDirectory(ignore_cleanup_errors=True) as tmpdir, progress:
        tmpdir = Path(tmpdir)
        produced_segments = _split_video(
            video_path=video_path,
            media_info=media_info,
            screencap_path=screencap_path,
            splits=cues,
            tmpdir=tmpdir,
            verbose=verbose,
        )
        progress.console.status(
            f"{len(cues)} cues produced {len(produced_segments)} segments"
        )
        for (segment_start, segment_end), segment_attrs in progress.track(
            produced_segments, "Moving"
        ):
            path = segment_attrs["path"]
            dest = output_dir
            dest.mkdir(parents=True, exist_ok=True)
            shutil.move(path, dest / path.name.replace(" ", "_"))
    return True


def _get_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("-i", "--input-dir", type=Path, metavar="DIR")
    parser.add_argument(
        "--LightWorks",
        type=Path,
        metavar="FILE",
        help="Optionally use these exported JSON cues from LightWorks. "
        "If --input-dir is given, exactly one JSON file is assumed to be these directions. "
        "Otherwise, an algorithm cuts the video at intervals. ",
    )
    parser.add_argument("--screencap", type=Path, metavar="FILE")
    parser.add_argument("--trash", type=Path, metavar="DIR")
    parser.add_argument("--video", type=Path, metavar="FILE")
    parser.add_argument("-o", "--output-dir", type=Path, metavar="DIR")
    parser.add_argument("-v", "--verbose", action="count", default=0)
    parser.add_argument(
        "--version", action="version", version=f"%(prog)s {__version__}"
    )
    return parser


def main():
    args = _get_parser().parse_args()
    log_level = logging.WARNING - 10 * args.verbose
    logging.basicConfig(level=log_level, format="%(levelname)s: %(message)s")

    if args.video:
        video_path = args.video
    elif args.input_dir:
        (video_path,) = args.input_dir.glob("*.mp4")
    if args.output_dir:
        output_dir = args.output_dir
    else:
        output_dir = args.input_dir
    if args.LightWorks:
        directions_path = args.LightWorks
    else:
        json_paths = list(args.input_dir.glob("*.json"))
        if len(json_paths) == 1:
            (directions_path,) = json_paths
    if args.screencap:
        screencap_path = args.screencap
    else:
        screencap_path = output_dir / "covers" / f"{video_path.name}.JPEG"
    if args.trash:
        args.trash.mkdir(exist_ok=True)

        def trash(path):
            shutil.move(path, args.trash)
    else:

        def trash(path):
            logging.debug("Deleting %r", path)
            subprocess.run(["gio", "trash", path])

    mode = "LightWorks" if directions_path else "scene score"
    screencap_path.parent.mkdir(exist_ok=True, parents=True)
    match mode:
        case "LightWorks":
            if split_lightworks(
                video_path,
                directions_path,
                output_dir,
                screencap_path=screencap_path,
                verbose=bool(args.verbose),
            ):
                if not directions_path.parent.samefile(screencap_path.parent):
                    shutil.move(directions_path, screencap_path.parent)
                trash(video_path)
            else:
                sys.exit(10)
        case "scene score":
            if not split_scene_score(
                video_path,
                output_dir,
                screencap_path=screencap_path,
                verbose=bool(args.verbose),
            ):
                trash(video_path)
            else:
                sys.exit(10)
        case _:
            sys.exit(f"Unknown mode {mode!r}")


if __name__ == "__main__":
    sys.exit(main())
