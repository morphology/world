import json
import logging
import re
from datetime import timedelta
from pathlib import Path

logger = logging.getLogger(__name__)


class CutList:
    timecode_re = re.compile(r"(\d\d)[:](\d\d)[:](\d\d)[:;+](\d\d)")

    def __init__(self, list_path):
        path = self.path = Path(list_path)
        with path.open() as fi:
            lightworks_cuelist = json.load(fi)
        self.markers = lightworks_cuelist.pop("markers")
        self.source = lightworks_cuelist.pop("source")
        fps_text, _, fps_unit = self.source["rate"].partition(" ")
        if fps_unit == "fps":
            self.fps = float(fps_text)

    def convert_timecode(self, text):
        if m := self.timecode_re.match(text):
            hours_s, minutes_s, seconds_s, frames_s = m.groups()
            timestamp = timedelta(
                hours=int(hours_s),
                minutes=int(minutes_s),
                seconds=int(seconds_s) + int(frames_s) / self.fps,
            )
            return timestamp

    def get_cues(self):
        cues = []

        # end=None here indicates start == end, a result of the UI not setting end
        for marker in self.markers:
            marker = dict(marker)
            in_s = marker.pop("in")
            out_s = marker.pop("out")
            cues.append(
                (
                    [
                        self.convert_timecode(in_s),
                        self.convert_timecode(out_s) if in_s != out_s else None,
                    ],
                    marker,
                )
            )
        for i in range(len(cues) - 1):
            span, _ = cues[i]
            if span[1] is None:
                (span[1], _), _ = cues[i + 1]
        return cues
