import logging
import os
from pathlib import Path


logger = logging.getLogger(__name__)

DEZGO_CONFIG_HOME = Path((os.getenv("XDG_CONFIG_HOME") or (Path.home() / ".config"))) / "dezgo"
LORAS_HOME = DEZGO_CONFIG_HOME / "loras"
PRESETS_HOME = DEZGO_CONFIG_HOME / "presets"
