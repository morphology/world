"""
Client for Dezgo
"""
import io
import json
import logging
import math
import os
import random
import re
import shutil
import tempfile
from dataclasses import dataclass
from functools import cache, cached_property
from typing import Optional, List, Set, Iterable

import httpx
import pyvips
from gimp_batch import images

logger = logging.getLogger(__name__)

CONTROLNET_CONTROLS = ["canny", "depth", "lineart", "mlsd", "normalbae", "openpose_full", "scribble"]


@cache
def args_for_aspect_ratio(ratio, n_pixels):
    if ratio == 1.0:
        return {}
    aspect_width = int(math.sqrt(n_pixels * ratio))
    aspect_height = int(n_pixels / aspect_width)
    return {"width": aspect_width, "height": aspect_height}


def seed_range(i: Optional[int], n: int):
    if i is None:
        i = random.randrange(0, 1 << 32)
    step_size = (1 << 32) // (n + 1)
    i %= step_size
    return [*range(i, 1 << 32, step_size), (1 << 32) - 1][:n]


@dataclass
class ModelInfo:
    name: str
    native_resolution: tuple[float, float]
    triggers: Optional[List]
    functions: Set[str]
    categories: Set[str]
    license: Optional[str]
    description: Optional[str]
    family: str

    @classmethod
    def from_json(cls, obj):
        resolution_d = obj.pop("nativeResolution")
        return cls(
            native_resolution=(resolution_d.pop("width"), resolution_d.pop("height")),
            functions=set(obj.pop("functions")),
            categories=set(obj.pop("categories")),
            **obj,
        )

    @cached_property
    def n_pixels(self):
        native_width, native_height = self.native_resolution
        return native_width * native_height

    def args_for_aspect_ratio(self, ratio):
        if ratio == 1.0:
            return {}
        return args_for_aspect_ratio(ratio, self.n_pixels)


@dataclass
class Lora:
    sha256sum: str
    strength: float
    url: Optional[str] = None
    family: Optional[str] = None
    triggers: Optional[list] = None

    @classmethod
    def from_file(cls, path):
        with open(path) as fi:
            return cls(**json.load(fi))

    def to_file(self, path):
        with open(path, "w") as fo:
            json.dump(vars(self), fo)

    def to_args(self, index=1):
        return {
            f"lora{index}": self.sha256sum,
            f"lora{index}_strength": self.strength,
        }


class Runner:
    def __init__(self, token=os.getenv("DEZGO_TOKEN")):
        self.token = token
        self.client = httpx.Client(
            headers={
                "Accept": "application/json",
                "X-Dezgo-Key": self.token,
            },
            base_url="https://api.dezgo.com/",
        )
        self.account_info = None
        self.workdir = tempfile.mkdtemp()

    def __del__(self):
        try:
            self.client.close()
        except:
            pass
        try:
            shutil.rmtree(self.workdir)
        except:
            pass

    def get_params(self, prompt, params=None, loras=(), aspect_ratio=None):
        params = dict(params) if params else {}
        if loras:
            for index, lora in enumerate(loras[-2:], start=1):
                params |= lora.to_args(index=index)
        if aspect_ratio is not None:
            assert "model" in params
            models = self.info()["models"]
            model = models[params["model"]]
            params |= model.args_for_aspect_ratio(aspect_ratio)
        logger.debug(f"Sending {params=}")
        return {**params, "prompt": prompt}

    def _set_account_info(self, headers, pattern=re.compile(r"^x-(dezgo)-(.*)")):
        account_info = {}
        for key, value in headers.items():
            if m := pattern.match(key):
                if value.isdigit():
                    value = int(value)
                elif value.isnumeric():
                    value = float(value)
                account_info[m.groups()] = value
        self.account_info = account_info

    @cache
    def info(self):
        response = self.client.get("info")
        self._set_account_info(response.headers)
        response.raise_for_status()
        info = response.json()
        info["models"] = {m.pop("id"): ModelInfo.from_json(m) for m in info.pop("models")}
        return info

    def controlnet(
        self,
        image: os.PathLike | bytes | io.BufferedReader | pyvips.Image,
        prompt: str,
        loras: Optional[Iterable[Lora]] = None,
        params: Optional[dict] = None,
    ):
        route, params = "controlnet", self.get_params(prompt, params, loras)
        image, info = images.load_image_info(image, max_dimension=512)
        response = self.client.post(
            route,
            files={"init_image": image.write_to_buffer(".png")},
            data=params,
            timeout=123,
        )
        if "upscale" in params:
            rescale = info["scale"] = params["upscale"] * info.get("scale", 1)
        self._set_account_info(response.headers)
        response.raise_for_status()
        return pyvips.Image.new_from_buffer(response.content, options=""), info

    def image2image(
        self,
        image: os.PathLike | bytes | io.BufferedReader | pyvips.Image,
        prompt: str,
        loras: Optional[Iterable[Lora]] = None,
        params: Optional[dict] = None,
    ):
        route, params = "image2image", self.get_params(prompt, params, loras)
        image, info = images.load_image_info(image, max_dimension=1024)
        response = self.client.post(
            route,
            files={"init_image": image.write_to_buffer(".png")},
            data=params,
            timeout=123,
        )
        if "upscale" in params:
            rescale = info["scale"] = params["upscale"] * info.get("scale", 1)
        self._set_account_info(response.headers)
        response.raise_for_status()
        return pyvips.Image.new_from_buffer(response.content, options=""), info

    def inpaint(
        self,
        image: os.PathLike | bytes | io.BufferedReader | pyvips.Image,
        prompt: str,
        mask: Optional[os.PathLike | bytes | io.BufferedReader | pyvips.Image] = None,
        params: Optional[dict] = None,
        invert_mask: bool = True,
    ):
        route, params = "inpainting", self.get_params(prompt, params)
        image, info = images.load_image_info(image, max_dimension=1024)
        if mask is None:
            assert info["layer"]["has_mask"]
            mask = info["mask"]
        else:
            mask, mask_info = images.load_image_info(mask, max_dimension=1024)
            if info != mask_info:
                logger.info(f"Image {info=}, but {mask_info=}")
        if invert_mask:
            mask = mask.invert()
        logger.debug(f"Sending {image=} with {mask=}")
        assert (image.width, image.height) == (mask.width, mask.height)
        response = self.client.post(
            route,
            files={"init_image": image.write_to_buffer(".png"), "mask_image": mask.write_to_buffer(".png")},
            data=params,
            timeout=123,
        )
        if "upscale" in params:
            rescale = info["scale"] = params["upscale"] * info.get("scale", 1)
        self._set_account_info(response.headers)
        response.raise_for_status()
        return pyvips.Image.new_from_buffer(response.content, options=""), info

    def remove_background(
        self,
        image: os.PathLike | bytes | io.BufferedReader | pyvips.Image,
    ):
        route = "remove-background"
        image, info = images.load_image_info(image, max_dimension=2048)
        assert "mask" not in info, "Combining masks unsupported"
        response = self.client.post(
            route,
            files={"image": image.write_to_buffer(".png")},
            data={"mode": "mask"},
            timeout=123,
        )
        self._set_account_info(response.headers)
        response.raise_for_status()
        mask = pyvips.Image.new_from_buffer(response.content, options="")
        if info.get("scale", 1.0) != 1.0:
            mask = mask.resize(1 / info.pop("scale"))
        info["mask"] = mask
        return image, info

    def text2image(
        self,
        prompt: str,
        loras: Optional[Iterable[Lora]] = None,
        params: Optional[dict] = None,
        aspect_ratio: Optional[float] = 1.0,
    ):
        route, params = "text2image", self.get_params(prompt, params, loras, aspect_ratio=aspect_ratio)
        info = {}
        if "upscale" in params:
            info["scale"] = params.get("upscale") * info.get("scale", 1)
        response = self.client.post(route, data=params, timeout=123)
        headers = dict(response.headers)
        self._set_account_info(response.headers)
        response.raise_for_status()
        info["filename"] = headers.pop("x-filename", None)
        return pyvips.Image.new_from_buffer(response.content, options=""), info

    def text2image_sdxl_lightning(
        self,
        prompt: str,
        params: Optional[dict] = None,
        aspect_ratio: Optional[float] = 1.0,
        # TODO: Wait for SDXL and lightning to support LORA
    ):
        route, params = "text2image_sdxl_lightning", self.get_params(prompt, params, aspect_ratio=aspect_ratio)
        info = {}
        if "upscale" in params:
            logger.warning("`upscale` unsupported by text2image_sdxl_lightning")
            info["scale"] = 1.0
        response = self.client.post(route, data=params, timeout=123)
        headers = dict(response.headers)
        self._set_account_info(response.headers)
        response.raise_for_status()
        info["filename"] = headers.pop("x-filename", None)
        return pyvips.Image.new_from_buffer(response.content, options=""), info

    def upscale(
        self,
        image: os.PathLike | bytes | io.BufferedReader | pyvips.Image,
        params: Optional[dict] = None,
    ):
        """
        Note that most sophisticated operations upscale.
        """
        route = "upscale"
        image, info = images.load_image_info(image, max_dimension=2048)
        info["scale"] = 2  # Only value supported
        response = self.client.post(
            route,
            files={"image": image.write_to_buffer(".png")},
            data=params,
            timeout=123,
        )
        self._set_account_info(response.headers)
        response.raise_for_status()
        return pyvips.Image.new_from_buffer(response.content, options=""), info
