#! /usr/bin/env python3
"""
Use Stable Diffusion methods at dezgo.com

"""
import argparse
import contextlib
import json
import logging
import os
import readline
import subprocess
import sys
import tempfile
import urllib.parse
import warnings
from pathlib import Path
from typing import Iterable

from gimp_batch import gimp, images
from rich.console import Console
from rich.logging import RichHandler
from rich.pretty import pprint
from rich.progress import Progress
from rich.prompt import Confirm, Prompt
from rich.table import Table
from rich_argparse import RawDescriptionRichHelpFormatter

from . import api
from ._version import __version__
from .dezgo_paths import DEZGO_CONFIG_HOME, LORAS_HOME, PRESETS_HOME

logger = logging.getLogger(__name__)

# Methods supported:
FUNCTIONS = [
    "controlnet",
    "image2image",
    "inpainting",
    "remove-background",
    "text2image",
    "text2image_sdxl_lightning",
    "upscale",
]
PARAMS_HELP = f"""\
You can create a parameters file with:

    %(prog)s params some-file.json model=analog_madness7 guidance=5 steps=80

strength: 0.0 to 1.0
model: Refer to `models --function={'|'.join(FUNCTIONS)}`
negative_prompt: up to 1000 characters
guidance: -20 to 20
steps: 10 to 150
upscale: 1 or 2

Look in {PRESETS_HOME} for examples.

lora1 and lora2 parameters can be inserted, but note that those are overruled by command-line args and belong in
their own loras JSON file.
"""


def launch(paths):
    proc = subprocess.Popen(["gio", "open", *paths], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def ask_launch(paths, console):
    label = f"{len(paths)}" if len(paths) > 1 else f"'{paths[0]}'"
    if console.is_interactive:
        console.print()
        if Confirm.ask(f"Launch {label}?"):
            return launch(paths)


@contextlib.contextmanager
def history(histfile):
    histfile = Path(histfile) if histfile else None
    if not histfile.exists():
        try:
            histfile.write_text("")
        except:
            histfile = None
    if histfile:
        readline.read_history_file(histfile)
    yield
    if histfile:
        readline.write_history_file(histfile)


def lora_preset(name, parent=LORAS_HOME):
    if parent.is_dir():
        return parent / f"{name}.json"


def preset(name, parent=PRESETS_HOME):
    if parent.is_dir():
        return parent / f"{name}.json"


def load_presets(paths: Iterable[os.PathLike]):
    params = {}
    paths = [p for p in paths if p and p.is_file()]
    for path in paths:
        with open(path) as fi:
            params |= json.load(fi)
    return params


def write_preset(path: os.PathLike, entries: Iterable[tuple]):
    content = {}
    for key, value in entries:
        if value.isdigit():
            value = int(value)
        elif value.isnumeric():
            value = float(value)
        content[key] = value
    if content:
        with open(path, "w") as fo:
            json.dump(content, fo)


def show_loras(console: Console, parent: os.PathLike = DEZGO_CONFIG_HOME / "loras"):
    if not parent.is_dir():
        logger.error(f"{parent} not found")
        return

    lora_paths = sorted(parent.glob("*.json"))
    if lora_paths:
        table = Table(title=f"{len(lora_paths)} LORA configs")

        table.add_column("name", style="bold")
        table.add_column("trigger")
        table.add_column("url")
        for path in lora_paths:
            try:
                lora = api.Lora.from_file(path)
                url = lora.url
            except Exception as e:
                logger.error(f"Ignoring '{path}': {e}")
                console.print_exception()
                url = None
            table.add_row(path.stem, lora.triggers or "", url)
        console.print(table)
    return bool(lora_paths)


def setup_parser(parser: argparse.ArgumentParser, command):
    match command:
        case None:
            # Common
            sg = parser.add_argument_group(
                "Common to all functions",
                description=f"Each function looks for its own preset under {PRESETS_HOME}. Only some functions support LORA, and look under {LORAS_HOME}.",
            )
            sg.add_argument("--version", action="version", version=f"%(prog)s {__version__}")
        case "lora":
            parser.add_argument("new_paths", type=Path, nargs="*")
        case "models":
            parser.add_argument("--category", action="append", dest="categories")
            parser.add_argument("--function", choices=FUNCTIONS)
            parser.add_argument("names", nargs="*")
        case "params":
            parser.add_argument("output_path", type=Path)
            parser.add_argument("key_values", metavar="KEY=VALUE", nargs="*")
        case "controlnet" | "image2image" | "inpaint":
            parser.add_argument("--n-repeats", "-n", type=int, default=1)
        case "remove-background":
            parser.add_argument("image_paths", nargs="+" if sys.stdin.isatty() else "*", metavar="IMAGE", type=Path)
            parser.add_argument("--output-type", "-t", choices=("png", "xcf"), default="xcf" if gimp else "png")
            parser.set_defaults(from_stdin=not sys.stdin.isatty())
        case "text2image":
            parser.add_argument("--aspect-ratio", type=float)
            parser.add_argument("--lightning", action="store_const", dest="function", const="text2image_sdxl_lightning")
            parser.add_argument("--n-repeats", "-n", type=int, default=1)
            parser.add_argument("output_dir", type=Path)
        case "upscale":
            parser.add_argument("image_paths", nargs="+" if sys.stdin.isatty() else "*", metavar="IMAGE", type=Path)
            parser.set_defaults(from_stdin=not sys.stdin.isatty())
    match command:
        case "controlnet" | "image2image" | "inpaint" | "text2image":
            sg = parser.add_argument_group(f"{command} presets")
            sg.add_argument(
                "--preset",
                type=preset,
                dest="preset_paths",
                action="append",
                help="(can be given multiple times)",
            )
            sg.add_argument(
                "--preset-json",
                type=Path,
                dest="preset_paths",
                action="append",
                help="(can be given multiple times)",
            )
    match command:
        case "controlnet" | "image2image" | "text2image":
            sg = parser.add_argument_group(f"{command} models")
            sg.add_argument(
                "--lora",
                type=lora_preset,
                dest="lora_paths",
                action="append",
                help="(only the last two LORA can be used)",
            )
            sg.add_argument(
                "--lora-json",
                type=Path,
                dest="lora_paths",
                action="append",
                help="(only the last two LORA can be used)",
            )
            sg.add_argument("--lora-strength", nargs=2, type=float, metavar=("LORA1", "LORA2"))
            if command == "controlnet":
                sg.add_argument("--control-model", choices=api.CONTROLNET_CONTROLS)
                sg.add_argument("--control-scale", type=float)
    # Automatically load preset files named after their command:
    match command:
        case "controlnet" | "image2image" | "inpaint":
            parser.set_defaults(preset_paths=[preset(command)])
        case _:
            parser.set_defaults(preset_paths=[])


def main():
    console = Console(stderr=True)
    logging.basicConfig(level=logging.DEBUG if __debug__ else logging.WARNING, handlers=[RichHandler(console=console)])
    warnings.simplefilter("ignore", ResourceWarning)
    parser = argparse.ArgumentParser(formatter_class=RawDescriptionRichHelpFormatter, description=__doc__)
    parser.add_argument("-v", "--verbose", action="count", default=0)
    setup_parser(parser, command=None)  # Common group
    subparsers = parser.add_subparsers(dest="command", required=True)
    setup_parser(subparsers.add_parser("controlnet", formatter_class=parser.formatter_class), "controlnet")
    setup_parser(subparsers.add_parser("image2image", formatter_class=parser.formatter_class), "image2image")
    setup_parser(subparsers.add_parser("inpaint", formatter_class=parser.formatter_class), "inpaint")

    setup_parser(
        subparsers.add_parser(
            "lora",
            formatter_class=parser.formatter_class,
            help="Create a parameters file for CivitAI LORA add-ons",
            description=f"Look in {LORAS_HOME} for examples",
        ),
        "lora",
    )
    setup_parser(
        subparsers.add_parser(
            "models",
            formatter_class=parser.formatter_class,
        ),
        "models",
    )
    setup_parser(
        subparsers.add_parser(
            "params",
            description=PARAMS_HELP,
            formatter_class=RawDescriptionRichHelpFormatter,
            help="Create a parameters file",
        ),
        "params",
    )
    setup_parser(
        subparsers.add_parser("remove-background", formatter_class=parser.formatter_class), "remove-background"
    )
    setup_parser(subparsers.add_parser("text2image", formatter_class=parser.formatter_class), "text2image")
    setup_parser(subparsers.add_parser("upscale", formatter_class=parser.formatter_class), "upscale")

    args = parser.parse_args()
    logger.setLevel(logging.WARNING - 10 * args.verbose)
    match args.command:
        case "lora":
            run_lora(args, console)
        case "models":
            run_models(args, console)
        case "params":
            run_params(args, console)
        case "controlnet" | "image2image" | "inpaint" | "text2image":
            run_interactive(args.command, args, console)
        case "remove-background":
            run_remove_background(args, console)
        case "upscale":
            run_upscale(args, console)


def run_lora(args, console):
    if args.new_paths:
        for path in args.new_paths:
            console.print(f"Saving {path}")
            if not (sha256sum := Prompt.ask("Paste SHA256 from CivitAI (empty cancels)", console=console)):
                return
            api.Lora(
                sha256sum=sha256sum,
                strength=float(Prompt.ask("Enter default strength (0 disables)", console=console) or 0),
                triggers=Prompt.ask("Optional triggers", console=console),
                url=Prompt.ask("Optional URL back to civitai.com", console=console),
            ).to_file(path)
            console.print()
    elif not show_loras(console):
        sys.exit("no match")


def run_models(args, console):
    runner = api.Runner()
    categories = set(args.categories) if args.categories else None
    models = sorted(runner.info()["models"].items(), key=lambda pair: (-pair[1].n_pixels, pair[0]))

    if args.names:
        models = [(i, m) for i, m in models if any(p in i for p in args.names)]
    if categories is not None:
        models = [(i, m) for i, m in models if (categories & m.categories)]
    if args.function is not None:
        models = [(i, m) for i, m in models if args.function in m.functions]
    if console.is_terminal:
        table = Table(title=f"{len(models)} model(s)")

        table.add_column("categories", style="green")
        table.add_column("resolution", justify="center")
        table.add_column("name")
        table.add_column("id", justify="right", no_wrap=True, style="bold")
        table.add_column("description")
        for name, model in models:
            table.add_row(
                ";".join(model.categories),
                str(model.native_resolution),
                model.name,
                name,
                model.description,
            )
        console.print(table)
    else:
        pprint(models, console=console)


def run_params(args, console):
    entries = []
    for entry in args.key_values:
        key, _, value = entry.partition("=")
        entries.append((key, value))
    write_preset(args.output_path, entries)
    console.print()


def load_loras(args):
    # Important: Only two LORA models can be loaded
    if args.lora_paths:
        paths = [p for p in args.lora_paths if p.is_file()]
        loras = [api.Lora.from_file(p) for p in paths[-2:]]
        if loras and args.lora_strength:
            for i in range(len(loras)):
                loras[i].strength = args.lora_strength[i]
        return loras


def run_interactive(command, args, console):
    runner = api.Runner()

    with history(".prompts"), tempfile.TemporaryDirectory(prefix=command) as td:
        td = Path(td)
        try:
            match command:
                case "controlnet" | "image2image" | "inpaint":
                    max_dimension = 1024
                    for order in range(1, 9999 + 1):
                        input_path = Prompt.ask(f"{order}: enter filename\n", console=console)
                        if not input_path:
                            break
                        if "://" in input_path:
                            url_parts = urllib.parse.urlsplit(input_path)
                            assert url_parts.scheme == "file"
                            input_path = Path(urllib.parse.unquote(url_parts.path))
                        else:
                            input_path = Path(input_path)
                        name = input_path.stem
                        if (output_path := input_path.parent / f"{name}-{command}.xcf").exists():
                            output_path.rename(f"{output_path}~")
                        prompt = Prompt.ask(f"Prompt for {output_path}\n", console=console)
                        console.print()
                        if not prompt:
                            break

                        original_image, loaded_info = images.load_image_info(input_path, max_dimension=max_dimension)
                        layer_paths = []
                        with (progress := Progress(console=console)):
                            for seed in progress.track(
                                api.seed_range(None, args.n_repeats), description="Stable Diffusion"
                            ):
                                params = {"seed": seed, **load_presets(args.preset_paths)}
                                match command:
                                    case "controlnet":
                                        if args.control_model is not None:
                                            params["control_model"] = args.control_model
                                        if args.control_scale is not None:
                                            params["control_scale"] = args.control_scale
                                logger.info(f"Using preset parameters {params}")
                                layer_path = td / f"{command}-{seed:08d}.png"
                                match command:
                                    case "controlnet":
                                        image, info = runner.controlnet(
                                            image=original_image,
                                            prompt=prompt,
                                            loras=load_loras(args),
                                            params=params,
                                        )
                                    case "image2image":
                                        image, info = runner.image2image(
                                            image=original_image,
                                            prompt=prompt,
                                            loras=load_loras(args),
                                            params=params,
                                        )
                                    case "inpaint":
                                        image, info = runner.inpaint(
                                            image=original_image,
                                            mask=loaded_info["mask"],
                                            prompt=prompt,
                                            params=params,
                                        )
                                image.write_to_file(layer_path)
                                image = None
                                layer_paths.append(layer_path)
                        layer_paths.sort()
                        with console.status("Saving results") as spinner:
                            transfer_scale = loaded_info.get("scale", 1.0) * info.get("scale", 1.0)
                            rescale = params.get("upscale", 1.0)
                            if "layer" in loaded_info:
                                transfer_offset = loaded_info["layer"]["offset"]
                                rescaled_offset = tuple(c * rescale for c in transfer_offset)
                            else:
                                rescaled_offset = (0, 0)
                            if gimp.combine(
                                [
                                    {
                                        "path": path,
                                        "offset-x": round(rescaled_offset[0]),
                                        "offset-y": round(rescaled_offset[1]),
                                    }
                                    for path in layer_paths
                                ],
                                backdrop_path=input_path,
                                scale=transfer_scale,
                                output_path=output_path,
                            ):
                                console.print("Done!")
                        ask_launch([output_path], console)
                    else:
                        logger.error("Possible endless loop")
                case "text2image":
                    args.output_dir.mkdir(exist_ok=True)

                    order = 1
                    while order < 10000:
                        if loras := load_loras(args):
                            function = args.function or "text2image"
                        else:
                            function = args.function or "text2image_sdxl_lightning"
                        prompt = Prompt.ask(f"Prompt for image #{order}\n")
                        console.print()
                        if not prompt:
                            break

                        generated_image_paths = []
                        with (progress := Progress(console=console)):
                            for seed in progress.track(
                                api.seed_range(order, args.n_repeats), description="Stable Diffusion"
                            ):
                                while (output_path := args.output_dir / f"{command}-{order:04d}.webp").exists():
                                    order += 1
                                    continue
                                params = {"seed": seed, **load_presets([preset(function), *args.preset_paths])}
                                logger.info(f"Using preset parameters {params}")
                                match function:
                                    case "text2image":
                                        image, info = runner.text2image(
                                            prompt=prompt,
                                            loras=loras,
                                            params=params,
                                            aspect_ratio=args.aspect_ratio,
                                        )
                                    case "text2image_sdxl_lightning":
                                        image, info = runner.text2image_sdxl_lightning(
                                            prompt=prompt,
                                            params=params,
                                            aspect_ratio=args.aspect_ratio,
                                        )
                                image.write_to_file(output_path)
                                image = None
                                generated_image_paths.append(output_path)
                        ask_launch([generated_image_paths], console)
                    else:
                        logger.error("Possible endless loop")
            logger.info(f"Account info: {runner.account_info}")
        except (EOFError, KeyboardInterrupt):
            return


def run_remove_background(args, console):
    runner = api.Runner()
    paths = args.image_paths
    if args.from_stdin:
        paths = [Path(line.rstrip("\n")) for line in sys.stdin]

    with tempfile.TemporaryDirectory(prefix=args.command) as td, (progress := Progress(console=console)):
        td = Path(td)
        output_suffix = f".{args.output_type}"
        for n, path in progress.track(enumerate(paths)):
            output_path = path.parent / f"{path.stem}-masked{output_suffix}"
            if output_path.exists():
                logger.warning(f"Overwriting '{output_path}'")
            image, info = runner.remove_background(path)
            info.pop("mask").write_to_file(mask_path := td / f"mask-{n:08d}.png")
            match output_path.suffix:
                case ".xcf":
                    gimp.mask(path, mask_path, output_path)
                case ".png":
                    gimp.run_batch(
                        """\
                        (define image (car (gimp-file-load RUN-NONINTERACTIVE {path} {path})))
                        (save-png
                          image 
                          (car (gimp-image-merge-visible-layers image CLIP-TO-IMAGE))
                          {output_path}
                          '(
                            (bkgd . 0)
                            (comment . 0)
                            (compression . 9)
                            (gama . 0)
                            (include-time . 0)
                            (interlace . 0)
                            (offs . 0)
                            (phys . 0)
                            (svtrans . 1)
                            )
                          )
                        (gimp-image-delete image)
                        """,
                        params=locals(),
                    )
            assert output_path.exists()


def run_upscale(args, console):
    runner = api.Runner()
    paths = args.image_paths
    if args.from_stdin:
        if stdin_paths := [Path(line.rstrip("\n")) for line in sys.stdin]:
            paths = stdin_paths

    with (progress := Progress(console=console)):
        for path in progress.track(paths):
            output_path = path.parent / f"{path.stem}-upscale.png"
            image, info = runner.upscale(path)
            image.write_to_file(output_path)
