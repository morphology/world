import logging
import sys
from .cli import main

logging.getLogger("httpcore").setLevel(logging.WARNING)
logging.getLogger("pyvips").setLevel(logging.INFO)
sys.exit(main())
