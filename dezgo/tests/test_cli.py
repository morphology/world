import pytest

import sys

from .conftest import TEST_INPUTS

from dezgo import cli


def test_help(monkeypatch, capsys):
    monkeypatch.setattr(sys, "argv", ["foo", "--help"])
    monkeypatch.setattr(sys, "exit", lambda return_value: None)
    cli.main()
    out, err = capsys.readouterr()
    assert "help" in out
