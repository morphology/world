#! /bin/sh
# 8096/tcp is used by default for HTTP traffic
# 8920/tcp is used by default for HTTPS traffic
# 1900/udp is used for service auto-discovery
# 7359/udp is also used for auto-discovery

set -eu
getent passwd jellyfin || useradd -r jellyfin -U -G Books,Music,Videos
install -o jellyfin -d /etc/jellyfin
install -o jellyfin -d /var/cache/jellyfin

docker compose up -d
