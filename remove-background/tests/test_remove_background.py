import logging

import pytest
import remove_background
import rich.console

from .conftest import TEST_INPUTS


def test_batch_create_masked_groups(tmp_path, caplog):
    output_path = tmp_path / "output.xcf"
    with caplog.at_level(logging.DEBUG):
        results = remove_background.batch_create_masked_groups(
            [(TEST_INPUTS / "restaurant.jpg", TEST_INPUTS / "mask.png", output_path)], workdir=tmp_path
        )
    assert results == [(output_path, "success")]
    with open(output_path, "rb") as fi:
        assert fi.read(8) == b"gimp xcf"


@pytest.mark.gpu
def test_remove_background(tmp_path, caplog):
    console = rich.console.Console(stderr=True)
    output_path = tmp_path / "output.xcf"
    with console.capture() as capture, caplog.at_level(logging.DEBUG):
        results = remove_background.batch_remove(
            [(TEST_INPUTS / "restaurant.jpg", output_path)], console=console, workdir=tmp_path
        )
    assert results == [(output_path, "success")]
