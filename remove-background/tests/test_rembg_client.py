import logging

import pytest

pytestmark = pytest.mark.http

import pyvips

from .conftest import TEST_INPUTS

from remove_background.rembg_client import api


def test_get(tmp_path):
    runner = api.Runner()
    mask = runner.proxy("https://upload.wikimedia.org/wikipedia/en/7/7d/Lenna_%28test_image%29.png")
    assert (mask.width, mask.height) == (512,) * 2


def test_post(tmp_path, caplog):
    input_path = TEST_INPUTS / "restaurant.jpg"
    image = pyvips.Image.new_from_file(input_path)
    runner = api.Runner()
    with caplog.at_level(logging.DEBUG):
        mask, mask_info = runner.remove_background(image)
    assert (image.width, image.height) == (mask.width, mask.height)
