"""
Client for rembg
"""
import io
import json
import logging
import os
import shutil
import tempfile

import httpx
import pyvips
from gimp_batch import images

logger = logging.getLogger(__name__)


class Runner:
    models = (
        "isnet-anime|isnet-general-use|sam|silueta|u2net_cloth_seg|u2net_custom|u2net_human_seg|u2net|u2netp".split("|")
    )

    def __init__(self, url=os.getenv("REMBG_API_URL")):
        assert url
        self.client = httpx.Client(
            headers={"Accept": "application/json"},
            base_url=url,
        )
        self.workdir = tempfile.mkdtemp()

    def __del__(self):
        try:
            self.client.close()
        except Exception as e:
            logger.exception(e)
        try:
            shutil.rmtree(self.workdir)
        except Exception as e:
            logger.exception(e)

    def _get_api_params(self, params, extra=None):
        api_params = {}
        for k, v in params.items():
            match k:
                case "alpha_matting":
                    api_params["a"] = bool(v)
                case "alpha_matting_threshold":
                    api_params["af"], api_params["ab"] = v
                case "alpha_matting_erode":
                    api_params["ae"] = v
                case "background_color":
                    api_params["bgc"] = v
                case "model":
                    model_name = str(v)
                    if model_name not in self.models:
                        logger.warning(f"Unexpected {model_name=}")
                    api_params["model"] = v
                case "only_mask":
                    api_params["om"] = bool(v)
                case "post_process":
                    api_params["ppm"] = bool(v)
        if extra:
            api_params["extra"] = json.dumps(extra)
        return api_params

    def remove_background(
        self,
        image: os.PathLike | bytes | io.BufferedReader | pyvips.Image,
        only_mask=False,
        params=None,
        extras=None,
    ) -> tuple[pyvips.Image, dict]:
        route, params = "remove", params or {}
        image, info = images.load_image_info(image)
        assert "mask" not in info, "Combining masks unsupported"
        response = self.client.post(
            route,
            files={"file": image.write_to_buffer(".png")},
            data=self._get_api_params({**params, "only_mask": True}, extras),
            timeout=123,
        )
        response.raise_for_status()
        if only_mask:
            mask = pyvips.Image.new_from_buffer(response.content, options="")
            info["mask"] = mask
        else:
            image = pyvips.Image.new_from_buffer(response.content, options="")
        return image, info

    def proxy(
        self,
        url: str,
        params=None,
        extras=None,
    ) -> tuple[pyvips.Image, dict]:
        route, params = "remove", params or {}
        response = self.client.get(
            route,
            params={**self._get_api_params(params, extras), "url": url},
            timeout=123,
        )
        response.raise_for_status()
        image = pyvips.Image.new_from_buffer(response.content, options="")
        return image
