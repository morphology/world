import os
import site
import warnings
from pathlib import Path

os.environ["LD_LIBRARY_PATH"] = str(Path(site.getsitepackages()[0]) / "tensorrt")
#################################################################################
import logging
import tempfile
from typing import Iterable, Optional

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import rembg
from rich.console import Console
from rich.progress import Progress

from gimp_batch import gimp

logger = logging.getLogger(__package__)

BATCH_CREATE_MASKED_GROUPS_SCHEME = """
(define (import-mask layer mask-file-path)
  (let* (
          (mask-image (car (gimp-file-load RUN-NONINTERACTIVE mask-file-path mask-file-path)))
          (mask-layer (car (gimp-image-get-active-layer mask-image)))
          )
    (unless (gimp-drawable-is-gray mask-layer) (gimp-image-convert-grayscale mask-image))

    (let (
           (layer-mask (car (gimp-layer-create-mask layer ADD-WHITE-MASK)))
           )
      (gimp-layer-add-mask layer layer-mask)
      (gimp-edit-named-copy mask-layer "mask")
      (gimp-floating-sel-anchor (car (gimp-edit-named-paste layer-mask "mask" TRUE)))
      )
    (if (equal? (gimp-layer-get-mask layer) -1) (throw 'programming-error "Failed to create mask"))
    (gimp-image-delete mask-image)
    )
  (gimp-displays-flush)
  )

(define (create-masked-group image-path mask-path output-path name)
  (let* (
          (input-image (car (gimp-file-load RUN-NONINTERACTIVE image-path image-path)))
          (top-layer (car (gimp-image-get-active-layer input-image)))
          (group-layer (car (gimp-layer-group-new input-image)))
          )
    (begin
      (gimp-item-set-name group-layer (or name "Imported"))
      (gimp-image-insert-layer input-image group-layer 0 -1)
      (gimp-image-reorder-item input-image top-layer group-layer 0)
      (import-mask group-layer mask-path)
      ;; for some reason, I see "Error: syntax error: illegal token 1" or "Error: illegal function"
      (gimp-file-save RUN-NONINTERACTIVE input-image group-layer output-path output-path)
      )
    )
  )

(define (batch-create-masked-groups input-paths mask-paths output-paths)
  (map (lambda (image-path mask-path output-path)
         (catch (begin (gimp-message output-path) (cons output-path #f))
           (begin
             (if (file-exists? output-path) (gimp-message (string-append "Appending to " output-path)))
             (create-masked-group image-path mask-path output-path "remove-background")
             (cons output-path (file-exists? output-path))
             )
           )
         )
       input-paths mask-paths output-paths
    )
  )
"""


def batch_create_masked_groups(path_args: Iterable[tuple[os.PathLike, os.PathLike, os.PathLike]], workdir=None):
    workdir = Path(workdir or tempfile.mkdtemp(prefix="batch_create_masked_groups-"))
    results_tab_path = workdir / "batch_create_masked_groups.tab"
    gimp.run_batch(
        scheme=rf"""{BATCH_CREATE_MASKED_GROUPS_SCHEME}
            (let (
                   (tab-port (open-output-file {{results_tab_path}}))
                   )
              (for-each (lambda (result-pair) (begin
                                                (display result-pair) (newline)
                                                (display (car result-pair) tab-port)
                                                (write-char #\tab tab-port)
                                                (display (if (cdr result-pair) "success" "failure") tab-port)
                                                (newline tab-port)
                                                )
                          )
                (batch-create-masked-groups
                   {{input_paths}}
                   {{mask_paths}}
                   {{output_paths}}
                  )
                )
              (unless (not tab-port) (close-output-port tab-port))
              )

            """,
        params=dict(
            input_paths=[p for p, _, _ in path_args],
            mask_paths=[p for _, p, _ in path_args],
            output_paths=[p for _, _, p in path_args],
            results_tab_path=results_tab_path,
        ),
    )
    results = []
    for line in results_tab_path.read_text().splitlines():
        output_path, result = line.split("\t")
        results.append((Path(output_path), result))
    return results


def batch_remove(
    path_args: Iterable[tuple[os.PathLike, os.PathLike]],
    console: Console,
    model: str = "sam",
    workdir: Optional[os.PathLike] = None,
):
    workdir = Path(workdir or tempfile.mkdtemp(prefix="batch_remove-"))
    rembg_session = rembg.new_session(model=model)

    create_masked_group_args = []
    n_failures = 0
    with (progress := Progress(console=console)):
        for n, (input_path, output_path) in enumerate(progress.track(path_args, description="rembg")):
            input_path = Path(input_path)
            output_path = Path(output_path)
            try:
                mask = rembg.remove(input_path.read_bytes(), only_mask=True, session=rembg_session)
            except Exception as e:
                logger.exception(e)
                n_failures += 1
                continue
            (mask_path := workdir / f"batch_remove-{n:08d}.png").write_bytes(mask)
            create_masked_group_args.append((input_path, mask_path, output_path))
    if n_failures > 0:
        logger.info(f"Suppressed {n_failures} error(s)")
    if create_masked_group_args:
        with console.status(f"Saving {len(create_masked_group_args)} XCF files") as spinner:
            # Disable spinner during debugging with spinner.stop()
            return batch_create_masked_groups(create_masked_group_args, workdir=workdir)
