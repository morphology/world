#! /usr/bin/env python3
import argparse
import logging
import os
import sys
import tempfile
from pathlib import Path
from typing import Optional

import rembg
import rich.progress
from rich.console import Console
from rich.logging import RichHandler
from rich_argparse import RawDescriptionRichHelpFormatter

from . import batch_remove

logger = logging.getLogger(__name__)


def setup_parser(parser: argparse.ArgumentParser, command):
    match command:
        case "remove-background":
            parser.add_argument("image_paths", nargs="+" if sys.stdin.isatty() else "*", metavar="IMAGE", type=Path)
            parser.add_argument("--model", choices=rembg.sessions.sessions_names)
            parser.set_defaults(from_stdin=not sys.stdin.isatty())


def run_remove_background(
    args,
    console,
    workdir: Optional[os.PathLike] = None,
):
    paths = args.image_paths
    workdir = Path(workdir or tempfile.mkdtemp(prefix="remove_background-"))
    if args.from_stdin:
        if stdin_paths := [Path(line.rstrip("\n")) for line in sys.stdin]:
            paths = stdin_paths
    with (progress := rich.progress.Progress(console=console)):
        batch_remove_args = []
        for n, path in progress.track(enumerate(paths)):
            output_path = path.parent / f"{path.stem}-masked.xcf"
            if output_path.exists():
                output_path.rename(f"{output_path}~")
            batch_remove_args.append((path, output_path))
    if batch_remove_args:
        n_failures = 0
        for output_path, status in batch_remove(batch_remove_args, console=console, model=args.model, workdir=workdir):
            if status != "success" or not output_path.exists():
                n_failures += 1
                logger.error(f"'{output_path}' failed")
        return n_failures == 0


def main():
    console = Console(stderr=True)
    logging.basicConfig(level=logging.DEBUG if __debug__ else logging.WARNING, handlers=[RichHandler(console=console)])
    parser = argparse.ArgumentParser(formatter_class=RawDescriptionRichHelpFormatter, description=__doc__)
    setup_parser(parser, "remove-background")
    with tempfile.TemporaryDirectory() as td:
        return run_remove_background(parser.parse_args(), console=console, workdir=td)
