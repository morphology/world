#! /usr/bin/env python3
import json
import os
import sys
from pathlib import Path
from typing import List, Optional

import pikepdf

__version__ = "0.0.1"


def merge_pdfs(input_paths: List[os.PathLike], output_path: Optional[os.PathLike]):
    merged = pikepdf.Pdf.new()

    qpdf_spec = {"objectStreams": "generate", "outputFile": str(output_path)}
    spec_parts = []
    for path in input_paths:
        part_pdf = pikepdf.Pdf.open(path)
        n_pages = len(part_pdf.pages)
        merged.pages.extend(part_pdf.pages)
        spec_parts.append(
            {
                "file": str(path),
                "range": f"1-{n_pages}",
                # password
            }
        )
    if spec_parts:
        qpdf_spec["pages"] = spec_parts
        qpdf_spec["empty"] = ""  # true
    if output_path:
        merged.save(
            str(output_path),
            object_stream_mode={
                "disable": pikepdf.ObjectStreamMode.disable,  # debug
                "generate": pikepdf.ObjectStreamMode.generate,
                "preserve": pikepdf.ObjectStreamMode.preserve,  # default
            }["generate"],
        )
    return qpdf_spec


def main():
    *args, output_path = sys.argv[1:]
    output_path = Path(output_path)
    match output_path.suffix.lower():
        case ".json":
            spec_path, output_path = output_path, None
        case _:
            spec_path = output_path.with_suffix(".json")
    spec = merge_pdfs(input_paths=args, output_path=output_path)
    if spec_path:
        with spec_path.open("w") as fo:
            json.dump(
                spec,
                fo,
                indent=2,
            )
        print(f"Use `qpdf --job-json-file={spec_path}` to create")
