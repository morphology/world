#! /usr/bin/env python3
import argparse
from contextlib import contextmanager
from decimal import Decimal
from pathlib import Path
import logging
import os
import shutil
import subprocess
import sys
import tempfile
from typing import Iterable, Optional
import urllib.parse

from rich_argparse import RawDescriptionRichHelpFormatter

from .ffmpeg import FFMPEG, VideoMetadata
from . import m3u

logger = logging.getLogger(__name__)


@contextmanager
def dash_creator(
    output_dir: os.PathLike,
    dash_args: Iterable = (
        "-single_file",
        "0",
        "-streaming",
        "1",
        "-use_template",
        "0",
        "-use_timeline",
        "0",
        "-movflags",
        "+default_base_moof,+frag_keyframe",
        "-codec",
        "copy",
    ),
):
    with tempfile.TemporaryDirectory() as workdir:
        workdir = Path(workdir)
        stream_dir = output_dir
        stream_dir.mkdir(exist_ok=True)

        playlist_filename = stream_dir / "manifest.mpd"
        yield {
            "ffmpeg_output_args": [
                "-f",
                "dash",
                *dash_args,
                playlist_filename,
            ],
            "working_dir": workdir,
        }


def make_dash(
    video_path: os.PathLike,
    output_dir: os.PathLike,
    extra_ffmpeg_args: Iterable = (),
):
    with dash_creator(
        [
            "-single_file",
            "0",
            "-streaming",
            "1",
            "-use_template",
            "0",
            "-use_timeline",
            "0",
            "-movflags",
            "+default_base_moof,+frag_keyframe",
            "-codec",
            "copy",
        ],
        output_dir,
    ) as mkdash:
        command = [
            *FFMPEG,
            *extra_ffmpeg_args,
            "-i",
            video_path,
            *mkdash.pop("ffmpeg_output_args"),
        ]
        logger.debug(f"Running {command}")
        subprocess.run(command, check=True)


@contextmanager
def hls_creator(
    hls_args: Iterable,
    output_dir: os.PathLike,
    segment_pattern: str = "stream-%v/segment-%05d.ts",
    playlist_filename: os.PathLike = "stream-%v/index.m3u8",
    key_info=None,
):
    with tempfile.TemporaryDirectory() as workdir:
        workdir = Path(workdir)
        if key_info:
            key_info_path = workdir / "key-info.txt"
            make_hls_key_info_file(key_info_path, **key_info)
            hls_args += [*hls_args, "-hls_key_info_file", key_info_path]

        yield {
            "ffmpeg_output_args": [
                "-f",
                "hls",
                "-hls_playlist",
                "1",
                "-hls_segment_filename",
                output_dir / segment_pattern,
                *hls_args,
                output_dir / playlist_filename,
            ],
            "working_dir": workdir,
        }


def make_hls(
    video_path: os.PathLike,
    output_dir: os.PathLike,
    key_info=None,
    extra_ffmpeg_args: Iterable = (),
):
    with hls_creator(
        [
            "-hls_playlist_type",
            "vod",
            "-hls_segment_type",
            "mpegts",
            "-hls_flags",
            "independent_segments",
            "-codec",
            "copy",
        ],
        output_dir=output_dir,
        key_info=key_info,
    ) as mkhls:
        command = [
            *FFMPEG,
            *extra_ffmpeg_args,
            "-i",
            video_path,
            *mkhls.pop("ffmpeg_output_args"),
        ]
        logger.debug(f"Running {command}")
        subprocess.run(command, check=True)


def make_hls_key_info_file(path: os.PathLike, location, key_path: os.PathLike, iv: Optional[str] = None):
    lines = [
        str(location),
        str(Path(key_path).resolve()),
    ]
    if iv:
        lines.append(iv)
    return Path(path).write_text("\n".join(lines))


class segment_creator:
    def __init__(
        self,
        video_path: os.PathLike,
        video_info: Optional[FFProbe] = None,
    ):
        self.video_path = video_path
        self.video_info = video_info or FFProbe(video_path)
        self.workdir = Path(tempfile.mkdtemp())
        stream_dir = self.stream_dir = self.workdir / "stream"
        stream_dir.mkdir()

        self.output_pattern = stream_dir / "segment-%05d.ts"
        self.list_path = stream_dir / "index.m3u8"

    def guess_title(self):
        video_format = self.video_info.ffprobe["format"]
        if "tags" in video_format:
            if title := video_format["tags"].get("title"):
                return title.strip()
        video_name = urllib.parse.unquote(self.video_path.stem)
        if " " in video_name:
            return video_name.replace("_", " ")
        else:
            return video_name.replace("_", " ").replace(".", " ")

    def __del__(self):
        shutil.rmtree(self.workdir)

    def __enter__(self):
        self.ffmpeg_output_args = [
            "-f",
            "segment",
            "-map",
            "0",
            # "-segment_start_number", "1",
            "-segment_list",
            self.list_path,
            # "-segment_list_type", "csv",
            "-reset_timestamps",
            "1",
            "-codec",
            "copy",
            self.output_pattern,
        ]
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.list_path and self.list_path.suffix.casefold() == ".m3u8":
            frame_rate = self.video_info.get_video_streams()[0]["r_frame_rate"]
            total_segment_duration = self.add_timestamps_to_playlist()
            duration_error = self.video_info.get_duration() - total_segment_duration
            if not (-1 / frame_rate < duration_error < 1 / frame_rate):
                logger.warning(
                    f"Sum of segments {total_segment_duration} not within +/-{1/frame_rate} of expected duration {self.video_info.get_duration()}"
                )

    def add_timestamps_to_playlist(self):
        # This mimics -segment_type csv, for consistency check while debugging
        playlist = m3u.read_playlist(self.list_path)

        segment_n = 0
        total_segment_duration = 0.0
        for entry in playlist:
            if isinstance(entry, m3u.PlaylistEntry):
                begin = total_segment_duration
                end = total_segment_duration = begin + entry.inf.duration
                entry.inf.row.extend((f"segment={segment_n}", f"begin={begin}", f"end={end}"))
                segment_n += 1

        shutil.move(self.list_path, f"{self.list_path}~")
        self.list_path.write_text("\n".join(str(e) for e in playlist) + "\n")
        return total_segment_duration

    def move(self, output_dir: os.PathLike):
        dest = output_dir / "stream"
        if dest.exists():
            shutil.move(dest, f"{dest}~")
        shutil.move(self.stream_dir, output_dir)


def main():
    parser = argparse.ArgumentParser(formatter_class=RawDescriptionRichHelpFormatter, description=__doc__)
    parser.add_argument("video_path", metavar="VIDEO", type=Path)
    parser.add_argument("-d", "--output-dir", metavar="DIR", type=Path)
    args = parser.parse_args()

    video_path = args.video_path
    with segment_creator(video_path=video_path) as segmenter:
        output_dir = Path(args.output_dir or segmenter.guess_title().replace(" ", "_").replace("/", " - "))
        command = [
            *FFMPEG,
            "-i",
            video_path,
            *segmenter.ffmpeg_output_args,
        ]
        logger.debug(f"Running {command}")
        subprocess.run(command, check=True, text=True)
    segmenter.move(output_dir)


if __name__ == "__main__":
    from rich.logging import RichHandler

    if sys.stderr.isatty():
        logging.basicConfig(
            level=logging.DEBUG if __debug__ else logging.WARNING,
            format="%(message)s",
            handlers=[RichHandler()],
        )
    main()
