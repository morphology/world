#! /usr/bin/env python3

import hashlib
import itertools
import os
import urllib.parse
from dataclasses import dataclass
from typing import Optional, Iterable

import anyio
import bencode2
from loguru import logger


class Magnet:
    #
    # NOTE: I don't really know how to support multiple exact topic entries.
    #
    def __init__(self, xt=None):
        self._schema = "magnet"
        self.exact_topic = xt
        self.fields = {}

    @classmethod
    def from_text(cls, text):
        magnet = cls()
        uri = urllib.parse.urlsplit(text.strip())
        magnet._schema = uri.scheme
        fields = urllib.parse.parse_qs(uri.query)
        xt_field = fields.pop("xt")
        magnet.exact_topic = xt_field if isinstance(xt_field, str) else xt_field.pop(0)
        magnet.fields = fields
        return magnet

    def __str__(self):
        #
        # TODO: test whether quote_plus excaping commas works as intended.
        #
        assert self.exact_topic
        if self.fields:
            qtext = urllib.parse.urlencode(self.fields, doseq=True)
            return f"{self._schema}:?xt={self.exact_topic}&{qtext}"
        else:
            return f"{self._schema}:?xt={self.exact_topic}"

    @property
    def display_name(self):
        return self.fields.get("dn")

    @display_name.setter
    def set_display_name(self, new_name):
        self.fields["dn"] = new_name

    @property
    def selected(self):
        return self.fields.get("so", "").split(",")

    @selected.setter
    def set_selected(self, new_entries):
        self.fields["so"] = ",".join(new_entries)


class Torrent:
    @dataclass(order=True, frozen=True)
    class Address:
        piece: int
        offset: int

    @dataclass(frozen=True)
    class Entry:
        name: str
        size: int
        start: "Torrent.Address"
        end: "Torrent.Address"
        order: int

    def __init__(self):
        #
        # TODO: do ISO torrents even have a files section?
        #
        self.entries: Optional[dict] = {}
        self.n_files = 0  # will this ever be 1, or will single-file torrents lack a files key?
        self.n_padding_files = 0
        self.name: Optional[str | bytes] = None
        self.infohash: str = None
        self.path: Optional[os.PathLike] = None
        self.pieces = []
        self.piece_length = None
        self.total_size = 0

    def get_magnet_link(self, select_files: Optional[Iterable] = None) -> Magnet:
        """
        select_files may be Torrent.Entry objects, which is handy because you may not know if entry.name is bytes or str.

        select_files = None, the default, selects all.
        """
        #
        # BitTorrent v1
        #
        magnet = Magnet(xt=f"urn:btih:{self.infohash}")
        if self.name:
            magnet.display_name = self.name
        if select_files:
            select_files = set(select_files)
            select_numbers = {f.order for f in self.files if f in select_files or f.name in select_files}
            ranges = []
            for include_sublist, sublist in itertools.groupby(
                range(self.n_files + self.n_padding_files), lambda x: x in select_numbers
            ):
                if include_sublist:
                    first, *rest = sublist
                    ranges.append(f"{first}-{rest.pop()}" if rest else str(first))
            if ranges:
                magnet.selected = ranges
        return magnet

    @classmethod
    def from_bytes(cls, content):
        torrent = cls()
        parsed_torrent = bencode2.bdecode(content)
        info = parsed_torrent.pop(b"info")
        torrent.infohash = hashlib.sha1(bencode2.bencode(info)).hexdigest()
        pieces_data = info.pop(b"pieces")
        torrent.piece_length = info.pop(b"piece length")
        torrent.pieces = [pieces_data[i : i + 20] for i in range(0, len(pieces_data), 20)]
        assert len(torrent.pieces[-1]) == 20

        offset = 0
        dir_entries = {}

        for order, attribs in enumerate(info.pop(b"files")):
            size = attribs.pop(b"length")
            begin_location = cls.Address(*divmod(offset, torrent.piece_length))
            offset += size

            if b"path.utf-8" in attribs:
                *parents, filename = [s.decode("utf-8") for s in attribs.pop(b"path.utf-8")]
                parent = "/".join(parents) if parents else None
                if filename.startswith("____padding_file"):
                    torrent.n_padding_files += 1
                    continue
                elif filename.startswith("____"):
                    logger.warning(f"Weird filename: {filename}")
                name = f"{parent}/{filename}" if parent else filename
            else:
                *parents, filename = attribs.pop(b"path")
                parent = b"/".join(parents) if parents else None
                if parent:
                    assert isinstance(parent, bytes)
                assert isinstance(filename, bytes)
                if filename.startswith(b"____padding_file"):
                    torrent.n_padding_files += 1
                    continue
                elif filename.startswith(b"____"):
                    logger.warning(f"Weird filename: {filename}")
                name = (parent + b"/" + filename) if parent else filename
            parent_entry = dir_entries
            for parent in parents:
                parent_entry = parent_entry.setdefault(parent, {})

            torrent_file = cls.Entry(
                name=name,
                size=size,
                start=begin_location,
                end=cls.Address(*divmod(offset, torrent.piece_length)),
                order=order,
            )
            if attribs and attribs.keys() != {b"path"}:
                logger.warning(f"Unexpected attributes on {torrent_file}: {attribs}")
            torrent.n_files += 1
            parent_entry[filename] = torrent_file
        torrent.n_files = torrent.n_files
        torrent.n_padding_files = torrent.n_padding_files
        torrent.entries = dir_entries or None
        torrent.total_size = offset
        if b"name.utf-8" in info:
            torrent.name = info.pop(b"name.utf-8").decode("utf-8")
        else:
            torrent.name = info.pop(b"name")
        return torrent

    @classmethod
    async def from_file(cls, path):
        torrent = cls.from_bytes(await anyio.Path(path).read_bytes())
        torrent.path = path
        return torrent

    def get_dirs(self, dir_entry=None, *, _parent=None):
        if dir_entry is None:
            dir_entry = self.entries
        else:
            assert _parent is not None
        files = []
        for name, item in dir_entry.items():
            if isinstance(item, self.Entry):
                files.append(item)
            elif isinstance(item, dict):
                if isinstance(_parent, str):
                    path = f"{_parent}/{name}"
                elif isinstance(_parent, bytes):
                    path = _parent + b"/" + name
                elif _parent is None:
                    path = name
                else:
                    raise ValueError(_parent)
                yield from self.get_dirs(dir_entry=item, _parent=path)
            else:
                raise ValueError(item)

        yield _parent, files

    @property
    def files(self):
        files = sum([files for _, files in self.get_dirs()], [])
        files.sort(key=lambda e: e.order)
        return files
