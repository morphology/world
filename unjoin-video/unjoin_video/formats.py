import bisect
import collections
import dataclasses
import decimal
import gzip
import itertools
import json
import os
import re
from pathlib import Path
from typing import Any, Iterable, Optional, TypedDict

import anyio
from loguru import logger

import unjoin_video.ffmpeg
from unjoin_video.ffmpeg import MetadataPoint, VideoMetadata


def get_slice_indexes(timestamp_list, segment_start, segment_end):
    if segment_start is not None:
        i = bisect.bisect_left(timestamp_list, segment_start)
        if i < 0:
            i = None
    else:
        i = None
    if segment_end is not None:
        j = bisect.bisect_right(timestamp_list, segment_end)
        if j >= len(timestamp_list):
            j = None
    else:
        j = None
    return (i, j)


class M3uList:
    def __init__(self):
        self.entries = []

    def __repr__(self) -> str:
        locations = []
        n_clips = 0
        for location, clips in self.get_entries():
            locations.append(location)
            n_clips += len(clips)
        return f"<{self.__class__.__name__} for {locations=} // {n_clips} clip(s)>"

    @dataclasses.dataclass(order=True, frozen=True)
    class ExtInf:
        duration: Optional[decimal.Decimal]
        row: tuple

        def __str__(self):
            return "#EXTINF:" + ",".join([str(self.duration) if self.duration else "-1", *self.row])

        @classmethod
        def from_line(cls, text):
            if text.startswith("#EXTINF:"):
                token, sep, text = text.partition("#EXTINF:")
                if sep:
                    duration, *row = text.strip().split(",")
                    duration = decimal.Decimal(duration) if duration != "-1" else None
                    return cls(duration, row=tuple(row))

    @dataclasses.dataclass(order=True, frozen=True)
    class PlaylistEntry:
        location: os.PathLike  # TODO: url
        inf: Optional["M3uList.ExtInf"]
        vlc_opt: Optional[dict] = None

        def __str__(self):
            lines = []
            if self.vlc_opt:
                lines.extend(f"#EXTVLCOPT:{key}={value}" for key, value in self.vlc_opt.items())
            if self.inf:
                lines.append(str(self.inf))
            lines.append(self.location)
            return "\n".join(lines)

        @property
        def exact_start(self):
            if "start-time" in self.vlc_opt:
                return decimal.Decimal(self.vlc_opt["start-time"])

        @property
        def exact_end(self):
            if "stop-time" in self.vlc_opt:
                return decimal.Decimal(self.vlc_opt["stop-time"])

    @classmethod
    def from_text(cls, text, parent=Path()):
        project = cls()
        vlc_opt = {}
        extinf = None
        for line in text.splitlines():
            if line.startswith("#EXTINF"):
                extinf = cls.ExtInf.from_line(line)
            elif line.startswith("#EXTVLCOPT"):
                token, sep, value = line.partition(":")
                if sep:
                    key, sep, value = value.partition("=")
                    if sep:
                        match key:
                            case "start-time" | "stop-time":
                                vlc_opt[key.strip()] = decimal.Decimal(value)
                            case _:
                                vlc_opt[key.strip()] = value.strip()
            elif line.startswith("#") or not line.strip():
                project.entries.append(line)
            else:
                location = line.strip()
                path = None
                if parent is not None:
                    _, name = os.path.split(location)
                    path = os.path.join(parent, name)
                project.entries.append(
                    cls.PlaylistEntry(
                        location=path if (path and os.path.isfile(path)) else location, inf=extinf, vlc_opt=vlc_opt
                    )
                )
                extinf = None
                vlc_opt = {}
        if vlc_opt or extinf:
            logger.warning(f"Ignoring {vlc_opt=} and {extinf=}")
        return project

    def to_text(self) -> str:
        return "\n".join(str(entry) for entry in self.entries)

    def get_entries(self, parent=None):
        for location, entry_iter in itertools.groupby(
            (entry for entry in self.entries if isinstance(entry, self.PlaylistEntry)), lambda e: e.location
        ):
            path = None
            if parent is not None:
                _, name = os.path.split(location)
                path = os.path.join(parent, name)
            yield path if (path and os.path.isfile(path)) else location, list(entry_iter)

    async def check_files(self, parent=None) -> dict[os.PathLike, Optional[VideoMetadata]]:
        results = {}
        for location, entries in self.get_entries(parent=parent):
            if os.path.isfile(location):
                path = location
                try:
                    results[path] = await unjoin_video.ffmpeg.get_video_info(location, show_frames="none")
                except Exception:
                    results[path] = None
        return results


class OpenShotProject:
    class Clip(TypedDict, total=False):
        layer: Any

    class File(TypedDict, total=False):
        path: os.PathLike
        size: str  # int

    def __init__(self):
        self.file_clips = []
        self.attr = {}

    @classmethod
    def from_text(cls, text, parent=None):
        project = cls()
        content = json.loads(text)
        layers_by_order = {layer["number"]: layer for layer in content.pop("layers")}
        clips_by_fid = collections.defaultdict(list)
        for clip in content.pop("clips"):
            fid = hash(clip.pop("file_id"))
            clip["layer"] = layers_by_order[clip["layer"]]
            clips_by_fid[fid].append(cls.Clip(**clip))
        file_by_fid = {}
        for vfile in content.pop("files"):
            fid = hash(vfile["id"])
            if parent is not None:
                _, name = os.path.split(vfile["path"])
                path = vfile["path"] = os.path.join(parent, name)
            file_by_fid[fid] = cls.File(**vfile)
        for fid, clips in clips_by_fid.items():
            clips.sort(key=lambda clip: decimal.Decimal(clip["position"]))
            project.file_clips.append((file_by_fid[fid], clips))
        content.pop("history")
        project.attr = content
        return project

    def __repr__(self):
        n_clips = 0
        filenames = []
        for video_file, clips in self.file_clips:
            n_clips += len(clips)
            filenames.append(Path(video_file.get("path")).name)
        width, height = self.get_resolution()
        duration = self.get_duration()
        return (
            f"<{self.__class__.__name__} for {filenames=} // {width}x{height} {duration:0.2f} s // {n_clips} clip(s)>"
        )

    def get_duration(self):
        return self.attr.get("duration")

    def get_entries(self, parent=None):
        for video_file, clips in self.file_clips:
            path = video_file["path"]
            if parent is not None:
                path = parent / Path(path).name
            yield path, list(clips)

    def get_resolution(self) -> tuple[int, int]:
        return self.attr.get("width"), self.attr.get("height")

    async def check_files(self, parent=None) -> dict[os.PathLike, VideoMetadata]:
        results = {}
        for video_file, _ in self.file_clips:
            path = video_file["path"]
            if parent is not None:
                path = parent / Path(path).name
            size = int(video_file.get("file_size"))
            if path.is_file():
                if path.stat().st_size != size:
                    logger.error(f"Size mismatch for {path}: expected {size=}, got {path.stat()}")
                results[path] = await unjoin_video.ffmpeg.get_video_info(path, show_frames="none")
        return results


def convert_lightworks_timecode(text, fps):
    timecode_re = re.compile(r"(\d\d)[:](\d\d)[:](\d\d)[:;+](\d\d)")
    if m := timecode_re.match(text):
        hours_s, minutes_s, seconds_s, frames_s = m.groups()
        return 60 * (60 * int(hours_s) + int(minutes_s)) + int(seconds_s) + (int(frames_s) / fps)


def correct_lightworks_entries(video_info: VideoMetadata, lightworks_project: "LightworksList"):
    """
    Modifies Lightworks project in-place.
    """
    real_duration = duration = video_info.get_duration()
    real_fps = video_info.get_fps()
    if lightworks_project.magenta_marker:
        last_marker = lightworks_project.markers.pop(-1)
        magenta_duration = last_marker.exact_start
        assert -5 < magenta_duration - real_duration < 5, "User-applied magenta marker misapplied"
        logger.debug(f"Replacing {lightworks_project.markers[-1]}.exact_end with {duration}")
        lightworks_project.markers[-1].exact_end = duration
    fps_text, _, fps_unit = lightworks_project.source["rate"].partition(" ")
    assert fps_unit == "fps"
    lightworks_fps = decimal.Decimal(fps_text)
    time_scaling_factor = 1.0
    if real_fps:
        time_scaling_factor = float(lightworks_fps) / real_fps
        if not (0.99 < time_scaling_factor < 1.01):
            logger.warning(f"Correcting markers by {lightworks_fps=}/{real_fps=} = {time_scaling_factor:1.6f}")
    for marker in lightworks_project.markers:
        if marker.exact_start is not None:
            marker.exact_start = float(marker.exact_start) * time_scaling_factor
    for marker, next_marker in zip(lightworks_project.markers, lightworks_project.markers[1:]):
        if marker.exact_end is None:
            marker.exact_end = next_marker.exact_start


def get_lightworks_markers(
    video_info: VideoMetadata, lightworks_project: "LightworksList"
) -> list["LightworksList.Marker"]:
    correct_lightworks_entries(video_info=video_info, lightworks_project=lightworks_project)
    name_default_pattern = re.compile(r"Cue Marker \d+")
    last_custom_name = None
    markers = []
    for marker in lightworks_project.markers:
        if marker.colour != "red":
            if name_default_pattern.match(marker.name):
                marker.name = last_custom_name
            else:
                last_custom_name = marker.name
            markers.append(marker)
    return markers


@dataclasses.dataclass
class LightworksList:
    source: dict
    markers: list
    magenta_marker: bool = False

    class Source(TypedDict, total=True):
        type: str
        name: str
        rate: str

    class Marker:
        def __init__(self, serialized_cue_marker, fps):
            entry = dict(serialized_cue_marker)
            self.name = entry.pop("name")
            exact_start_s = entry.pop("in")
            self.exact_start = convert_lightworks_timecode(exact_start_s, fps)
            exact_end_s = entry.pop("out")
            if exact_start_s != exact_end_s:
                self.exact_end = convert_lightworks_timecode(exact_end_s, fps)
            else:
                logger.debug("Lightworks UI did not set 'out'")
                self.exact_end = None
            self.colour = entry.pop("colour")
            self.description = entry.pop("description", None)
            if entry:
                logger.error(f"Ignoring {entry}")

        def get_duration(self) -> Optional[decimal.Decimal]:
            if self.exact_end is not None:
                return self.exact_end - self.exact_start

    @classmethod
    def from_export(cls, text):
        """
        It seems LightWorks can export to JSON, but defaults to CSV, so be careful.
        """
        content = json.loads(text)
        source = cls.Source(**content.pop("source"))
        fps_text, _, fps_unit = source["rate"].partition(" ")
        assert fps_unit == "fps"
        fps = decimal.Decimal(fps_text)
        markers = [cls.Marker(serialized_cue_marker=m, fps=fps) for m in content.pop("markers")]
        cues = cls(source=source, markers=markers, magenta_marker=markers[-1].colour == "magenta")
        if content:
            logger.error(f"Ignoring {content}")
        return cues

    def get_name(self):
        return self.source.get("name") or None

    def __repr__(self):
        return f"<LightworksList({self.get_name()}) // {len(self.markers)} clips>"


class UnjoinProject:
    """
    Native format for this application: flat file resulting from ffmpeg -filter:v metadata

    Equivalent to:
        ffmpeg ... \
            -i "$arg" -filter:v "blackdetect,blurdetect,freezedetect,scdet,metadata=print:file=${tmp_txt}" \
            -f null - && <"${tmp_txt}" gzip -n -9
    """

    def __init__(self, frame_attrs: Iterable[tuple[unjoin_video.ffmpeg.MetadataPoint, dict]]) -> None:
        self.frame_attrs = list(frame_attrs)
        if self.frame_attrs:
            start_frame, _ = self.frame_attrs[0]
            self.project_start = start_frame.pts_time  # Set this to None if you know frame_attrs is complete
            end_frame, _ = self.frame_attrs[-1]
            self.project_end = end_frame.pts_time  # Set this to None if you know frame_attrs is complete
        self.parent = None
        self.path = None

    def __bool__(self) -> bool:
        return len(self.frame_attrs) > 0

    def __repr__(self):
        n_frames = len(self.frame_attrs) if self.frame_attrs else None
        n_clips = sum(1 for _ in self.get_entries()) if self.frame_attrs else None
        return f"<{self.__class__.__name__} from {self.parent or self.path!s} // {n_clips} clip(s) in {n_frames=}>"

    @property
    def exact_start(self) -> decimal.Decimal:
        frame, _ = self.frame_attrs[0]
        return frame.pts_time

    @property
    def exact_end(self) -> decimal.Decimal:
        frame, _ = self.frame_attrs[-1]
        return frame.pts_time

    @classmethod
    def from_ffmpeg_metadata_text(cls, text: str) -> "UnjoinProject":
        try:
            return cls(frame_attrs=unjoin_video.ffmpeg.parse_ffmpeg_txt(text))
        except (ValueError, decimal.DecimalException) as e:
            logger.opt(exception=e).error("Corrupt metadata dump")
            raise

    @classmethod
    async def from_ffmpeg_metadata_gz(cls, path: os.PathLike) -> "UnjoinProject":
        with gzip.open(path, "rt") as zfi:
            project = cls.from_ffmpeg_metadata_text(await anyio.wrap_file(zfi).read())
        project.path = Path(path)
        return project

    async def save_ffmpeg_metadata_gz(self, path: os.PathLike) -> None:
        lines = []
        for point, attr in self.frame_attrs:
            lines.append(str(point))
            for key, value in attr.items():
                lines.append(f"{key}={value}")
        if lines:
            with gzip.open(path, "wt") as zfo:
                await anyio.wrap_file(zfo).write("\n".join(lines) + "\n")
        self.path = Path(path)

    def get_entries(self):
        """
        Partition ffmpeg metadata into segments based (only) on black and freeze detections.
        """
        segment_list = []
        segment_start, segment_end = self.project_start, None
        for _, attr in self.frame_attrs:
            segment_end = attr.get("lavfi.black_start") or attr.get("lavfi.freezedetect.freeze_start")
            if segment_end:
                if segment_start is None:
                    if segment_list:
                        segment_start, _ = segment_list.pop(-1)
                segment_list.append((segment_start, segment_end))
                segment_start = None
            segment_start = segment_start or attr.get("lavfi.black_end") or attr.get("lavfi.freezedetect.freeze_end")
        if segment_start is not None:
            segment_list.append((segment_start, segment_end or self.project_end))
        return segment_list

    def slice(self, segment_start, segment_end):
        """
        Child projects based on partitioned frame-attribute lists.
        """
        if (segment_start, segment_end) == (None, None):
            return self
        i, j = get_slice_indexes(
            timestamp_list=sorted(p.pts_time for p, _ in self.frame_attrs),
            segment_start=segment_start,
            segment_end=segment_end,
        )
        subproject = UnjoinProject(frame_attrs=self.frame_attrs[i:j])
        subproject.parent = self
        return subproject


def sort_exact_segments(
    exact_segments: list[tuple[decimal.Decimal | None, decimal.Decimal | None]],
) -> list[tuple[decimal.Decimal | None, decimal.Decimal | None]]:
    segments_without_start, segments_without_end, defined_segments = [], [], []
    for segment in exact_segments:
        match segment:
            case None, None:
                raise ValueError("This is not a list of segments")
            case None, _:
                segments_without_start.append(segment)
            case _, None:
                segments_without_end.append(segment)
            case _, _:
                defined_segments.append(segment)
    segments_without_start.sort()
    segments_without_end.sort()
    defined_segments.sort()
    return [*segments_without_start, *defined_segments, *segments_without_end]


async def read_file(
    path: os.PathLike, video_info: Optional[VideoMetadata] = None
) -> tuple[list[tuple[MetadataPoint | None, MetadataPoint | None]], Any]:
    path = anyio.Path(path)
    match path.suffix.lower():
        case ".json":
            lightworks_project = project = LightworksList.from_export(await path.read_text())
            assert video_info, "Lightworks has a bug which requires knowing video duration"
            markers = get_lightworks_markers(video_info=video_info, lightworks_project=lightworks_project)
            exact_segments = [(entry.exact_start, entry.exact_end) for entry in markers]
        case ".ffmpeg-metadata-gz":
            unjoin_project = project = await UnjoinProject.from_ffmpeg_metadata_gz(path)
            exact_segments = list(unjoin_project.get_entries())
        case ".m3u":
            m3u_list = project = M3uList.from_text(await path.read_text())
            #
            # TODO: m3u with multiple locations
            #
            ((_, entries),) = m3u_list.get_entries()
            exact_segments = [(entry.exact_start, entry.exact_end) for entry in entries]
        case ".osp":
            openshot_project = project = OpenShotProject.from_text(await path.read_text(), parent=path.parent)
            ((_, entries),) = openshot_project.get_entries()
            exact_segments = [(decimal.Decimal(entry["start"]), decimal.Decimal(entry["end"])) for entry in entries]
        case _:
            raise ValueError(f"Unsupported format: {path}")
    return sort_exact_segments(exact_segments), project


async def show_file(
    project_path, video_arg: Optional[VideoMetadata | os.PathLike] = None, parent=None, console=None
) -> bool:
    import rich.table

    project_path = Path(project_path)
    if video_arg is None:
        video_info = video_path = None
    elif isinstance(video_arg, VideoMetadata):
        video_info = video_arg
        video_path = video_info.get_path()
    else:
        video_path = Path(video_arg)
        video_info = await unjoin_video.ffmpeg.get_video_info(video_path)

    try:
        exact_segments, project = await read_file(project_path, video_info=video_info)
    except ValueError:
        logger.info(f"{project_path} unreadable")
        return False
    if isinstance(project, M3uList):
        table = rich.table.Table(title="M3U Labels:")

        table.add_column("Filename")
        table.add_column("Order", justify="right")
        table.add_column("Labels")
        for location, entries in project.get_entries():
            for n, entry in enumerate(entries, start=1):
                table.add_row(str(location), str(n), "|".join(entry.inf.row))
        console.print(table)
    elif isinstance(project, OpenShotProject):
        table = rich.table.Table(title="OpenShot Labels:")

        table.add_column("Filename")
        table.add_column("Order", justify="right")
        table.add_column("Labels")
        for name, entries in project.get_entries():
            for n, entry in enumerate(entries, start=1):
                table.add_row(str(name), str(n), entry.get("layer").get("label"))
        console.print(table)
    try:
        infos = await project.check_files(parent=parent)
        ((_, video_info),) = infos.items()
    except Exception as e:
        logger.info(f"Checking {project} media failed: {e}")

    table = rich.table.Table(title="Cuts:", show_footer=True)

    table.add_column("Start", justify="right")
    table.add_column("End", justify="right")
    total_cut_duration = 0
    durations = []
    for exact_start, exact_end in exact_segments:
        match exact_start, exact_end:
            case None, None:
                duration = video_info.get_duration()
            case None, _:
                duration = exact_end
            case _, None:
                duration = video_info.get_duration() - exact_start
            case _, _:
                duration = exact_end - exact_start
        total_cut_duration += duration
        match exact_start, exact_end:
            case None, None:
                duration = f"[red]{duration:0.3f}[/]"
            case None, _:
                duration = f"[yellow]{duration:0.3f}[/]"
            case _, None:
                duration = f"[yellow]{duration:0.3f}[/]"
            case _, _:
                duration = f"{duration:0.3f}"
        durations.append(duration)
    table.add_column("Duration", justify="right", footer=f"Total: {total_cut_duration:0.3f}")

    for (exact_start, exact_end), duration in zip(exact_segments, durations):
        table.add_row(
            f"{exact_start:0.3f}" if exact_start is not None else "",
            f"{exact_end:0.3f}" if exact_end is not None else "",
            duration,
        )

    console.print(f"{project}:")
    console.print(table)
    return True


if __name__ == "__main__":
    import sys
    import rich.console

    console = rich.console.Console()
    anyio.run(show_file, sys.argv[1], sys.argv[2], console)
