def timestamp(text):
    """
    most likely called with text=str(timedelta(seconds=float(duration)))
    """
    while text and text.startswith("00:"):
        text = text[3:]
    minutes_part, _, seconds_part = text.rpartition(":")
    minutes_part = minutes_part.lstrip("0")
    if "." in seconds_part:
        seconds_part = seconds_part.rstrip("0").rstrip(".")
    return f"{minutes_part}:{seconds_part}" if minutes_part else str(seconds_part)
