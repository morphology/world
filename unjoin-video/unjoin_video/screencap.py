"""
Important: usage of ImageMagick will likely require loosening `/etc/ImageMagick-6/policy.xml`:

```diff
66c66
<   <policy domain="resource" name="disk" value="1GiB"/>
---
>   <policy domain="resource" name="disk" value="40GiB"/>
91c91
<   <policy domain="path" rights="none" pattern="@*"/>
---
>   <policy domain="path" rights="read" pattern="@*"/>

```

"""

import bisect
import decimal
import os
import shutil
import tempfile
from datetime import timedelta
from pathlib import Path
from typing import Iterable, Optional

import anyio
import anyio.to_thread
from loguru import logger

from unjoin_video import ffmpeg
from unjoin_video.ffmpeg import (
    MetadataPoint,
    VideoMetadata,
)
import unjoin_video.formats
import unjoin_video.human_readable

CJXL_DISTANCE = 3.0
JPEG_QUALITY = 66
CONVERT = ("convert", "-quality", str(JPEG_QUALITY))
CONVERT_OVERLAY = (
    "-fill", "gray95",
    "-undercolor", "#00000080",
    "-font", "DejaVu-Sans-Bold",
    "-pointsize", str(24),
    "-antialias",
)  # fmt: skip
FFMPEG = unjoin_video.ffmpeg.FFMPEG
MONTAGE = ("montage",)
NATIVE_SUFFIX = ".ffmpeg-metadata-gz"
SUPPORTED_FORMATS = ("jpg", "jpeg", "png")


async def extract_whole(
    video_info: VideoMetadata,
    screencap_path: os.PathLike,
    metadata_path: Optional[os.PathLike] = None,
) -> unjoin_video.formats.UnjoinProject:
    """
    Produces the first-pass, segmenting a video based on transitions, intros, etc.

    This can be very slow.
    """
    video_path = Path(video_info.get_path())
    screencap_path = Path(screencap_path)
    if metadata_path:
        metadata_path = Path(metadata_path)
        if metadata_path.exists():
            metadata_path.rename(f"{metadata_path}~")
    with tempfile.NamedTemporaryFile(suffix=".log", delete=not __debug__) as logfile:
        async with (
            ffmpeg.screencap_tiles(
                video_info=video_info,
                image_pattern=screencap_path,
            ) as screencapper,
            ffmpeg.transition_detector(video_info=video_info) as detector,
        ):
            command = [
                *FFMPEG,
                "-nostdin",
                *detector.ffmpeg_input_args,
                "-i", video_path,
                *detector.ffmpeg_output_args,
                *screencapper.ffmpeg_output_args,
            ]  # fmt: skip
            logger.debug(f"Running {command}, see {logfile.name}")
            await anyio.run_process(command, stderr=logfile)
    project = unjoin_video.formats.UnjoinProject.from_ffmpeg_metadata_text(detector.results["metadata_text"])
    project.project_start = project.project_end = None
    if metadata_path:
        await project.save_ffmpeg_metadata_gz(metadata_path)
    return project


def align_on_key_frames(
    segment_list: Iterable[tuple[decimal.Decimal, decimal.Decimal]], key_frame_list: list[decimal.Decimal]
) -> list[tuple[float | decimal.Decimal, float | decimal.Decimal]]:
    key_frame_list = list(key_frame_list)
    segments_on_key_frames = []
    for segment_start, segment_end in segment_list:
        if segment_start is not None:
            i = bisect.bisect_right(key_frame_list, segment_start) - 1
            if i < 0:
                logger.debug(f"{segment_start=} before {key_frame_list[0]}")
                i = 0
        else:
            i = 0
        if segment_end is not None:
            j = bisect.bisect_left(key_frame_list, segment_end, lo=i)
            segments_on_key_frames.append((key_frame_list[i], key_frame_list[j] if j < len(key_frame_list) else None))
        else:
            segments_on_key_frames.append((key_frame_list[i], None))
    return segments_on_key_frames


def detect_transitions(
    project: unjoin_video.formats.UnjoinProject,
) -> Optional[list[tuple[decimal.Decimal, decimal.Decimal]]]:
    def blur_key(item):
        frame, attrs = item
        assert isinstance(frame, MetadataPoint)
        return -attrs.get("lavfi.blur", 0)

    def scene_key(item):
        frame, attrs = item
        assert isinstance(frame, MetadataPoint)
        return -attrs.get("lavfi.scd.score", 0)

    scene_timestamps = {frame.pts_time for frame, attrs in project.frame_attrs if "lavfi.scd.time" in attrs}
    logger.debug(f"{len(scene_timestamps)} transition(s) found for {project}")
    if not scene_timestamps:
        return
    scene_timestamps.add(project.exact_start)
    scene_timestamps.add(project.exact_end)
    scene_timestamps = sorted(scene_timestamps)
    exact_segments = []
    for exact_start, exact_end in zip(scene_timestamps, scene_timestamps[1:]):
        assert exact_end != exact_start
        exact_segments.append((exact_start, exact_end))
    return exact_segments


async def extract_parts(
    video_info: VideoMetadata,
    screencap_pattern: str,
    segment_file_path: Optional[os.PathLike] = None,
    do_detect_transitions: Optional[bool] = None,
    metadata_pattern: Optional[str] = None,
    progress_handler=None,
) -> list[dict]:
    """
    Executes the second pass, forming multiple files based on filename pattern.

    Handles caching for .extract_whole()
    """
    video_path = video_info.get_path()
    if not getattr(video_info, "key_frames", None):
        video_info = await ffmpeg.get_video_info(video_path, show_frames="key")
    segment_file_path = Path(segment_file_path) if segment_file_path else None
    if metadata_pattern is None:
        metadata_name, _ = os.path.splitext(screencap_pattern)
        metadata_pattern = f"{metadata_name}{NATIVE_SUFFIX}"
    total_duration = float(video_info.get_duration())
    if progress_handler:
        task = progress_handler.add_task(f"Extracting {video_path.name}...", total=total_duration)
    else:
        task = None

    exact_segments = []
    project = None
    unjoin_project = None
    if segment_file_path and segment_file_path.exists():
        exact_segments, project = await unjoin_video.formats.read_file(segment_file_path, video_info)
    if isinstance(project, unjoin_video.formats.UnjoinProject):
        unjoin_project = project

    with tempfile.NamedTemporaryFile(suffix=".log", delete=not __debug__) as logfile:
        if not exact_segments:
            if not unjoin_project:
                #
                # Derive segments from freezedetect and blackdetect
                #
                result = {"n": 0, "duration": total_duration}
                image_path = result["image_path"] = Path(screencap_pattern.format(n=0))
                assert image_path.parent.is_dir() and os.access(image_path.parent, os.W_OK)
                metadata_path = result["metadata_path"] = Path(metadata_pattern.format(n=0))
                if metadata_path == segment_file_path:
                    metadata_path = None
                if task:
                    progress_handler.update(task, total=2 * total_duration)
                unjoin_project = result["project"] = await extract_whole(
                    video_info=video_info,
                    screencap_path=image_path,
                    metadata_path=metadata_path,
                )
                assert result["metadata_path"].exists()
                yield result
                if task:
                    progress_handler.update(task, advance=total_duration)
            exact_segments = unjoin_video.formats.sort_exact_segments(unjoin_project.get_entries())
        if do_detect_transitions is None:
            do_detect_transitions = len(exact_segments) < 2
        if unjoin_project and do_detect_transitions:
            if segments_with_transitions := detect_transitions(project=unjoin_project):
                if len(segments_with_transitions) > len(exact_segments):
                    exact_segments = segments_with_transitions
                else:
                    logger.debug("Transition detection wasn't useful")

        #
        # Multiple serial processes are not the most-expensive step. ffmpeg is threaded.
        #
        assert getattr(video_info, "key_frames", None), f"{video_info} lacks key frames"
        segments_on_key_frames = align_on_key_frames(
            segment_list=exact_segments, key_frame_list=[f.timestamp for f in video_info.key_frames]
        )
        for i in range(len(segments_on_key_frames) if segments_on_key_frames else 0):
            result = {}
            n = result["n"] = i + 1
            image_path = result["image_path"] = Path(screencap_pattern.format(n=n))  # 1st and further images produced
            assert image_path.parent.is_dir() and os.access(image_path.parent, os.W_OK)

            key_frame_start, _ = result["key_frame_segment"] = segments_on_key_frames[i]
            exact_start, exact_end = exact_segments[i]
            if exact_end is not None:
                duration = decimal.Decimal(exact_end) - decimal.Decimal(key_frame_start)
            else:
                duration = video_info.get_duration() - decimal.Decimal(key_frame_start)
            result["duration"] = duration
            if duration < 1:
                logger.warning(f"Skipping {result}")
                continue
            if image_path.exists():
                image_path.rename(f"{image_path}~")
            if unjoin_project and metadata_pattern:
                metadata_path = result["metadata_path"] = Path(metadata_pattern.format(n=n))
                assert metadata_path != segment_file_path
                if subproject := unjoin_project.slice(segment_start=exact_start, segment_end=exact_end):
                    result["project"] = subproject
                    if metadata_path.exists():
                        metadata_path.rename(f"{metadata_path}~")
                    await subproject.save_ffmpeg_metadata_gz(metadata_path)
                    assert metadata_path.exists()

            async with ffmpeg.screencap_tiles(
                video_info=video_info,
                image_pattern=image_path,
                duration=duration,
            ) as screencapper:
                input_args = result["ffmpeg_args"] = [
                    *screencapper.ffmpeg_input_args,
                    *unjoin_video.ffmpeg.args_to_select_segment(key_frame_start, exact_end),  # beware the order...
                    "-i", video_path,  # ...here
                ]  # fmt: skip
                command = [
                    *FFMPEG,
                    "-nostdin",
                    *input_args,
                    *screencapper.ffmpeg_output_args,
                ]
                logger.debug(f"Running {command}, see {logfile.name}")
                await anyio.run_process(command, stderr=logfile)
            yield result
            if task:
                progress_handler.update(task, advance=float(duration))
    if task:
        progress_handler.remove_task(task)


async def montage(
    image_paths: Iterable[os.PathLike],
    video_info: VideoMetadata,
    output_path: os.PathLike,
    tile: Optional[tuple[int, int | bool]],
    tile_size: str = "100000@",
    title: Optional[str] = None,
    timestamps: Optional[str] = None,
    comment: Optional[bytes] = None,
):
    """
    Valid values for tile are like 6x and 6x5
    """
    video_path = video_info.get_path()
    if not title:
        if tags := video_info.format.get("tags"):
            title = tags.get("title")
    title = title or video_path.stem.replace("_", " ")

    with tempfile.TemporaryDirectory(delete=not __debug__) as td:
        td = Path(td)
        logfile_path = td / "montage.log"
        with logfile_path.open("ab") as logfile:
            if len(image_paths) > 1:
                montage_path = td / "montage.PNG"
                command = [*MONTAGE, "-mode", "Concatenate"]
                if tile:
                    tile_x, tile_y = tile
                    command += ["-tile", f"{tile_x}x{tile_y or ''}"]
                for path in image_paths:
                    command += ["(", "-resize", tile_size, path, ")"]
                command += ["-path", montage_path]
                await anyio.run_process(command, stderr=logfile)
            else:
                (montage_path,) = image_paths
            width, height = video_info.get_resolution()
            megapixels = width * height / 1e6

            command = [
                *CONVERT,
                montage_path,
                *CONVERT_OVERLAY,
                 
                # Title
                "-gravity", "northwest",
                "-annotate", "+0+0",
                title,
                
                # Rate
                "-gravity", "northeast",
                "-annotate", "+0+0",
                f"{megapixels:0.2f} Mpx @ {video_info.get_bit_rate():0.1f} Mbps",
                
                # Duration
                "-gravity", "southwest",
                "-annotate", "+0+0",
                timestamps or unjoin_video.human_readable.timestamp(str(timedelta(seconds=float(video_info.get_duration())))),
                
                # Size
                "-gravity", "southeast",
                "-annotate", "+0+0",
                "{:,d} bytes".format(video_info.get_size()),
            ]  # fmt:skip
            if comment:
                comment_path = anyio.Path(td / "comment.txt")
                await comment_path.write_bytes(comment)
                command += ["-set", "comment", f"@{comment_path}"]
            match ext := output_path.suffix.lower():
                #
                # These prefixes permit ImageMagick utilities to operate on filenames which
                # match arguments.
                #
                case ".jpeg":
                    command += [f"jpg:{output_path}"]
                case ".jxl":
                    intermediate_path = output_path.with_suffix(".PNG")
                    command += [f"png:{intermediate_path}"]
                case _:
                    prefix = ext.strip(".")
                    command += [f"{prefix}:{output_path}"]
            logger.debug(f"Running {command}, see {logfile_path}")
            await anyio.run_process(command, stderr=logfile)
            match output_path.suffix.lower():
                case ".jxl":
                    assert intermediate_path.is_file()
                    command = [
                        "cjxl",
                        f"--distance={CJXL_DISTANCE or 0.0}",
                        intermediate_path,
                        output_path,
                    ]
                    logger.debug(f"Running {command}, see {logfile_path}")
                    await anyio.run_process(command, stderr=logfile)


async def screencap(
    video_arg: VideoMetadata | os.PathLike,
    contact_sheet_pattern: str,
    segment_file_path: Optional[os.PathLike] = None,
    do_detect_transitions: Optional[bool] = None,
    magnet_link=None,
    do_metadata=None,
    progress_handler=None,
) -> Optional[list[os.PathLike]]:
    if isinstance(video_arg, VideoMetadata):
        video_info = video_arg
        video_path = video_info.get_path()
    else:
        video_path = Path(video_arg)
        video_info = await ffmpeg.get_video_info(video_path, "key")

    image_paths = []
    metadata_paths = []
    with tempfile.TemporaryDirectory(delete=not __debug__) as td:
        td = Path(td)
        async for screencap_info in extract_parts(
            video_info=video_info,
            screencap_pattern=f"{str(td)}/{{n:03d}}.PNG",
            do_detect_transitions=do_detect_transitions,
            segment_file_path=segment_file_path,
            progress_handler=progress_handler,
        ):
            exact_start_s = (
                unjoin_video.human_readable.timestamp(str(screencap_info["project"].exact_start or ""))
                if screencap_info.get("project")
                else None
            )
            exact_end_s = (
                unjoin_video.human_readable.timestamp(str(screencap_info["project"].exact_end or ""))
                if screencap_info.get("project")
                else None
            )
            image_path = Path(
                contact_sheet_pattern.format(
                    stem=video_path.stem,
                    from_stem=video_path.stem.replace(":", "_").replace(" ", "_"),
                    video_dir=video_path.parent,
                    segment_file_dir=segment_file_path.parent if segment_file_path else video_path.parent,
                    n=screencap_info.get("n"),
                    timestamps=f"{exact_start_s}-{exact_end_s}" if (exact_start_s or exact_end_s) else "screens",
                )
            )
            if "image_path" in screencap_info:
                logger.debug(f"{screencap_info} -> {image_path}")
                await montage(
                    image_paths=[
                        screencap_info.pop("image_path")
                    ],  # Only issue multiple paths here when you need to tile
                    video_info=video_info,
                    output_path=image_path,
                    tile=True,
                    timestamps=f"{exact_start_s}-{exact_end_s} s" if (exact_start_s or exact_end_s) else None,
                    comment=str(magnet_link).encode() if magnet_link else None,
                )
                image_paths.append(image_path)
            if do_metadata and "metadata_path" in screencap_info and screencap_info["metadata_path"].is_file():
                suffix = screencap_info["metadata_path"].suffix
                assert suffix == NATIVE_SUFFIX, f"Only {NATIVE_SUFFIX} supported for now."
                metadata_path = image_path.with_suffix(suffix)
                logger.debug(f"{screencap_info} -> {metadata_path}")
                if metadata_path.exists():
                    await anyio.Path(metadata_path).rename(f"{metadata_path}~")
                await anyio.to_thread.run_sync(shutil.copy, screencap_info.pop("metadata_path"), metadata_path)
                metadata_paths.append(metadata_path)
    return [*image_paths, *metadata_paths]
