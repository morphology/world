import loguru

from unjoin_video.screencap import SUPPORTED_FORMATS as SUPPORTED_FORMATS

__version__ = "0.0.7"

loguru.logger.disable("unjoin_video")
