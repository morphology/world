#! /usr/bin/env python3
"""
Command-line driver for unjoin_video.screencap
"""

import argparse
import os
import sys
import tempfile
from pathlib import Path

import anyio
from loguru import logger
from rich_argparse import RawDescriptionRichHelpFormatter

from . import __version__
import unjoin_video.formats
import unjoin_video.torrents
from unjoin_video import ffmpeg
from unjoin_video import screencap

NATIVE_SUFFIX = unjoin_video.screencap.NATIVE_SUFFIX


def _setup_parser(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    formatter_class = parser.formatter_class
    parser.add_argument(
        "--tile",
        nargs=2,
        metavar=("COLUMNS", "ROWS"),
        default=ffmpeg.DEFAULT_TILE,
        help="Contact sheet geometry, %(default)s by default",
    )
    parser.add_argument("--quiet", "-q", action="store_true")
    parser.add_argument(
        "--title",
        metavar="SCREENCAP TEXT",
        help="Use '{from_stem}' to override the media tags, or '{orig_title}' to pick only media tags.",
    )
    parser.add_argument("--version", action="version", version=__version__)
    subparsers = parser.add_subparsers(dest="mode")
    extract_parser = subparsers.add_parser("extract", formatter_class=formatter_class)
    extract_parser.add_argument("video_path", metavar="VIDEO", type=Path)
    extract_parser.add_argument("screencap_pattern", metavar="IMAGE")

    screencap_parser = subparsers.add_parser("screencap", formatter_class=formatter_class)
    screencap_parser.add_argument(
        "--detect-transitions",
        action=argparse.BooleanOptionalAction,
        help="Force segments one each occurrance of blur and scene transitions",
    )
    screencap_parser.add_argument("--magnet-link", type=str)
    screencap_parser.add_argument("--magnet-from", metavar="FILE", type=Path)
    screencap_parser.add_argument("--save-metadata", action=argparse.BooleanOptionalAction, default=__debug__)
    screencap_parser.add_argument("--segments-from", metavar="FILE", type=Path)
    screencap_parser.add_argument("video_paths", metavar="VIDEO", type=Path, nargs="*")
    screencap_parser.add_argument(
        "--contact-sheet-pattern",
        metavar="IMAGE",
        default="{segment_file_dir}/{stem}-{n:03d}-{timestamps}.jpeg",
    )

    montage_parser = subparsers.add_parser("montage", formatter_class=formatter_class)
    montage_parser.add_argument("contact_sheet_path", metavar="IMAGE", type=Path)
    montage_parser.add_argument("screencap_dirs", nargs="+", type=Path)

    info_parser = subparsers.add_parser("info", formatter_class=formatter_class)
    info_parser.add_argument("--videos-from", metavar="DIR", type=Path)
    info_parser.add_argument("paths", metavar="FILE", type=Path, nargs="*")
    return parser


def guess_segment_file_path(video_path):
    """
    Lightworks exports markers as JSON. JSON is way too widespread to assume it here.
    """
    video_path = Path(video_path)
    for suffix in (".osp", ".m3u", ".M3U", NATIVE_SUFFIX):
        found_segment_paths = sorted(video_path.parent.glob(f"{video_path.stem.rstrip()}*{suffix}"))
        if len(found_segment_paths) == 1:
            logger.debug(f"{found_segment_paths=} for {video_path}")
            return found_segment_paths.pop()
    logger.debug(f"Found no sole pre-defined table of cuts for {video_path}")


async def _async_main():
    import rich.console
    import rich.prompt
    import rich.progress

    parser = argparse.ArgumentParser(formatter_class=RawDescriptionRichHelpFormatter, description=__doc__)
    _setup_parser(parser)
    arguments = parser.parse_args()
    console = rich.console.Console(quiet=arguments.quiet)
    match arguments.mode:
        case "extract":
            if os.path.sep in arguments.screencap_pattern:
                parent, _ = os.path.split(arguments.screencap_pattern)
                parent = Path(parent)
                parent.mkdir(exist_ok=True, parents=True)
            else:
                parent = Path()
            result = await screencap.extract_whole(
                video_arg=arguments.video_path,
                screencap_path=arguments.screencap_pattern,
                do_montage=False,
            )
            tmp_segment_file = tempfile.mktemp(dir=parent, suffix=NATIVE_SUFFIX)
            unjoin_video.formats.UnjoinProject.from_ffmpeg_metadata_text(
                result["metadata_text"]
            ).save_ffmpeg_metadata_gz(tmp_segment_file)
        case "montage":
            image_paths = []
            for d in arguments.screencap_dirs:
                image_paths.extend(d.glob("**/frame-*.PNG"))
            image_paths.sort(key=lambda p: p.stem)
            await screencap.montage(
                image_paths=image_paths,
                output_path=arguments.contact_sheet_path,
                tile=arguments.tile,
                title=arguments.title,
            )
        case "screencap":
            video_paths = arguments.video_paths
            if not video_paths and not sys.stdin.isatty():
                video_paths = [Path(p) for p in sys.stdin.read().strip("\n").splitlines() if p]
            if not video_paths:
                return None

            if arguments.magnet_from:
                torrent_dir = Path(arguments.magnet_from).parent
                torrent = await unjoin_video.torrents.Torrent.from_file(arguments.magnet_from)
            elif arguments.magnet_link:
                torrent = None
                magnet = unjoin_video.torrents.Magnet.from_text(arguments.magnet_link)
                if "tr" in magnet.fields:
                    del magnet.fields["tr"]
            else:
                torrent = None
                magnet = None

            with (
                progress_handler := rich.progress.Progress(
                    console=rich.console.Console(stderr=True),
                    disable=not sys.stderr.isatty(),
                    expand=True,
                    transient=not __debug__,
                )
            ):
                created_images = []
                for video_path in progress_handler.track(video_paths, description="Processing video(s)..."):
                    try:
                        video_info = await ffmpeg.get_video_info(video_path)
                    except Exception:
                        if not arguments.quiet:
                            logger.exception(f"Skipping {video_path}, expect errors")
                        continue
                    if torrent:
                        magnet = torrent.get_magnet_link(select_files=[str(video_path.relative_to(torrent_dir))])
                    # TODO: allow override video_info.format["duration"]: str
                    segment_file_path = arguments.segments_from or guess_segment_file_path(video_path)
                    if arguments.title:
                        tags = video_info.format.setdefault("tags", {})
                        new_title = arguments.title.format(
                            stem=video_path.stem,
                            from_stem=video_path.stem.replace("_", " ").strip(),
                            orig_title=tags["title"],
                        )
                        tags["title"] = new_title
                    created_images.extend(
                        await screencap.screencap(
                            video_arg=video_info,
                            contact_sheet_pattern=arguments.contact_sheet_pattern,
                            segment_file_path=segment_file_path,
                            do_detect_transitions=arguments.detect_transitions,
                            magnet_link=str(magnet) if magnet else None,
                            do_metadata=arguments.save_metadata,
                            # Manipulates it own progress bar:
                            progress_handler=progress_handler,
                        )
                    )
            if created_images:
                for path in created_images:
                    print(path)
            else:
                return "Failed to create screenshots"
        case "info":
            import rich.json

            paths = arguments.paths
            if not paths and not sys.stdin.isatty():
                logger.debug("Reading paths from {sys.stdin}")
                paths = [Path(p) for p in sys.stdin.read().strip("\n").splitlines() if p]
            if not paths:
                return None
            valid_paths = []
            for path in paths:
                match path.suffix.lower():
                    case ".ffmpeg-metadata-gz" | ".json" | ".m3u" | ".osp":
                        project_path = path
                        if await unjoin_video.formats.show_file(
                            project_path=project_path,
                            video_arg=None,
                            parent=arguments.videos_from or project_path.parent,
                            console=console,
                        ):
                            valid_paths.append(project_path)
                    case _:
                        video_path = path
                        try:
                            video_info = await ffmpeg.get_video_info(path=video_path)
                        except Exception:
                            if not arguments.quiet:
                                logger.exception(f"Skipping {video_path}, expect errors")
                            continue
                        if video_info:
                            console.print(rich.json.JSON(video_info.to_text()))
                            valid_paths.append(video_path)
            if valid_paths:
                for path in valid_paths:
                    print(path)
            else:
                return "No valid paths found"


def main():
    import sys

    import loguru

    logger.enable("unjoin_video")
    if not __debug__:
        loguru.logger.remove()  # Remove all handlers added so far, including the default one.
        loguru.logger.add(
            sys.stderr,
            format="{message}",
            colorize=True,
            level="WARNING",
            diagnose=False,  # Appears to leak sensitive memory contents
        )
    return anyio.run(_async_main)


if __name__ == "__main__":
    import sys

    sys.exit(main())
