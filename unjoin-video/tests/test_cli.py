import sys

import anyio
import loguru
import pytest
from pathlib import Path

import unjoin_video.cli
import unjoin_video.ffmpeg

from .conftest import TEST_INPUT_DIR

BACKENDS = ("asyncio",)

loguru.logger.enable("unjoin_video")


@pytest.mark.parametrize("command", [None, "extract", "montage", "screencap"])
@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_main_help(command, capsys, monkeypatch, anyio_backend):
    monkeypatch.setattr(sys, "argv", ["uvideo", command, "--help"] if command else ["uvideo", "--help"])
    with pytest.raises(SystemExit):
        returnstatus = await unjoin_video.cli._async_main()
        assert returnstatus in (None, 0)
    out, _ = capsys.readouterr()
    assert out.startswith("Usage: ")


@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_main_screencap(capsys, monkeypatch, tmp_path, anyio_backend):
    """
    Should report a list of screencap images, at least one for each input video.
    """
    good_inputs = sorted(TEST_INPUT_DIR.glob("good/*.mp4"))
    assert good_inputs, "No input files found"
    output_pattern = f"{tmp_path}/{{stem}}-{{n:03d}}.jpeg"
    monkeypatch.setattr(
        sys,
        "argv",
        [
            "uvideo",
            "screencap",
            f"--contact-sheet-pattern={output_pattern}",
        ]
        + [str(p) for p in good_inputs],
    )
    returnstatus = await unjoin_video.cli._async_main()
    assert returnstatus in (None, 0)
    out, _ = capsys.readouterr()
    emitted_paths = [Path(line) for line in out.splitlines()]
    for path in emitted_paths:
        assert str(path.resolve()).startswith(str(tmp_path))
        assert path.exists()
    assert len(emitted_paths) >= len(good_inputs)


@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_main_screencap_with_segments(capsys, monkeypatch, tmp_path, anyio_backend):
    """
    Should report a list of screencap images, at least one for each input video.
    """
    (good_input,) = TEST_INPUT_DIR.glob("ffmpeg-metadata-gz/*.mp4")
    output_pattern = f"{tmp_path}/{{stem}}-{{n:03d}}.jpeg"
    monkeypatch.setattr(
        sys, "argv", ["uvideo", "screencap", f"--contact-sheet-pattern={output_pattern}", str(good_input)]
    )
    returnstatus = await unjoin_video.cli._async_main()
    assert returnstatus in (None, 0)
    out, _ = capsys.readouterr()
    emitted_paths = [Path(line) for line in out.splitlines()]
    for path in emitted_paths:
        assert str(path.resolve()).startswith(str(tmp_path))
        assert path.exists()


@pytest.mark.parametrize(
    "magnet_content",
    [pytest.param(p.read_text(), id=p.stem) for p in TEST_INPUT_DIR.glob("misc/magnet*.txt")],
)
@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_main_screencap_with_magnet(magnet_content, capsys, tmp_path, monkeypatch, anyio_backend):
    """
    Should embed comments into supported image formats.
    """
    image_formats = unjoin_video.screencap.SUPPORTED_FORMATS
    good_inputs = sorted(TEST_INPUT_DIR.glob("good/*.mp4"))
    assert good_inputs, "No input files found"
    output_pattern = f"{tmp_path}/{{stem}}-{{n:03d}}.jpeg"
    monkeypatch.setattr(
        sys,
        "argv",
        [
            "uvideo",
            "screencap",
            f"--contact-sheet-pattern={output_pattern}",
            f"--magnet-link={magnet_content}",
        ]
        + [str(p) for p in good_inputs],
    )
    returnstatus = await unjoin_video.cli._async_main()
    assert returnstatus in (None, 0)
    out, _ = capsys.readouterr()
    emitted_paths = [Path(line) for line in out.splitlines()]
    for path in emitted_paths:
        if path.suffix.lower() in image_formats:
            assert "magnet:?xt" in (await anyio.run_process(["identify", "-format", "%c", path])).stdout.decode()


@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_main_info(anyio_backend, monkeypatch):
    monkeypatch.setattr(
        sys,
        "argv",
        ["uvideo", "info"] + [str(p) for p in TEST_INPUT_DIR.glob("good/*.mp4")],
    )
    returnstatus = await unjoin_video.cli._async_main()
    assert returnstatus in (None, 0)
