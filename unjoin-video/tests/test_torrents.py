import anyio
import loguru
import pytest

import unjoin_video.torrents

from .conftest import TEST_INPUT_DIR

loguru.logger.enable("unjoin_video")

BACKENDS = ("asyncio",)


@pytest.mark.parametrize(
    "magnet_content",
    [pytest.param(p.read_text(), id=p.stem) for p in TEST_INPUT_DIR.glob("torrents/magnet*.txt")],
)
def test_magnet_from_link(magnet_content):
    magnet = unjoin_video.torrents.Magnet.from_text(magnet_content)
    assert magnet.exact_topic


@pytest.mark.parametrize(
    "torrent_file_path",
    [pytest.param(anyio.Path(p), id=p.stem) for p in TEST_INPUT_DIR.glob("torrents/*.torrent")],
)
@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_parse_torrent_file(torrent_file_path, anyio_backend):
    print()
    torrent = await unjoin_video.torrents.Torrent.from_file(torrent_file_path)
    total_file_size = 0
    rows = []
    for name, entries in torrent.get_dirs():
        name = name or torrent.name or "unnamed torrent"
        dir_size = sum(e.size for e in entries)
        total_file_size += dir_size
        if dir_size > 0:
            name = name if isinstance(name, str) else name.decode()
            rows.append((dir_size / 1e6, name))
    rows.sort()
    packing_efficiency = total_file_size / (torrent.piece_length * len(torrent.pieces))
    print(f"Packing efficiency: {packing_efficiency:0.1%}")
    for size, name in rows:
        print(f"{size:,.1f} MB\t{name}")
    print()
