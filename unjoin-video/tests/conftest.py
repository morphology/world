from pathlib import Path

TEST_INPUT_DIR = Path(__file__).parent / "test-inputs"
if not list(TEST_INPUT_DIR.glob("good/*.mp4")):
    print(f"=== No good test cases found under {TEST_INPUT_DIR.resolve()} ===")
