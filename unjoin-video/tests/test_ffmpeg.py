import pytest

import subprocess

from .conftest import TEST_INPUT_DIR

import unjoin_video.ffmpeg

BACKENDS = ("asyncio",)


def test_version():
    proc = subprocess.run(
        [*unjoin_video.ffmpeg.FFMPEG, "-version"],
        capture_output=True,
        text=True,
    )
    version_lines = proc.stdout.splitlines()
    print(version_lines[0])
    assert proc.returncode == 0


@pytest.mark.parametrize(
    "path",
    [pytest.param(str(p), id=p.name) for p in TEST_INPUT_DIR.glob("good/*.mp4")],
)
@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_get_video_info(path, anyio_backend):
    await unjoin_video.ffmpeg.get_video_info(path)
