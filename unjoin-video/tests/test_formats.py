import pytest

import unjoin_video.formats

from .conftest import TEST_INPUT_DIR

BACKENDS = ("asyncio",)


# These are exported from Lightworks with Cues List -> Export -> JSON
# The default save location is ~/Lightworks/Lists/
@pytest.mark.parametrize(
    "exported_cues_path",
    [pytest.param(p, id=p.stem) for p in TEST_INPUT_DIR.glob("Lightworks/*.json")],
)
def test_lightworks(exported_cues_path):
    cues = unjoin_video.formats.LightworksList.from_export(exported_cues_path.read_text())
    for marker in cues.markers:
        if marker.exact_end is not None:
            assert marker.exact_start < marker.exact_end


# TODO: magenta-coded Lightworks


@pytest.mark.parametrize(
    "m3u_path",
    [pytest.param(p, id=p.name) for p in TEST_INPUT_DIR.glob("m3u/*.m3u")],
)
def test_m3u_entries(m3u_path):
    text = m3u_path.read_text()
    project = unjoin_video.formats.M3uList.from_text(text)
    for _, entries in project.get_entries():
        for entry in entries:
            if entry.exact_end is not None:
                assert entry.exact_start < entry.exact_end


@pytest.mark.parametrize(
    "osp_path",
    [pytest.param(p, id=p.name) for p in TEST_INPUT_DIR.glob("osp/*.osp")],
)
def test_openshot_entries(osp_path):
    project = unjoin_video.formats.OpenShotProject.from_text(osp_path.read_text())
    for _, entries in project.get_entries():
        for entry in entries:
            exact_start, exact_end = entry["start"], entry["end"]
            if exact_end is not None:
                assert exact_start < exact_end


@pytest.mark.parametrize(
    "ffmpeg_metadata_gz_path",
    [pytest.param(p, id=p.name) for p in TEST_INPUT_DIR.glob("ffmpeg-metadata-gz/*.ffmpeg-metadata-gz")],
)
@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_unjoin_project(ffmpeg_metadata_gz_path, anyio_backend):
    project = await unjoin_video.formats.UnjoinProject.from_ffmpeg_metadata_gz(ffmpeg_metadata_gz_path)
    n_entries = sum(1 for _ in project.get_entries())
    assert n_entries > 0
