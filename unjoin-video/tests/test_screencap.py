import anyio
import loguru
import pytest

import unjoin_video.ffmpeg
import unjoin_video.formats
import unjoin_video.screencap

from .conftest import TEST_INPUT_DIR

BACKENDS = ("asyncio",)

loguru.logger.enable("unjoin_video")


@pytest.fixture
def shorten_test(monkeypatch):
    """
    Speed up video when only a few frames are adequate
    """
    short_FFMPEG = [*unjoin_video.ffmpeg.FFMPEG, "-t", "15"]
    monkeypatch.setattr(unjoin_video.ffmpeg, "FFMPEG", short_FFMPEG)


@pytest.mark.parametrize(
    "video_path",
    [pytest.param(p, id=p.name) for p in TEST_INPUT_DIR.glob("good/*.mp4")],
)
@pytest.mark.parametrize("anyio_backend", BACKENDS)
@pytest.mark.slow
async def test_extract_whole(video_path, tmp_path, anyio_backend):
    video_info = await unjoin_video.ffmpeg.get_video_info(video_path)
    stills_file = anyio.Path(tmp_path / "blank-contact-sheet.jpeg")
    metadata_file = anyio.Path(tmp_path / "foo.ffmpeg-metadata.gz")
    unjoin_project = await unjoin_video.screencap.extract_whole(
        video_info=video_info, screencap_path=stills_file, metadata_path=metadata_file
    )
    print(unjoin_project)
    assert await stills_file.exists()
    assert await metadata_file.exists()


@pytest.mark.parametrize(
    "video_path",
    [pytest.param(p, id=p.name) for p in TEST_INPUT_DIR.glob("ffmpeg-metadata-gz/*.mp4")],
)
@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_read_segments_ffmpeg_metadata_gz(video_path, anyio_backend):
    video_info = await unjoin_video.ffmpeg.get_video_info(video_path)
    segments_file_path = unjoin_video.cli.guess_segment_file_path(video_path)
    exact_segments, project = await unjoin_video.formats.read_file(path=segments_file_path, video_info=video_info)
    print(project)


@pytest.mark.parametrize(
    "video_path",
    [pytest.param(p, id=p.name) for p in TEST_INPUT_DIR.glob("ffmpeg-metadata-gz/*.mp4")],
)
@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_detect_transitions(video_path, anyio_backend):
    def duration_key(chapter):
        return chapter.exact_end - chapter.exact_start

    video_info = await unjoin_video.ffmpeg.get_video_info(video_path)
    segments_file_path = unjoin_video.cli.guess_segment_file_path(video_path)
    scene_results, project = await unjoin_video.formats.read_file(path=segments_file_path, video_info=video_info)
    for exact_start, exact_end in project.get_entries():
        subproject = project.slice(exact_start, exact_end)
        print(subproject)
