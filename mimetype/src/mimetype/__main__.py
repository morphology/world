"""
Simplest useful replacement for `file --mime-type` that works on Windows.
"""
import argparse
from pathlib import Path

from . import guess_type


class _IgnoreAction(argparse._CountAction):
    pass


def _get_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.set_defaults(show_filenames=True)
    parser.add_argument("--brief", "-b", action="store_false", dest="show_filenames")
    parser.add_argument("--mime-type", action=_IgnoreAction)
    parser.add_argument("--dereference", "-L", action=_IgnoreAction)
    parser.add_argument("--no-pad", "-N", action="store_const", dest="padding", const=0)
    parser.add_argument("paths", nargs="+", type=Path)
    return parser


def main():
    args = _get_parser().parse_args()
    mime_type_by_path = [(path, guess_type(path)) for path in args.paths]
    if args.show_filenames:
        if not isinstance(args.padding, int):
            args.padding = max(len(str(path)) for path, _ in mime_type_by_path)
        for path, mime_type in mime_type_by_path:
            print((f"{path}:").ljust(args.padding + 1), mime_type)
    else:
        for _, mime_type in mime_type_by_path:
            print(mime_type)


import sys

sys.exit(main())
