"""
Simplest usable replacement for `file --mime-type` that works on Windows.
"""

import mimetypes
from pathlib import Path

__all__ = ["guess_type"]

try:
    # Windows _might_ have an answer here with python-magic-bin
    import magic
except ImportError:
    magic = None

if magic is not None:

    def guess_type(path):
        return magic.from_file(path, mime=True)

else:

    def guess_type(path):
        path = Path(path)
        if path.is_dir():
            return "directory"
        guessed_type, encoding_program = mimetypes.guess_type(path, strict=False)
        if guessed_type is None:
            size = path.stat().st_size
            if size == 0:
                return "inode/x-empty"
            # See _encodings_map_default
            if encoding_program == "gzip":
                return "application/gzip"
            elif encoding_program == "compress":
                return "application/x-compress"
            elif encoding_program == "bzip2":
                return "application/x-bzip2"
            elif encoding_program == "xz":
                return "application/x-xz"
            with path.open("rb") as fi:
                if fi.seekable():
                    fi.seek(size - 1)
                    if fi.read(1) == b"\n":
                        return "text/plain"
            return "application/octet-stream"
        return guessed_type
