#! /bin/sh
set -eu
source_dir="$1"
install_prefix="$2"

cd "${source_dir}/cmd/caddy"
  go build
  install -s caddy -D "${install_prefix}/sbin/caddy"
  $SUDO setcap cap_net_bind_service=+ep "${install_prefix}/sbin/caddy"
cd -
