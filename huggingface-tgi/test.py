from huggingface_hub import InferenceClient

import sys

(host,) = sys.argv[1:]

client = InferenceClient(model=f"http://{host}")
output = client.text_generation(
    "What is the current version of the Python interpreter?", max_new_tokens=35
)
print(output)
