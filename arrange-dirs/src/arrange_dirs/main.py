import fileinput
import subprocess
import sys
import tempfile
from pathlib import Path
import os

from . import folder
from . import rotate

from .paths import load_config
from .split import Sorter

VISUAL = os.environ["VISUAL"]


def apply_rotate():
    args = sys.argv[1:]
    config = load_config(".rotate.yml")
    rotate.rotate(
        destdir=config["destdir"],
        subdirs=args or config["subdirs"],
    )


def apply_sorter(sorter: Sorter):
    config = load_config(".sorter.yml")
    find_args = sys.argv[1:]
    if find_args:
        proc = subprocess.run(
            ["find", *find_args], check=True, text=True, stdout=subprocess.PIPE
        )
        paths = [Path(p) for p in proc.stdout.splitlines()]
    else:
        if sys.stdin.isatty():
            print("Waiting for paths on stdin...", file=sys.stderr)
        paths = []
        for line in fileinput.input():
            if line := line.rstrip():
                paths.append(Path(line))
    if not paths:
        return "No paths found"

    if script_content := folder.rearrange_paths(
        sorter,
        paths=paths,
        middle_sep=config.pop("middle_sep", None),
        destdir=config.pop("destdir", None),
    ):
        if sys.stdout.isatty():
            with tempfile.NamedTemporaryFile(suffix=".sh") as fo:
                tf = Path(fo.name)
                tf.write_text(script_content + "\n")
                subprocess.run([VISUAL, tf])
        else:
            print(script_content)
            if config:
                print(f"# extra values: {config}")
