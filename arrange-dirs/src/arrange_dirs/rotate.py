from pathlib import Path


def rotate(destdir, subdirs):
    destdir = Path(destdir)
    top_path = Path().parent
    if not subdirs:
        raise ValueError("No subdirectories specified")
    for subdir in subdirs:
        link_path = top_path / subdir
        target = destdir / subdir
        target.mkdir(exist_ok=True, parents=True)
        if link_path.exists() and link_path.samefile(target):
            print(link_path, "already points to", target)
            continue
        elif link_path.is_symlink():
            link_path.unlink()
        if link_path.exists():
            print(subdir, "is not a link")
        else:
            print("Linking", link_path, "to", target)
            link_path.symlink_to(target)
