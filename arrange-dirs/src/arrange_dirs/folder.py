import os
import shlex
from pathlib import Path
from typing import Optional, List

from .split import join_tags, split_filenames, Sorter
from ._version import __version__


def rearrange_paths(
    sorter: Sorter, paths: List[os.PathLike], middle_sep=None, destdir=None
):
    lines = _get_rearrange_script_lines(
        split_filenames(sorter, paths), middle_sep=middle_sep, destdir=destdir
    )
    if lines:
        return "\n".join(lines)


def _get_rearrange_script_lines(
    named_attribs: List,
    middle_sep: Optional[str] = None,
    destdir: Optional[os.PathLike] = None,
):
    destdir = Path(destdir) if destdir else "."
    script_lines = []
    for name, attribs in named_attribs:
        section_script_lines = []
        # name could be any key, possibly losing data
        name = name or "assorted"
        last_root = None
        last_dest = None
        for entry in attribs["files"]:
            original_labels = entry["original_labels"]
            original_path = entry["original_path"]
            assert (
                original_path.is_file()
            ), f"Refusing to operate on directory or non-existent file: {original_path}"
            new_root = Path()
            if attribs["tags"]:
                new_root /= join_tags(attribs["tags"], sep=",")
            if original_labels:
                if middle_sep is None:
                    new_root /= os.path.join(*original_labels).replace(" ", "_")
                else:
                    new_root /= middle_sep.join(original_labels).replace(" ", "_")
            elif name:
                new_root /= name.replace(" ", "_")
            same_root = new_root == last_root

            if entry["tags"]:
                new_subdir = join_tags(entry["tags"], sep=",")
            else:
                new_subdir = None

            filename = original_path.name
            new_path = new_root / (new_subdir or "") / filename
            same_dest = new_path.parent == last_dest

            if original_path != new_path:
                if not same_root:
                    section_script_lines += [
                        f"under=$DESTDIR/{shlex.quote(str(new_root))}"
                    ]
                if not same_dest:
                    section_script_lines += [
                        f'dest="$under"/{shlex.quote(str(new_subdir))}'
                        if new_subdir
                        else 'dest="$under"',
                        'mkdir -p "$dest"',
                    ]
                section_script_lines += [
                    f'  if [ -e {shlex.quote(str(original_path))} -a -d "$dest" ]; then',
                    f'    $MV {shlex.quote(str(original_path))} "$dest"',
                    "  fi",
                ]
                last_root = new_root
                last_dest = new_path.parent
        if section_script_lines:
            script_lines += [
                "# %s {{{" % name,
                *section_script_lines,
                "# }}} %s" % name,
                "",
            ]
    if script_lines:
        return [
            "#! /bin/sh",
            f"# created by {__name__} {__version__}",
            "set -e",
            f"DESTDIR={destdir}",
            "FIND='ionice find'  # Maybe gfind on Mac",
            "MV='mv -b'  # Maybe gmv on Mac",
            "",
            *script_lines,
            "",
            '$FIND -type d -empty -not -path "./.git/*" -delete',
            "# vim: foldmethod=marker",
        ]
