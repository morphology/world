"""
Config for semi-automatic directory organizer
"""
from datetime import datetime
from pathlib import Path

import yaml


def load_config(path="."):
    config_file = Path(path)
    if not config_file.is_file():
        raise ValueError("No config file given")
    parent = config_file.parent
    config = yaml.safe_load(config_file.open())
    return {
        "config_file": config_file,
        "parent": parent,
        "destdir": parent / datetime.now().strftime(config["timestamp_format"]),
        **config,
    }
