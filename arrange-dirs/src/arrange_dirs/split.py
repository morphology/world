import functools
import logging
import os
import re
from collections import Counter, defaultdict
from datetime import datetime
from pathlib import Path
from typing import Iterable, Optional

logger = logging.getLogger(__name__)

### Unique categories


class Category:
    def __init__(self, values: Iterable, weight=0, regex=None, assumes=(), removes=()):
        values = self.values = tuple(
            values.split(",") if isinstance(values, str) else values
        )
        if regex is None:
            regex = rf"({'|'.join(values)})"
        if isinstance(regex, str):
            regex = re.compile(rf"^{regex}$")
        self.regex = regex
        self.weight = float(weight)
        self.assumes = tuple(
            assumes.split(",") if isinstance(assumes, str) else assumes
        )
        self.removes = tuple(
            removes.split(",") if isinstance(removes, str) else removes
        )

    def get(self, text):
        return CommonTag(category=self, order=self.values.index(text))


class MutuallyExclusive(Category):
    def get(self, text):
        return MutuallyExclusiveTag(category=self, order=self.values.index(text))


class EarliestDate:
    def __init__(self, timestamp_format, weight=0, regex=None):
        self.regex = re.compile(regex)
        self.timestamp_format = timestamp_format
        self.weight = float(weight)
        self.assumes = ()
        self.removes = ()

    def get(self, text):
        try:
            return EarliestTag(
                category=self, dt=datetime.strptime(text, self.timestamp_format)
            )
        except ValueError as e:
            logger.debug(f"'{text}' failed to parse")
            logger.exception(e)
            return text


class RemoveCategory(Category):
    def get(self, text):
        _, _, tag = text.partition("no")
        return RemoveTag(category=self, removes_tag=tag)


### Wrappers for members


class Tag:
    """Abstract"""

    def __repr__(self):
        return f"<{str(self)}>"

    def __eq__(self, other):
        return str(self) == str(other)

    def collect(self, iterable):
        tag_list = list(iterable)
        if self.category.removes:
            for i, t in enumerate(tag_list):
                if str(t) in self.category.removes:
                    tag_list[i] = None
        category_member_indices = [
            i for i, t in enumerate(tag_list) if self.is_comparable(t)
        ]
        if category_member_indices:
            collected = sorted({tag_list[i] for i in category_member_indices})
            for i in category_member_indices:
                tag_list[i] = None
            insert_into = category_member_indices[0]
            tag_list = tag_list[:insert_into] + collected + tag_list[insert_into + 1 :]
        return [t for t in tag_list if t]


@functools.total_ordering
class EarliestTag(Tag):
    def __init__(self, category: EarliestDate, dt: datetime):
        self.category = category
        self.dt = dt

    def __str__(self):
        return self.dt.strftime(self.category.timestamp_format)

    def __gt__(self, other):
        if isinstance(other, EarliestTag):
            return self.dt > other.dt
        else:
            return -self.category.weight > -other.category.weight

    def __hash__(self):
        return hash(self.dt)

    def collect(self, iterable):
        tag_list = list(iterable)
        indices = [
            i for i, element in enumerate(tag_list) if isinstance(element, EarliestTag)
        ]
        if len(indices) > 1:
            tag_list[indices[0]] = min(tag_list[i] for i in indices)
            for i in indices[1:]:
                tag_list[i] = None
        return tag_list


@functools.total_ordering
class CommonTag(Tag):
    def __init__(self, category: Category, order: int):
        self.category = category
        self.order = order

    def __gt__(self, other):
        if isinstance(other, CommonTag):
            return (-self.category.weight, self.order) > (
                -other.category.weight,
                other.order,
            )
        else:
            return (-self.category.weight, str(self)) > (
                -other.category.weight,
                str(other),
            )

    def __hash__(self):
        return hash((id(self.category), self.order))

    def __str__(self):
        return self.category.values[self.order]

    def is_comparable(self, other):
        return isinstance(other, Tag) and self.category.weight == other.category.weight


class MutuallyExclusiveTag(CommonTag):
    def collect(self, iterable):
        tag_list = list(iterable)
        if self.category.removes:
            indices = [
                i
                for i in range(len(tag_list))
                if str(tag_list[i]) in self.category.removes
            ]
            for i in indices:
                tag_list[i] = None
        indices = [
            i for i, element in enumerate(tag_list) if self.is_comparable(element)
        ]
        if len(indices) > 1:
            tag_list[indices[0]] = tag_list[indices[-1]]
            for i in indices[1:]:
                tag_list[i] = None
        return tag_list


class RemoveTag(Tag):
    def __init__(self, category: Category, removes_tag: str):
        self.category = category
        self.removes_tag = removes_tag

    def __repr__(self) -> str:
        return f"<Remove {self.removes_tag}>"

    def is_comparable(self, other):
        return (self is other) or (str(other) == self.removes_tag)

    def collect(self, iterable):
        tag_list = list(iterable)
        # import pdb; pdb.set_trace()
        indices = [
            i for i, element in enumerate(tag_list) if self.is_comparable(element)
        ]
        for i in indices:
            tag_list[i] = None
        return tag_list


class Sorter:
    def __init__(self, definitions, num_major_categories=1):
        self.categories = []
        self.load_definitions(definitions, num_major_categories=num_major_categories)

    @functools.cache
    def parse_name(self, text):
        parts = text.split(",")
        new_parts = []
        for part in parts:
            new_part = part.strip("+")
            for category in self.categories:
                if m := category.regex.match(new_part):
                    if tag := category.get(new_part):
                        if category.assumes:
                            for text in category.assumes:
                                new_parts.extend(self.parse_name(text))
                        new_parts.append(tag)
                        break
            else:
                if part:
                    new_parts.append(part)
        assert None not in new_parts
        return new_parts

    def load_definitions(self, definitions, num_major_categories):
        ordered_entry_lists = [
            (num_major_categories - order, [e] if isinstance(e, (str, dict)) else e)
            for order, e in enumerate(definitions)
        ]
        for default_weight, entry_list in ordered_entry_lists:
            for entry in entry_list:
                if isinstance(entry, str):
                    values = entry
                    entry = {"type": "Common", "values": values}
                entry.setdefault("weight", default_weight)
                match entry.pop("type", "Common").split():
                    case ["Common"]:
                        assert all(entry["values"])
                        do_negate = entry.pop("negate", True)
                        category = Category(**entry)
                        if do_negate:
                            self.categories.append(
                                RemoveCategory(
                                    values=[f"no{v}" for v in category.values]
                                )
                            )
                    case ["MutuallyExclusive"]:
                        assert all(entry["values"])
                        category = MutuallyExclusive(**entry)
                    case ["EarliestDate"]:
                        category = EarliestDate(**entry)
                self.categories.append(category)

    def split_path(self, path):
        path = str(path)
        # Expand out the tags
        parts = path.split(os.path.sep)
        placeholders = []
        for part in parts:
            placeholders.extend(self.parse_name(part))
        # Apply each subclass
        for part in placeholders[:]:
            if isinstance(part, Tag):
                placeholders = part.collect([p for p in placeholders if p])
        return tuple(p for p in placeholders if p)


def split_filenames(sorter, paths):
    def key_func(entry):
        for label in entry["original_labels"]:
            if label != "assorted":
                return label.replace("_", " ")
        return ""

    group_entries = defaultdict(list)
    for p in paths:
        p = Path(p)
        labels, tags = [], []
        dir_tags = sorter.split_path(p.parent)
        if "delme" in dir_tags:
            continue
        for tag in dir_tags:
            if isinstance(tag, str):
                if tag not in labels:
                    labels.append(tag)
            else:
                tags.append(tag)
        entry = {
            "original_path": p,
            "original_tags": set(tags),
            "original_labels": labels,
        }
        group_entries[key_func(entry)].append(entry)
    for name, entries in group_entries.items():
        group_tags, tag_frequency = _get_group_tags(entries)
        for attribs in _split_file_sets(
            {"tags": group_tags, "tag_frequency": tag_frequency, "files": entries}
        ):
            yield (
                name
                or useful_filename_set(
                    [
                        e["original_path"].stem
                        for e in attribs["files"]
                        if not e["original_path"].name.startswith(".")
                    ]
                ),
                attribs,
            )


@functools.cache
def useful_filename(
    stem: str, sequence_pattern=re.compile(r"(?P<prefix>[^/]*)[ ._-]+(?P<order>[0-9]+)")
) -> Optional[str]:
    if stem.isdigit():
        return
    if m := sequence_pattern.search(stem):
        match prefix := m.group("prefix"):
            case "DCIM" | "IMG" | "Untitled":
                prefix = None
        if m.group("order"):
            return prefix
    return stem.capitalize()


def useful_filename_set(stems):
    name_patterns = Counter()
    for stem in stems:
        if t := useful_filename(stem):
            name_patterns[t] += 1
    logger.debug(f"{len(name_patterns)} different filename pattern(s)")
    return re.sub(r"[._ ]+", "_", os.path.commonprefix(name_patterns)).rstrip("_")


def _get_group_tags(entries):
    group_tags = set()
    tag_frequency = []
    for tag, freq in Counter(
        t for e in entries for t in e["original_tags"]
    ).most_common():
        if freq == len(entries):
            group_tags.add(tag)
        else:
            tag_frequency.append((tag, freq / len(entries)))
    for entry in entries:
        entry["tags"] = set(entry["original_tags"]) - group_tags
    return group_tags, tag_frequency


def _split_file_sets(attribs):
    if len(attribs["files"]) > 20:
        if attribs["tag_frequency"]:
            split_on, freq = attribs["tag_frequency"][0]
            if freq >= 0.5:
                entries_lhs, entries_rhs = [], []
                for entry in attribs["files"]:
                    (entries_lhs if split_on in entry["tags"] else entries_rhs).append(
                        entry
                    )
                tags_lhs, tag_freq_lhs = _get_group_tags(entries_lhs)
                tags_rhs, tag_freq_rhs = _get_group_tags(entries_rhs)
                return [
                    {
                        "tags": attribs["tags"] | tags_lhs,
                        "tag_frequency": tag_freq_lhs,
                        "files": entries_lhs,
                    },
                    {
                        "tags": attribs["tags"] | tags_rhs,
                        "tag_frequency": tag_freq_rhs,
                        "files": entries_rhs,
                    },
                ]
    return [attribs]


def join_tags(parts: Iterable, sep=os.path.sep):
    def key_func(arg):
        if isinstance(arg, Tag):
            return -arg.category.weight
        return 0

    binned_parts = defaultdict(list)
    for part in parts:
        if part:
            binned_parts[key_func(part)].append(part)
    new_parts = []
    for rank, items in sorted(binned_parts.items()):
        items.sort()
        if all(isinstance(t, Tag) for t in items):
            new_parts.append(",".join(str(t) for t in items))
        else:
            for t in items:
                if t:
                    new_parts.append(str(t))
    return sep.join(new_parts)
