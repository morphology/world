"""
Semi-automatic directory organizer
"""
from .rotate import rotate

__all__ = ["rotate"]
