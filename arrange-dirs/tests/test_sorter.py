import itertools
import os

import pytest

from arrange_dirs.split import *


@pytest.fixture
def toy_sorter():
    sorter = Sorter(
        definitions=[
            {
                "values": ["Documents", "Pictures", "Photos"],
                "type": "MutuallyExclusive",
                "weight": 1000,
            },
            {
                "type": "EarliestDate",
                "regex": r"\d{4}-\d{2}",
                "timestamp_format": "%Y-%m",
            },
            "cat,dog",
            "moose,squirrel",
        ]
    )
    return sorter


def test_sorter(toy_sorter):
    input_tags = "//2020-02/Documents/moose,squirrel".split("/")
    for split1 in itertools.combinations(input_tags, len(input_tags)):
        split2 = toy_sorter.split_path(os.path.join(*split1))
        assert join_tags(split2) == "Documents/2020-02/moose,squirrel"
    input_tags = "2020-02//Documents/foo/Pictures/1999-12/2020-12/bar".split("/")
    for split1 in itertools.combinations(input_tags, len(input_tags)):
        split2 = toy_sorter.split_path(os.path.join(*split1))
        assert join_tags(split2) == "Pictures/1999-12/foo/bar"


def test_unique(toy_sorter):
    input_tags = "//2020-02/Documents/moose,squirrel,moose/squirrel".split("/")
    for split1 in itertools.combinations(input_tags, len(input_tags)):
        split2 = toy_sorter.split_path(os.path.join(*split1))
        assert join_tags(split2) == "Documents/2020-02/moose,squirrel"


@pytest.fixture
def negation_sorter():
    sorter = Sorter(
        definitions=[
            {
                "values": ["Documents", "Pictures", "Photos"],
                "type": "MutuallyExclusive",
                "weight": 1000,
            },
            {
                "type": "EarliestDate",
                "regex": r"\d{4}-\d{2}",
                "timestamp_format": "%Y-%m",
            },
            "cat,dog",
            "moose,squirrel",
        ]
    )
    return sorter


def test_removal(negation_sorter):
    input_tags = "//2020-02/Documents/squirrel/nomoose/nomoose/nodog/nocat".split("/")
    for split1 in itertools.combinations(input_tags, len(input_tags)):
        split2 = negation_sorter.split_path(os.path.join(*split1))
        assert join_tags(split2) == "Documents/2020-02/squirrel"
    input_tags = "//2020-02/Documents/moose,squirrel/nomoose/moose".split("/")
    for split1 in itertools.combinations(input_tags, len(input_tags)):
        split2 = negation_sorter.split_path(os.path.join(*split1))
        assert join_tags(split2) == "Documents/2020-02/squirrel"
    input_tags = "2020-02//Documents/foo/Pictures/1999-12/2020-12/bar/nobar".split("/")
    for split1 in itertools.combinations(input_tags, len(input_tags)):
        split2 = negation_sorter.split_path(os.path.join(*split1))
        assert join_tags(split2) == "Pictures/1999-12/foo/bar/nobar"
