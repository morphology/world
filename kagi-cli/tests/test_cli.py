import sys

from kagi_cli import cli

from .conftest import *


def test_cli_pdf_many(capsys, monkeypatch):
    paths = [str(p) for p in TEST_INPUTS.glob("good/*.pdf")]
    monkeypatch.setattr(sys, "argv", ["progname", "summarize", *paths])
    cli.main()
    out, err = capsys.readouterr()
    assert out.strip() and not err.strip()
