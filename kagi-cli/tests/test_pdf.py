import logging

import pytest
import rich.console
from kagi_cli import pdf_to_md

from .conftest import *

logger = logging.getLogger(__name__)


@pytest.mark.parametrize("pdf_path", list(TEST_INPUTS.glob("good/*.pdf")))
def test_pdf_conversion(pdf_path, tmp_path):
    console = rich.console.Console(stderr=True)
    pdf_to_md(pdf_path, output_path=tmp_path, console=console)
    n_md = sum(1 for _ in tmp_path.glob("*.md"))
    assert n_md == 1
