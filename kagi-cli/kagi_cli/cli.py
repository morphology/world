import argparse
import contextlib
import json
import logging
import socket
import sys
import tempfile
import time
import urllib.parse
import warnings
from pathlib import Path

import ocr.cli
import ocr.tesseract
import xmlrpc.client
from kagiapi import KagiClient
from rich.console import Console
from rich.logging import RichHandler
from rich_argparse import RawDescriptionRichHelpFormatter
from unoserver.client import UnoClient
from unoserver.server import UnoServer

from . import docx_to_md, pdf_to_md
from ._version import __version__

logger = logging.getLogger(__package__)
KAGI_RUNNER = KagiClient()


def _local_file_or_url(text) -> Path | urllib.parse.SplitResult:
    if "://" in text:
        url = urllib.parse.urlsplit(text)
        if url.scheme == "file":
            path = urllib.parse.unquote(url.path)
            return Path(path).resolve()
        else:
            return url
    return Path(text)


def run_summarize(args, console, workdir=None):
    if not workdir:
        workdir = tempfile.mkdtemp()
    workdir = Path(workdir)
    file_args, image_args, url_args = [], [], []
    uno_server = None
    uno_client = None
    for path in args.paths:
        if isinstance(path, urllib.parse.SplitResult):
            url_args.append(path.geturl())
        else:
            match path.suffix.lower():
                case ".jpeg" | ".jpg" | ".png":
                    image_args.append(path)
                case ".docx" | ".md" | ".pdf" | ".txt":
                    file_args.append(path)
                case ".pptx":
                    file_args.append(path)
                    uno_server = uno_server or start_unoserver(workdir / "libreoffice")
                    uno_client = uno_client or UnoClient(server=uno_server.interface, port=uno_server.port)
                case _:
                    logger.info(f"Ignoring {path}")
    results = {}
    for url in url_args:
        results[url] = KAGI_RUNNER.summarize(url=str(url), cache=True)
    for arg_n, path in enumerate(file_args):
        options = None
        match path.suffix.lower():
            case ".docx":
                output_dir = docx_to_md(path, output_dir=workdir)
                for md_path in output_dir.glob("*.md"):
                    results[md_path] = KAGI_RUNNER.summarize(text=md_path.read_text(), cache=True)
            case ".md" | ".txt":
                results[path] = KAGI_RUNNER.summarize(text=path.read_text(), cache=True)
            case ".pdf":
                if (options_file := Path(f"{path}.json")).exists():
                    with options_file.open() as fo:
                        options = json.load(fo)
                output_dir = pdf_to_md(path, output_path=workdir, console=console, options=options)
                for md_path in output_dir.glob("*.md"):
                    results[md_path] = KAGI_RUNNER.summarize(text=md_path.read_text(), cache=True)
            case ".pptx":
                document_name = path.stem
                tmp_path = workdir / f"converted-{arg_n:03d}.pdf"
                for retry in range(3):
                    try:
                        uno_client.convert(
                            inpath=path,
                            outpath=tmp_path,
                            convert_to="pdf",
                        )
                        break
                    except xmlrpc.client.Error as e:
                        logger.debug(f"Retrying after {retry}: {e}")
                        time.sleep(1.)
                else:
                    raise TimeoutError(f"Failed to connect to {uno_server=}")
                output_dir = pdf_to_md(
                    input_path=tmp_path,
                    output_path=workdir / f"{document_name.replace(' ', '_')}.md",
                    console=console,
                )
                for md_path in output_dir.glob("*.md"):
                    results[md_path] = KAGI_RUNNER.summarize(text=md_path.read_text(), cache=True)
    if image_args:
        for path, ocr_result in sorted(ocr.tesseract.ocr_many(image_args).items()):
            paragraphs = []
            for _, paragraph in ocr_result.paragraphs.items():
                for detections in paragraph:
                    words = [w.text for w in detections if w]
                    if words:
                        paragraphs.append(" ".join(words))
            if paragraphs:
                results[path] = KAGI_RUNNER.summarize(text="\n\n".join(paragraphs), cache=True)
    match len(results):
        case 0:
            sys.exit(f"No results for {len(args.paths)} input(s)")
        case 1:
            show_filename = False
        case _:
            show_filename = True
    with console.pager(styles=bool(console.is_interactive)):
        for path, result in results.items():
            if show_filename:
                if console.is_interactive:
                    console.rule(path.name)
                else:
                    console.print()
                    console.print(path.name)
            console.print(result["data"]["output"])
            console.print()
    if uno_server:
        uno_server.stop()


def find_unused_port(start_port=49152, end_port=65535):
    for port in range(start_port, end_port + 1):
        with contextlib.closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            if sock.connect_ex(("localhost", port)) != 0:
                return port


def start_unoserver(user_installation):
    port = find_unused_port()
    user_installation = Path(user_installation)
    user_installation.mkdir(exist_ok=True)
    server = UnoServer(port=port, user_installation=user_installation.as_uri())
    server.start()
    with contextlib.closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        for retry in range(16):
            if sock.connect_ex((server.interface, int(server.port))) == 0:
                logger.debug(f"Connected to {server=}")
                return server
            time.sleep(1.0)


def setup_parser(parser: argparse.ArgumentParser, command):
    match command:
        case None:
            # Common
            sg = parser.add_argument_group("Common to all functions")
            sg.add_argument("--version", action="version", version=f"%(prog)s {__version__}")
        case "summarize":
            parser.add_argument(
                "paths", nargs="+" if sys.stdin.isatty() else "*", metavar="FILE or URL", type=_local_file_or_url
            )
            libreoffice_options = parser.add_argument_group("LibreOffice connection (for slideshows)")
            libreoffice_options.add_argument("--uno-host", default="localhost")
            libreoffice_options.add_argument("--uno-port", default=2003, type=int)
            parser.set_defaults(from_stdin=not sys.stdin.isatty())


def main():
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    warnings.simplefilter("ignore", ResourceWarning)
    console = Console(stderr=True, record=True)
    logging.basicConfig(level=logging.DEBUG if __debug__ else logging.WARNING, handlers=[RichHandler(console=console)])
    parser = argparse.ArgumentParser(formatter_class=RawDescriptionRichHelpFormatter, description=__doc__)
    subparsers = parser.add_subparsers(dest="command")
    ocr.cli.setup_parser(subparsers.add_parser("ocr", formatter_class=parser.formatter_class), "detect")
    setup_parser(subparsers.add_parser("summarize", formatter_class=parser.formatter_class), "summarize")
    args = parser.parse_args()

    match args.command:
        case "ocr":
            ocr.cli.run_detect(args, console=console)
        case "summarize":
            with tempfile.TemporaryDirectory() as td:
                run_summarize(args, console=console, workdir=td)
        case _:
            raise NotImplementedError(args.command)


if __name__ == "__main__":
    sys.exit(main())
