import gzip
import logging
import os
from pathlib import Path
from subprocess import run
from typing import Optional, Union, Iterable

import fitz  # pymupdf
from fitz import Rect
from rich.console import Console
from rich.progress import Progress

from .pdf_formatting import maybe_unordered_list

logger = logging.getLogger(__name__)
ALL = object()
HORIZONTAL_LINE = "- - -"


def _process_pdf(
    path: os.PathLike,
    output_dir: os.PathLike,
    console: Console,
    pages=ALL,
    pages_with_raster=ALL,
    pages_with_vector=None,
    redact=(),
    bound: Optional[tuple] = None,
    write_pdf: Union[bool, os.PathLike] = False,
):
    path = Path(path)
    output_dir = Path(output_dir)
    if isinstance(bound, (float, int)):
        bound = Rect(+1, +1, -1, -1) * bound
    if write_pdf:
        write_pdf = Path(write_pdf)
        assert write_pdf.parent.exists()
        if write_pdf.exists():
            write_pdf.rename(f"{write_pdf}~")

    seen_xrefs = set()

    with fitz.open(path) as fi:
        all_page_numbers = list(range(1, fi.page_count + 1))
        n_digits = len(str(fi.page_count + 1))
        if pages is ALL:
            pages = all_page_numbers
        if pages:
            if pages_with_raster is ALL:
                pages_with_raster = all_page_numbers
            pages_with_raster = pages_with_raster or {}
            if not isinstance(pages_with_raster, dict):
                # Extension decided later
                pages_with_raster = {page_n: f"image-{page_n:0{n_digits}}" for page_n in pages_with_raster}

            if pages_with_vector is ALL:
                pages_with_vector = all_page_numbers
            pages_with_vector = pages_with_vector or {}
            if not isinstance(pages_with_vector, dict):
                pages_with_vector = {page_n: f"whole-page-{page_n:0{n_digits}}.svgz" for page_n in pages_with_vector}

        # Redaction is TODO, likely on a style-by-style basis for different documents
        with (progress := Progress(console=console, transient=True)):
            for page_n in progress.track(pages, description="Reading pages"):
                raster_prefix = (output_dir / pages_with_raster[page_n]) if page_n in pages_with_raster else None
                vector_path = (output_dir / pages_with_vector[page_n]) if page_n in pages_with_vector else None
                page = fi[page_n - 1]

                items_by_region = []
                n_redactions = 0
                page_bound = (page.rect + bound) if bound else None
                for x0, y0, x1, y1, text, block_no, block_type in fi.get_page_text(
                    page_n - 1,
                    "blocks",
                    clip=page_bound,
                ):
                    rect = Rect(x0, y0, x1, y1)
                    # Only an example of redaction:
                    if text.strip() in redact:
                        page.add_redact_annot(rect)
                        n_redactions += 1
                        continue
                    block_type = ["text", "image"][block_type]
                    if block_type == "text":
                        markdown = ""
                        for line in text.splitlines():
                            # Maybe insert anchors for each paragraph
                            if line.strip():
                                markdown += maybe_unordered_list(line)
                        items_by_region.append(
                            (
                                rect,
                                {
                                    "markdown": markdown,
                                    "text": text,
                                },
                            )
                        )
                    elif block_type == "image":
                        items_by_region.append(
                            (
                                rect,
                                {
                                    "text": text,
                                },
                            )
                        )
                if n_redactions > 0:
                    page.apply_redactions()

                if vector_path:
                    svg_content = fi[page_n - 1].get_svg_image(text_as_path=True)
                    with gzip.open(vector_path, "w") as fo:
                        fo.write(svg_content.encode())
                    items_by_region.append((None, {"path": vector_path}))
                if raster_prefix:
                    for (
                        xref,
                        smask,
                        width,
                        height,
                        bpc,
                        colorspace,
                        alt_colorspace,
                        name,
                        image_filter,
                    ) in fi.get_page_images(page_n - 1):
                        if xref not in seen_xrefs:
                            seen_xrefs.add(xref)
                            rects = fi[page_n].get_image_rects(xref)
                            rect = min(rects) if rects else None
                            image = fi.extract_image(xref)
                            ext = image["ext"]
                            raster_path = Path(f"{raster_prefix}-{xref}.{ext}")
                            raster_path.write_bytes(image["image"])
                            items_by_region.append(
                                (
                                    rect,
                                    {
                                        "name": name,
                                        "path": raster_path,
                                    },
                                )
                            )
                yield (
                    {
                        "page": page_n,
                        "label": page.get_label().strip(),
                        "dimensions": page.rect,
                    },
                    items_by_region,
                )
        if write_pdf:
            with console.status(f"Saving '{write_pdf}'"):
                fi.save(
                    write_pdf,
                    garbage=3,
                    deflate=True,
                    clean=True,
                    no_new_id=True,
                    encryption=fitz.PDF_ENCRYPT_NONE,
                )


def pdf_to_md(
    input_path: os.PathLike,
    console: Console,
    output_path: Optional[os.PathLike] = None,
    options: Optional[dict] = None,
):
    input_path = Path(input_path)
    name = input_path.stem
    output_dir = None
    if output_path:
        output_path = Path(output_path)
        if output_path.is_dir():
            output_dir, output_path = output_path, None
        else:
            output_dir = output_path.parent
    output_dir = output_dir or (input_path.parent / name.replace(" ", "_"))
    output_path = output_path or (output_dir / f"{name.replace(' ', '_')}.md")
    if output_path.exists():
        output_path.rename(f"{output_path}~")
    options = options or {}

    document_name = input_path.name
    markdown = ""
    markdown_toc = ""
    page_number_offset = options.pop("page_number_offset", 0)
    cover_page_n = 0
    for page_info, items_by_region in _process_pdf(input_path, output_dir=output_dir, console=console, **options):
        label = page_info.get("label")
        if not label:
            display_page_number = page_info["page"] + page_number_offset
            if display_page_number > 0:
                label = f"page {display_page_number}"
            else:
                label = f"cover {cover_page_n}"
                cover_page_n += 1
        anchor = label.replace(" ", "-")
        dimensions = (page_info["dimensions"].y1, page_info["dimensions"].x1)
        if items_by_region:
            markdown += f"<a id='{anchor}'></a>\n\n"
            items_by_region.sort(key=lambda pair: (pair[0] or Rect(0, 0, 0, 0)).y0)
            n_paragraphs = 0
            n_images = 0
            for _, item in items_by_region:
                if "markdown" in item:
                    markdown += item["markdown"]
                    n_paragraphs += 1
                if "path" in item:
                    path = item["path"]
                    name = item.get("name") or path.stem
                    markdown += f"![{name}]({path.name})\n\n"
                    n_images += 1
            markdown_toc += f"[{label}](#{anchor}) {dimensions} {n_paragraphs} paragraph(s), {n_images} images\n\n"
    with output_path.open("w") as fo:
        fo.write(f"# {document_name}\n\n")
        if markdown_toc:
            fo.write(markdown_toc)
            fo.write("\n")
        if markdown:
            fo.write(markdown)
    return output_dir


def docx_to_md(
    input_path,
    output_dir: Optional[os.PathLike] = None,
    pandoc_args: Iterable = ("--to=gfm+attributes+rebase_relative_paths", "--standalone"),
):
    input_path = Path(input_path)
    name = input_path.stem
    output_dir = output_dir or (input_path.parent / name.replace(" ", "_"))
    if output_dir.exists():
        output_dir.rename(f"{output_dir}~")
        output_dir.mkdir()
    output_path = output_dir / f"{name.replace(' ', '_')}.md"
    if output_path.exists():
        output_path.rename(f"{output_path}~")
    command = [
        "pandoc",
        input_path,
        *pandoc_args,
        f"--extract-media={output_dir}",
        "-o",
        output_path,
    ]
    logger.debug(f"Running {command}")
    run(command, check=True)
    return output_dir
