import locale

locale.setlocale(locale.LC_ALL, "en_US.UTF-8")

exchange_rates = CurrencyRates().get_rates("USD")

for symbol, value in exchange_rates.items():
    print(symbol, locale.currency(value))
