
ureg = pint.UnitRegistry(autoconvert_offset_to_baseunit=True)
Q_ = ureg.Quantity

rad = ureg['radian']
deg = ureg['degree']

y = year = years = ureg['year']
month = months = ureg['month']
day = days = ureg['day']
h = hour = hours = ureg['hour']
minute = minutes = ureg['minute']
s = second = seconds = ureg['seconds']
ms = ureg['millisecond']
microsecond = ureg['microsecond']
ns = ureg['nanosecond']

mg = ureg['milligram']
g = ureg['gram']
kg = ureg['kilogram']
lb = ureg['pound']
short_ton = ton = ureg['ton']
long_ton = tonne = ureg['tonne']

mi = mile = miles = ureg['mile']
km = ureg['kilometer']
m = meter = metre = ureg['meter']
cm = ureg['centimeter']
mm = ureg['millimeter']
inch = inches = ureg['inch']
foot = feet = ureg['foot']

degC = ureg['degC']
degF = ureg['degF']
degK = ureg['degK']

ohm = ohms = ureg['ohm']
amp = amps = ureg['ampere']
watt = watts = ureg['watt']
volt = volts = ureg['volt']
kwh = Q_(1000, 'watt_hour')

acre = acres = ureg['acre']

cc = ureg['cubic_centimeters']
gal = gallon = ureg['gallon']
l = liter = litre = ureg['liter']
ml = ureg['ml']

psi = ureg['psi']
atm = ureg['atm']
mbar = ureg['mbar']
kpa = ureg['kilopascal']

N_A = ureg['avogadro_number']

tsp = ureg['teaspoon']
tbl = ureg['tablespoon']
cup = ureg['liquid_cup']
fl_oz = ureg['fluid_ounce']
