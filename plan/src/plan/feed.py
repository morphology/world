from datetime import datetime, timedelta
from textwrap import wrap

from bs4 import BeautifulSoup
import feedparser


def get_GitHub_entries(username, since=timedelta(days=7)):
    assert username
    entries = feedparser.parse(f"https://github.com/{username}.atom").entries
    for e in entries:
        if not isinstance(e.published, datetime):
            e.published = datetime.strptime(e.published, "%Y-%m-%dT%H:%M:%SZ")
    if isinstance(since, dict):
        since = timedelta(**since)
    if isinstance(since, timedelta):
        since = datetime.now() - since
    return [
        (e.published, e.title, condense_summary(e.summary))
        for e in entries
        if e.published > since
    ]


def condense_summary(summary_html, width=80):
    return "\n".join(
        wrap(
            " ".join(BeautifulSoup(summary_html, "lxml").stripped_strings),
            width=width,
        )
    )
