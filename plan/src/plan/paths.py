#! /usr/bin/env python3
import os
from pathlib import Path

PLAN_PATH = Path("~/.plan").expanduser()
PROJECT_PATH = Path("~/.project").expanduser()
