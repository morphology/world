#! /usr/bin/env python3
"""
Edit your project and plan.
"""
import argparse
from datetime import datetime, timedelta
import os
from pathlib import Path
import sys

NOW = datetime.now()

from . import TrackerEntry
from .feed import get_GitHub_entries
from .terminal import set_title


def _get_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__
    )
    parser.add_argument("mode", choices=["edit", "push", "show"])
    parser.add_argument(
        "--from-github",
        action="store_true",
        help="Include recent GitHub activity into .plan",
    )
    parser.add_argument(
        "--wiki",
        type=Path,
        default=os.getenv("VIMWIKI_DIR"),
        help="Path to vimwiki directory; previous contents dumped here",
    )
    return parser


def main():
    args = _get_parser().parse_args()
    if args.wiki:
        wiki = Path(args.wiki) / "diary" / f"{NOW:%Y-%m-%d}.wiki"
    else:
        wiki = None

    t = TrackerEntry()
    if args.mode == "edit":
        if wiki:
            t.publish_to_vimwiki(wiki)
        t.input_project()
        if args.from_github:
            since = t.timestamp or timedelta(days=7)

            for timestamp, title, text in sorted(
                get_GitHub_entries(username=os.getenv("GITHUB_USERNAME"), since=since)
            ):
                ago = NOW - timestamp
                lines = [title, "-" * len(title), "", f"{ago.days} day(s) ago", text]
                t.plan_contents += "\n".join(lines) + "\n"

        else:
            t.input_plan()
        set_title(str(t))
        t.write()
    elif args.mode == "push":
        t.publish_to_vimwiki(wiki)
    elif t:
        print(t)


sys.exit(main())
