from contextlib import contextmanager, nullcontext
import ctypes
from pathlib import Path
import sys

if sys.platform == "win32":
    readline = None

    def set_title(text):
        kernel32 = ctypes.WinDLL("kernel32", use_last_error=True)
        kernel32.SetConsoleTitleW(text)

else:
    import readline

    def set_title(text):
        assert sys.stdout.isatty()
        sys.stdout.write("\x1b]2;" + text + "\x07")


if readline:

    @contextmanager
    def readline_history(histfile):
        if histfile:
            histfile = Path(histfile)
            if histfile.exists():
                readline.read_history_file(histfile)
        yield
        if histfile:
            readline.write_history_file(histfile)
        readline.clear_history()

else:
    readline_history = nullcontext
