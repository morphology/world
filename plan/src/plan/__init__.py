#! /usr/bin/env python3
from datetime import datetime
from pathlib import Path

from .paths import PLAN_PATH, PROJECT_PATH
from .terminal import readline_history


class TrackerEntry:
    plan_history_path = Path("~/.plan_history").expanduser()
    project_history_path = Path("~/.project_history").expanduser()
    sig_path = Path("~/.sig").expanduser()

    @property
    def timestamp(self):
        if PLAN_PATH.exists():
            return datetime.fromtimestamp(PLAN_PATH.stat().st_mtime)

    def __init__(self):
        if PLAN_PATH.exists():
            self.plan_contents = PLAN_PATH.read_text().strip()
        else:
            self.plan_contents = ""
        if PROJECT_PATH.exists():
            self.project_contents = PROJECT_PATH.read_text().strip()
        else:
            self.project_contents = ""

    def get_markdown(self, level=1):
        lines = []
        if self.project_contents:
            lines.append(f"{'#'*level} {self.project_contents}")
        if self.timestamp:
            ago = datetime.now() - self.timestamp
            lines.append("")
            lines.append(f"For {ago}")
        if self.plan_contents:
            if lines:
                lines.append("")
            lines.append(self.plan_contents)
        return "\n".join(lines)

    def get_hashtag(self):
        return f"#{self.project_contents.replace(' ', '')}"

    def __bool__(self):
        return bool(self.project_contents and self.timestamp)

    def __repr__(self):
        if self:
            return f"<{self.__class__.__name__} {self.get_hashtag()} since {self.timestamp}>"
        else:
            return f"<{self.__class__.__name__} empty>"

    def input_project(self):
        with readline_history(self.project_history_path):
            line = input("project: ")
            if line:
                self.project_contents = line

    def input_plan(self):
        with readline_history(self.plan_history_path):
            lines = []
            while line := input("plan (End with blank line): ").strip():
                lines.append(line)
            if lines:
                self.plan_contents = "\n".join(lines)

    def write(self):
        PROJECT_PATH.write_text(f"{self.project_contents.rstrip()}\n")
        PLAN_PATH.write_text(f"{self.plan_contents.rstrip()}\n")

    def set_signature(self, parent=Path("~/.signatures").expanduser()):
        stem = self.project_contents.replace(" ", "")
        if (specific_signature := parent / f"{stem}.html").exists():
            self.sig_path.unlink(missing_ok=True)
            return self.sig_path.write_text(specific_signature.read_text())

    def publish_to_vimwiki(self, vimwiki_path):
        assert vimwiki_path
        vimwiki_path = Path(vimwiki_path)
        vimwiki_path.parent.mkdir(exist_ok=True)
        level = 1 if not vimwiki_path.exists() else 2
        with vimwiki_path.open("a") as fo:
            fo.write(f"\n{self.get_markdown(level=level)}\n")
