import argparse
import logging
import sys
import warnings
from pathlib import Path

from rich.console import Console
from rich.logging import RichHandler
from rich_argparse import RawDescriptionRichHelpFormatter

from . import RealDebridClient, info_from_file, TorrentInfo
from ._version import __version__
from .magnet import Magnet

logger = logging.getLogger(__package__)


def _local_file_or_magnet(text) -> Path | Magnet:
    if text.startswith("magnet:"):
        return Magnet(text)
    if Path(text).exists():
        return Path(text)
    raise ValueError(text)


def setup_parser(parser: argparse.ArgumentParser, command):
    match command:
        case None:
            # Common
            sg = parser.add_argument_group("Common to all functions")
            sg.add_argument("--version", action="version", version=f"%(prog)s {__version__}")
        case "available":
            parser.add_argument(
                "paths", nargs="+" if sys.stdin.isatty() else "*", metavar="FILE or URL", type=_local_file_or_magnet
            )


def main():
    logging.getLogger("httpcore").setLevel(logging.WARNING)
    logging.getLogger("httpx").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    warnings.simplefilter("ignore", ResourceWarning)
    console = Console(stderr=True)
    logging.basicConfig(level=logging.DEBUG if __debug__ else logging.WARNING, handlers=[RichHandler(console=console)])
    parser = argparse.ArgumentParser(formatter_class=RawDescriptionRichHelpFormatter, description=__doc__)
    setup_parser(parser, None)
    subparsers = parser.add_subparsers(dest="command")
    setup_parser(subparsers.add_parser("available", formatter_class=parser.formatter_class), "available")
    setup_parser(subparsers.add_parser("limit", formatter_class=parser.formatter_class), "limit")
    args = parser.parse_args()

    match args.command:
        case "available":
            check_availability(args, console=console)
        case "limit":
            check_limit(console=console)
        case _:
            raise NotImplementedError(args.command)


def check_availability(args, console):
    inputs = list(args.paths)
    torrent_specs = {}
    if sys.stdin.isatty():
        console.print("<End with Ctrl-D>")
    for line in sys.stdin.read().splitlines():
        if not line.strip():
            continue
        elif line.startswith("magnet:"):
            inputs.append(Magnet(line))
        elif Path(line).exists():
            inputs.append(Path(line))
        else:
            raise ValueError(line)
    assert inputs
    for arg in inputs:
        if isinstance(arg, Magnet):
            torrent_specs[arg] = arg.get_info_hash()
        elif isinstance(arg, Path):
            torrent_specs[arg] = info_from_file(arg)
        elif isinstance(arg, TorrentInfo):
            torrent_specs[arg] = arg["info_hash"]
        else:
            raise NotImplementedError(arg)
    client = RealDebridClient()
    availability = client.get_instant_availability(torrent_specs.values())
    logger.debug(f"{len(availability)} results")
    for arg, info_hash in torrent_specs.items():
        if file_selections := availability.pop((info_hash.lower(), "rd"), None):
            file_selections.sort(key=lambda selections: -sum(e.filesize for e in selections.values()))
            console.print(arg, justify="center")
            for selection in file_selections:
                console.print(" ", ",".join(str(i) for i in sorted(selection)))
                for entry in selection.values():
                    console.print(f"    {entry.filename} = " f"{entry.filesize:,} bytes")
            console.print()
    if availability:
        logger.debug(f"Ignoring {availability=}")


def check_limit(console):
    num, denom = RealDebridClient().get_capacity()
    if num < denom:
        console.print(f"Torrent limit: [green]{num}[/]/{denom}")
    else:
        console.print(f"Torrent limit reached: [red]{num}[/]/{denom}")
        sys.exit(10)
