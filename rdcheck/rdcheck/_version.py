from importlib.metadata import version

try:
    __version__ = version("rdcheck")
except:
    __version__ = None
