import hashlib
import logging
import os
from collections import defaultdict
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from typing import Iterable

import httpx
from fastbencode import bencode, bdecode

from .magnet import Magnet

logger = logging.getLogger(__name__)


def info_from_file(path):
    torrent = bdecode(Path(path).read_bytes())
    return hashlib.sha1(bencode(torrent[b"info"])).hexdigest()


class TorrentInfo(dict):
    pass


@dataclass
class FileEntry:
    filename: str
    filesize: int


class FileSelection(dict):
    pass


class RealDebridClient:
    def __init__(self, api_token=None):
        headers = {}
        self.token = api_token or os.getenv("REALDEBRID_TOKEN")
        if self.token:
            headers["Authorization"] = f"Bearer {self.token}"
        self.client = httpx.Client(
            headers={"Accept": "application/json", **headers},
            base_url="https://api.real-debrid.com/rest/1.0/",
        )

    def get_capacity(self):
        response = self.client.get("/torrents/activeCount")
        response.raise_for_status()
        result = response.json()
        return result.pop("nb"), result.pop("limit")

    def get_instant_availability(self, info_hashes: Iterable[str]):
        assert not isinstance(info_hashes, str)
        logger.debug(f"Checking {len(info_hashes)} info_hash: {info_hashes}")
        path = "/torrents/instantAvailability/" + ("/".join(info_hashes))
        response = self.client.get(path)
        response.raise_for_status()
        results = defaultdict(list)
        for info_hash, available_hosters in response.json().items():
            if isinstance(available_hosters, dict):
                for hoster, selected_files_variants in available_hosters.items():
                    if selected_files_variants:
                        for selected_files in selected_files_variants:
                            results[info_hash, hoster].append(
                                FileSelection((int(order), FileEntry(**attribs)) for
                                              order, attribs in selected_files.items())
                            )
            else:
                logger.debug(f"Ignoring {info_hash=}: {available_hosters}")
        return dict(results)

    def get_user_torrents(self):
        response = self.client.get("/torrents")
        response.raise_for_status()
        torrents = {}
        for torrent_info in response.json():
            torrent_id = torrent_info["id"]
            info_hash = torrent_info.pop("hash", None)
            try:
                torrent_info["added"] = datetime.fromisoformat(torrent_info["added"])
            except:
                pass
            try:
                torrent_info["ended"] = datetime.fromisoformat(torrent_info["ended"])
            except:
                pass
            torrents[torrent_id] = TorrentInfo(torrent_info, info_hash=info_hash)
        return torrents
