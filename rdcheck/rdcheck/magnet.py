import urllib.parse


class Magnet:
    def __init__(self, text, include_trackers=False) -> None:
        uri = urllib.parse.urlsplit(text)
        self._schema = uri.scheme
        fields = urllib.parse.parse_qs(uri.query)
        if not include_trackers:
            fields.pop("tr", ())
        (xt,) = fields.pop("xt")
        self.fields = fields
        self.urn_type, _, self.urn_value = xt.rpartition(":")

    def __str__(self):
        query_string = urllib.parse.urlencode(self.fields, doseq=True)
        return f"{self._schema}:?xt={self.urn_type}:{self.urn_value}&{query_string}"

    def get_info_hash(self) -> str:
        if self.urn_type == "urn:btih":
            return self.urn_value

    def __repr__(self):
        return f"<Magnet link {str(self)}>"
