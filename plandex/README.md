Runs on 8080 by default.

0. Check rules

	netsh interface portproxy dump

1. Add proxy

	netsh interface portproxy add v4tov4 listenport=8080 connectport=8080 connectaddress=172...

2. Open firewall

	netsh advfirewall firewall add rule name= "Open Port 8080" dir=in action=allow protocol=TCP localport=8080

3. Tear down proxy

	netsh interface portproxy delete v4tov4 listenport=8080

4. Tear down firewall rule

	netsh advfirewall firewall delete rule name="Open port 8080"
