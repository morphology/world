#! /bin/sh
set -e
mode="$1"
command shift

DB=example.db
case $mode in
  "change-account-address")
    account_num="$1"
    sqlite3 "$DB" "update accounts set service_address = edit(service_address) where account_num = ${account_num};"
    ;;
  "change-contact-details")
    account_num="$1"
    sqlite3 "$DB" "update billing set contacts = edit(contacts) where account_num = ${account_num};"
    ;;
  "create-monthly-bill")
    date=$( date +'%Y_%m' )
    table_name="bills_${date}"
    sqlite3 "$DB" "create table ${table_name} as select * from monthly_bill;"
    ;;
  "get-email-list")
    sqlite3 "$DB" ".mode markdown" "select * from billing_email;"
    ;;
  "get-phone-list")
    sqlite3 "$DB" ".mode markdown" "select * from billing_contacts;"
    ;;
  *) echo "'$mode' not understood" >&2 ;;
esac