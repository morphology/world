BEGIN TRANSACTION;
CREATE TABLE accounts
(
    account_num     integer primary key,
    service_address text not null,
    note            text
);

CREATE TABLE billing
(
    account_num     integer not null,
    account_name    text    not null,
    contacts        JSON,
    billing_address JSON,
    note            text,

    foreign key (account_num) references accounts (account_num)
);

CREATE TABLE readings
(
    read_date   DATETIME not null default CURRENT_TIMESTAMP,
    account_num integer  not null,
    meter_num   text     not null,
    reading     real,
    meter_reset boolean,
    note        text,

    foreign key (account_num) references accounts (account_num)
);

CREATE INDEX readings_idx on readings (read_date, account_num);

CREATE VIEW reconciled_readings as
select *
from readings
where rowid in (select max(rowid) over (partition by read_date, account_num) from readings)
order by read_date, account_num desc;

CREATE VIEW monthly_usage as
select *, reading + (30.5 * usage / billed_days) as next_month_expected, (10 / 325851.4) *
        usage as usage_af, 10 * usage / billed_days as usage_gpd
            from (select *,
            reading - previous_reading as usage,
            julianday(read_date) - julianday(previous_read_date) as billed_days
            from (select *,
                lag(read_date) over (partition by account_num) as previous_read_date,
                lag(reading) over (partition by account_num) as previous_reading
                from reconciled_readings
                where not meter_reset)
            where previous_read_date is not NULL
            and previous_reading is not NULL);

create view billing_contacts as
select account_num,
       account_name,
       coalesce(json_each.key, 'phone') as key,
       json_each.value as phone,
       note
from billing, json_each(contacts)
where json_each.key like '%phone%'
   or json_each.key is null;

create view billing_email as
select account_num,
       account_name,
       json_each.key,
       json_each.value as email,
       note
from billing, json_each(contacts)
where json_each.key like '%email%';

create view monthly_bill as
select billing.account_num,
       billing.note,
       billing.account_name,
       billing.billing_address,
       monthly_usage.usage_gpd,
       monthly_usage.previous_read_date,
       monthly_usage.read_date,
       json_object('fixed', 125, 'well fee', 15, 'variable', 100 * monthly_usage.usage_af) as charges
from monthly_usage
         inner join billing using (account_num);
COMMIT;
