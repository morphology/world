#! /usr/bin/make -rf

origin := 'git+https://gitlab.com/morphology/personal-utilities.git'

all : ${HOME}/bin/arrange-dirs.pyz ${HOME}/.local/bin/pdf2md ${HOME}/bin/scene-split.pyz ${HOME}/bin/world

%/arrange-dirs.pyz :
	shiv -o "$@" '${origin}#egg=arrange-dirs&subdirectory=arrange-dirs/'

%/pdf2md.pyz :
	pip install --user '${origin}#egg=pdf2md&subdirectory=pdf2md/'

%/rdcheck.pyz :
	shiv -o "$@" -c rdcheck '${origin}#egg=rdcheck&subdirectory=rdcheck/'

%/scene-split.pyz :
	shiv -o "$@" '${origin}#egg=scene-split&subdirectory=scene-split/'

%/unjoin-video.pyz :
	shiv -o "$@" '${origin}#egg=unjoin_video&subdirectory=unjoin-video/'

%/world.pyz :
	shiv -o "$@" -c world '${origin}#egg=world&subdirectory=world/'

# vim: tabstop=3 shiftwidth=3 noexpandtab ff=unix
