#! /usr/bin/env python3
"""Create a Python pyz from a script and its dependencies.

This is inferior to `shiv`:
- it loads slower
- has fewer dependencies
- is aware of conda.

A minimized equivalent would be:

    python3 -m pip install . --target $DEPENDENCIES
    python3 -m zipapp \\
        --main "$YOURSCRIPT:main" \\
        --output "$ZIPAPP.pyz" --compress \\
        $DEPENDENCIES

"""
import argparse
import logging
import os
import shutil
import tempfile
import warnings
import zipapp
from pathlib import Path

import pip
from rich.console import Console
from rich.logging import RichHandler
from rich_argparse import RawDescriptionRichHelpFormatter

logger = logging.getLogger(__name__)


console = Console(stderr=True)
logging.basicConfig(
    level=logging.DEBUG if __debug__ else logging.WARNING,
    handlers=[RichHandler(console=console)],
)
warnings.simplefilter("ignore", ResourceWarning)


def setup_parser(parser: argparse.ArgumentParser):
    parser.add_argument("-v", "--verbose", action="count", default=0)
    pip_group = parser.add_argument_group(
        "pip options",
    )
    pip_group.add_argument("-r", "--requirements", type=Path,
        help="If no requirements file is given, then the first argument is passed to pip",
    )
    zipapp_group = parser.add_argument_group(
        "zipapp options",
    )
    zipapp_group.add_argument("-m", "--main",
        help="If no main() method is specified, then the zipapp will open an interactive console.",
    )
    zipapp_group.add_argument("-o", "--output", type=Path)
    zipapp_group.add_argument("sources", nargs="+")
    return parser


def guess_interpreter():
    if prefix := os.getenv("CONDA_PREFIX"):
        return Path(prefix) / "bin/python3"
    else:
        return "/usr/bin/env python3"


def main():
    args = setup_parser(
        argparse.ArgumentParser(
            formatter_class=RawDescriptionRichHelpFormatter, description=__doc__
        )
    ).parse_args()
    logger.setLevel(logging.WARNING - 10 * args.verbose)
    if args.requirements:
        sources = args.sources
    else:
        package_arg, *sources = args.sources
    with tempfile.TemporaryDirectory() as td:
        td = Path(td)
        if args.requirements:
            pip.main(["install", "-r", str(args.requirements), "--target", str(td)])
        elif package_arg:
            pip.main(["install", str(package_arg), "--target", str(td)])
        for path in sources:
            shutil.copy(path, td)
        zipapp.create_archive(
            source=td,
            target=args.output,
            interpreter=guess_interpreter(),
            main=args.main or "code:interact",
            compressed=True,
        )


if __name__ == "__main__":
    main()
