.lessfilter
===========

Note that only bencode2, hachoir, and python-magic are pure-Python.
On Debian, do:

```sh
sudo apt-get install python3-cryptography python3-fitz
pipx install --system-site-packages \
	"https://gitlab.com/morphology/personal-utilities/-/tree/main/lens"
```
