#! /usr/bin/env python3
"""
Helper around hachoir and mupdf to act as a filter for lessfilter and ripgrep.
"""

import hashlib
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path
from typing import Tuple
import bencode2


@dataclass(order=True)
class TorrentFile:
    name: str
    size: int
    begin: Tuple[int]  # piece, offset
    end: Tuple[int]  # piece, offset


def parse_torrent(path):
    path = Path(path)
    torrent = bencode2.bdecode(path.read_bytes())
    info = torrent.pop(b"info")
    info_hash = hashlib.sha1(bencode2.bencode(info)).hexdigest()
    pieces_data = info.pop(b"pieces")
    pieces = [pieces_data[i : i + 20] for i in range(0, len(pieces_data), 20)]
    assert len(pieces[-1]) == 20
    piece_length = info.pop(b"piece length")

    dirs = defaultdict(list)
    offset = 0
    for order, attribs in enumerate(info.pop(b"files")):
        *parents_b, filename_b = attribs.pop(b"path")
        parent = b"/".join(parents_b).decode() if parents_b else None
        filename = filename_b.decode()
        path = f"{parent}/{filename}"

        size = attribs.pop(b"length")
        begin_location = divmod(offset, piece_length)
        offset += size
        torrent_file = TorrentFile(
            name=filename,
            size=size,
            begin=begin_location,
            end=divmod(offset, piece_length),
        )
        if attribs:
            print(f"Extra data on {torrent_file}: {attribs}")
        dirs[parent].append(torrent_file)
    return {
        "infohash": info_hash,
        "dirs": sorted(dirs.items()),
        "pieces": pieces,
        "piece length": piece_length,
        "total size": offset,
    }
