#! /usr/bin/env python3
"""
Helper around hachoir and mupdf to act as a filter for lessfilter and ripgrep.
"""

import gzip
import shutil
import subprocess
import tempfile
import warnings
from contextlib import contextmanager
from pprint import pprint
from subprocess import run

warnings.simplefilter("ignore", (ResourceWarning, DeprecationWarning))  # noqa: E402

import hachoir.metadata
import hachoir.metadata.metadata_item
import hachoir.parser

from cryptography import x509

from .torrents import parse_torrent


@contextmanager
def _temporary_decompress(path):
    dpath = path
    with tempfile.NamedTemporaryFile(prefix=path.name) as tf:
        match path.suffix.lower():
            case ".gz":
                dpath = tf.name
                tf.write(gzip.open(path).read())
            case ".zst":
                dpath = tf.name
                subprocess.run(["zstd", "-dfk", "-o", dpath, path], check=True)
        yield dpath


if shutil.which("tcpick"):

    def pcap_to_text(path):
        with _temporary_decompress(path) as dpath:
            run(["tcpick", "-td", "-r", dpath])
else:

    def pcap_to_text(path):
        with _temporary_decompress(path) as dpath:
            run(["tcpdump", "-qns", "0", "-r", dpath])


def hachoir_metadata(path):
    with hachoir.parser.createParser(str(path), tags=None) as parser:
        metadata = hachoir.metadata.extractMetadata(parser)
        if lines := metadata.exportPlaintext(
            human=True,
            priority=hachoir.metadata.metadata_item.MAX_PRIORITY,
            line_prefix="",
        ):
            for line in lines:
                print(line)


def torrent_metadata(path, show_files=None):
    hachoir_metadata(path=path)
    torrent_info = parse_torrent(path=path)
    print("Info hash:", torrent_info["infohash"])
    size_text = f"{torrent_info['total size']/1e6:,.1f} MB"
    if show_files is None:
        show_files = len(torrent_info["dirs"]) == 1
    print("Total size:", size_text)
    for parent, entries in torrent_info["dirs"]:
        if show_files:
            for entry in entries:
                path = f"{parent}/{entry.name}" if parent else entry.name
                size_text = f"{entry.size/1e6:,.1f}"
                print(f"{size_text:>10} MB|{path}")
        else:
            size = sum(info.size for info in entries)
            size_text = f"{size/1e6:,.1f}"
            print(f"{size_text:>10} MB|{parent}")


def certificate_metadata(path):
    content = path.read_bytes()
    is_binary = 0 in content
    match path.suffix.lower(), is_binary:
        case ".crl", False:
            crl = x509.load_pem_x509_crl(content)
            pprint(crl)
        case ".crl", True:
            crl = x509.load_der_x509_crl(content)
            pprint(crl)
        case (".crt", False) | (".pem", False):
            crts = x509.load_pem_x509_certificates(content)
            pprint(crts)
        case (".crt", True) | (".der", True):
            crt = x509.load_der_x509_certificate(content)
            pprint(crt)
        case ".csr", False:
            csr = x509.load_pem_x509_csr(content)
            pprint(csr)
        case ".csr", True:
            csr = x509.load_der_x509_csr(content)
            pprint(csr)
        case _:
            raise NotImplementedError(path)


def pandoc(path, output_format="plain"):
    with _temporary_decompress(path) as dpath:
        run(["pandoc", "--to", output_format, "--output", "-", dpath])
