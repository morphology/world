try:
    import magic
except ImportError:
    magic = None

if magic:
    # Linux
    def guess_file_type(path):
        mime_type = magic.from_file(path, mime=True)
        return mime_type, None
else:
    # Windows
    import mimetypes

    def guess_file_type(path):
        mime_type, encoding = mimetypes.guess_type(path, strict=False)  # in 3.13, guess_file_type
        return mime_type, encoding
