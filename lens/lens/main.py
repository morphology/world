#! /usr/bin/env python3
"""
Helper around hachoir and mupdf to act as a filter for lessfilter and ripgrep.
"""

import argparse
import json
import logging
import os
import plistlib
import re
import struct
import sys
from collections import Counter
from pathlib import Path
from tarfile import TarFile
from zipfile import ZipFile

from . import converter, guess_file_type
from .__about__ import __version__
from .tesseract import ocr_one
from .viewer import *

CPIO_FORMAT = "newc"

warnings.simplefilter("ignore", (ResourceWarning, DeprecationWarning))
logging.getLogger("pyvips").setLevel(logging.INFO)


def esuffix(path):
    match path.suffix.lower():
        case ".gz" | ".zst":
            if left_suffix := path.with_suffix("").suffix:
                return left_suffix
    return path.suffix


def _setup_parser(parser: argparse.ArgumentParser):
    parser.add_argument(
        "--version",
        action="version",
        version=__version__,
    )
    parser.add_argument("paths", nargs="*", type=Path)
    return parser


class LessIsCapable(Exception):
    """Special exception indicating that `less` can read this binary."""

    pass


def binary_reader(path, exit_if_less=False, try_ocr=False):
    """
    True: succeeded in reading as non-text
    None: didn't try
    False: not binary

    """
    try:
        width, height = os.get_terminal_size()
    except OSError:
        width, height = 80, 25

    mime_type, encoding = guess_file_type(path)
    title = f"{path} {mime_type} {path.stat().st_size:,d} bytes"

    # Special formats that defy mimetype
    if path.stem in ("id_rsa", "id_ed25519"):
        run(["ssh-keygen", "-Bv", "-f", path])
        return run(["ssh-keygen", "-l", "-f", path]).returncode == 0
    match esuffix(path).lower():
        case ".crl" | ".crt" | ".csr" | ".der" | ".pem":
            certificate_metadata(path)
            return True
        case ".ko":
            return run(["modinfo", path]).returncode == 0
        case ".pcap":
            pcap_to_text(path)
            return True
        case ".pyz":
            print(title)
            with ZipFile(path) as zf:
                for entry in zf.infolist():
                    pprint(entry)
            return True
        case ".safetensors":
            with Path(path).open("rb") as fi:
                (header_size,) = struct.unpack("<Q", fi.read(8))
                header = fi.read(header_size)
            tensors = json.loads(header.decode("utf-8"))
            metadata = tensors.pop("__metadata__", None)

            n_tensors = len(tensors)
            print(f"SafeTensors '{path}' with {n_tensors} tensor(s)")
            for key, value in metadata.items():
                try:
                    value = json.loads(value)
                except:
                    pass
                print(f"{key}:", value)
            print()
            name_counter = Counter("\u24c3".join(re.split(r"\d+", n)) for n in tensors)
            for pattern, count in name_counter.most_common():
                print(count, pattern)

    match mime_type:
        case "text/plain":
            return False

    if exit_if_less:
        # less is capable for these binary types:
        if mime_type.startswith("image/") and not mime_type == "image/heic":
            raise LessIsCapable

    if mime_type in converter.VALID_TYPES:
        content = converter.extract(path, mime_type=mime_type)
        if content:
            print(content)
            return True

    match mime_type:
        #
        # When calling run() here, you probably DO NOT want check=True
        #
        case "application/epub+zip" | "application/rtf" | "text/rtf":
            print(title)
            pandoc(path)
            return True
        case "application/x-bittorrent":
            print(title)
            torrent_metadata(path)
            return True
        case "application/x-cpio":
            print(title)
            # -H newc refers to SVR4
            return run(["cpio", "-H", CPIO_FORMAT, "-tvF", path]).returncode == 0
        case "application/dicom":
            print(title)
            # Note: the +C color scheme of dcmdump assumes a white background, i.e. remedy the text with sed -r 's/\x1B\[30m//g'
            return run(["dcmdump", "+E", "+fo", "-M", "+uc", path]).returncode == 0
        case "application/plist":
            print(title)
            pprint(plistlib.load(path.open("rb")), width=width)
            return True
        case "application/x-executable" | "application/x-pie-executable" | "application/x-sharedlib":
            print(title)
            return run(["objdump", "-x", "-M", "intel", path]).returncode == 0
        case "application/x-tar":
            print(title)
            with TarFile(path) as tf:
                for entry in tf:
                    pprint(entry, width=width)
            return True
        case m if m.startswith("image/"):
            print(title)
            hachoir_metadata(path)
            if try_ocr:
                print(ocr_one(path))
            return True
        case m if m.startswith("video/"):
            print(title)
            hachoir_metadata(path)
            return True
        case "application/octet-stream":
            print(title)
            return run(["od", "-Ax", "-tx1z", path]) == 0


def ripgrep_filter():
    parser = _setup_parser(
        argparse.ArgumentParser(description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    )
    args = parser.parse_args()
    for path in args.paths:
        if not binary_reader(path=path, exit_if_less=False, try_ocr=True):
            print(path.read_text())


def lessfilter():
    parser = _setup_parser(
        argparse.ArgumentParser(description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    )
    args = parser.parse_args()

    (path,) = args.paths
    try:
        if not binary_reader(path=path, exit_if_less=True):
            sys.exit(1)
    except LessIsCapable:
        sys.exit(1)
