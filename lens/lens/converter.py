"""
Simple converter using docling (which depends on pytorch) or a
lighter workaround that depends on pymupdf and unoconvert.
"""

import subprocess

from subprocess import run
from zipfile import is_zipfile

try:
    import docling
    from docling.document_converter import DocumentConverter

    _converter = DocumentConverter()

    fitz = None
except ModuleNotFoundError:
    # TODO: Debian packages an older Python, which succeeds in importing fitz.
    # Newer Python must import pymupdf
    import fitz

    docling = None

DOC_TYPES = [
    "application/rtf",
    "text/rtf",
    "application/vnd.oasis.opendocument.text",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
]
SPREADSHEET_TYPES = [
    "application/vnd.oasis.opendocument.spreadsheet",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
]
VALID_TYPES = [*DOC_TYPES, "application/pdf", *SPREADSHEET_TYPES]

has_unoserver = run(["systemctl", "--user", "is-active", "--quiet", "unoserver.service"]).returncode == 0


if docling:

    def extract(source, mime_type=None):
        # Only .convert_all() takes raise_on_error
        result = _converter.convert(source)
        if content := result.document.export_to_markdown():
            return content.strip()
else:
    assert has_unoserver

    def extract(path, mime_type):
        if mime_type in SPREADSHEET_TYPES:
            proc = run(["unoconvert", path, "--convert-to=tsv", "-"], stdout=subprocess.PIPE, check=True, text=True)
            if content := proc.stdout:
                return content.strip()
        elif mime_type == "application/pdf" and not is_zipfile(path):
            doc = fitz.open(path)
            assert doc.is_pdf
            assert not doc.needs_pass
            # Default:
            # flags = fitz.TEXT_PRESERVE_LIGATURES | fitz.TEXT_PRESERVE_WHITESPACE
            flags = fitz.TEXT_INHIBIT_SPACES
            content = ""
            for page_n in range(doc.page_count):
                page = doc[page_n]
                page_content = page.get_text("text", flags=flags)
                if page_content.strip():
                    content += f"\n# Page {page_n+1}\n{page_content}"
            return content
        else:
            if mime_type not in DOC_TYPES:
                print(f"Guessing {path} is a text document")
            proc = run(["unoconvert", path, "--convert-to=txt", "-"], stdout=subprocess.PIPE, check=True, text=True)
            if content := proc.stdout:
                return content.strip()
