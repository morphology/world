"""
Create contact sheet for home movies
"""
import argparse
import contextlib
import logging
import os
import shutil
import subprocess
import sys
import tempfile
from pathlib import Path
from typing import Iterable

from rich_argparse import RawDescriptionRichHelpFormatter

from .ffprobe import ffprobe

logger = logging.getLogger(__name__)

DEFAULT_SCREENCAP_CONVERT_ARGS = [
    "-quality",
    "66",
    "-resize",
    "2000000@>",
    "-fill",
    "gray95",
    "-undercolor",
    "#00000080",
    "-font",
    os.getenv("SCREENCAP_FONT") or "DejaVu-Sans-Bold",
    "-pointsize",
    str(os.getenv("SCREENCAP_FONT_SIZE") or 24),
    "-antialias",
]


def _get_ffmpeg_image_tiling_video_filter(media_info, tile="tile=6x5"):
    height, width = media_info.get_dimensions()
    duration = media_info.get_duration()
    screencap_interval = float(duration.total_seconds()) / 31.0
    filters = [
        f"""select='isnan(prev_selected_t)+gte(t-prev_selected_t\\,{screencap_interval})'"""
    ]
    if min(height, width) > 256:
        filters.append("scale=w=-1:h=256")
    if tile:
        filters.append(tile)
    return ",".join(filters)


def get_tile_commands(media_info, output_path: os.PathLike, **kwargs):
    return [
        "-an",
        "-vf",
        _get_ffmpeg_image_tiling_video_filter(media_info, **kwargs),
        "-frames:v",
        "1",
        output_path,
    ]


def _overlay_video_info_args(probe, fallback_title=""):
    duration = probe.get_duration()
    width, height = probe.get_dimensions()

    mbps = probe.get_bit_rate()
    megapixels = width * height / 1e6
    size = probe.get_file_size()
    title = probe.get_title() or fallback_title

    convert_args = ["-gravity", "northwest", "-annotate", "+0+0", title]
    if 0.01 < megapixels:
        convert_args += [
            "-gravity",
            "northeast",
            "-annotate",
            "+0+0",
            f"{megapixels:0.2f} Mpx @ {mbps:0.1f} Mbps",
        ]
    if duration:
        minutes_part, _, seconds_part = str(duration).lstrip("0:").rpartition(":")
        minutes_part = minutes_part or "0"
        seconds_part = float(seconds_part)

        convert_args += [
            "-gravity",
            "southwest",
            "-annotate",
            "+0+0",
            f"{minutes_part}:{seconds_part:06.3f}",
        ]
    if size:
        convert_args += [
            "-gravity",
            "southeast",
            "-annotate",
            "+0+0",
            f"{size:,d} bytes",
        ]
    return convert_args


def overlay_video_info(
    input_path: os.PathLike,
    media_info,
    output_path: os.PathLike,
    convert_args: Iterable = tuple(DEFAULT_SCREENCAP_CONVERT_ARGS),
):
    command = [
        "convert",
        *convert_args,
        input_path,
        *_overlay_video_info_args(
            probe=media_info,
            fallback_title=media_info.get_path().stem.replace("_", " "),
        ),
        output_path,
    ]
    return subprocess.run(command, check=True)


def screencap(video_path, screencap_path):
    interactive = sys.stdout.isatty()
    media_info = ffprobe(video_path)
    with tempfile.TemporaryDirectory() as tmpdir:
        with screencap_within_ffmpeg_call(
            media_info, screencap_path, tmpdir=tmpdir
        ) as arguments:
            command = [
                "ffmpeg",
                "-hide_banner" if interactive else "-nostdin",
                "-i",
                video_path,
                *arguments,
            ]
            subprocess.run(command, check=True, text=True)


@contextlib.contextmanager
def screencap_within_ffmpeg_call(media_info, screencap_path, tmpdir):
    screencap_path = Path(screencap_path)

    tmpdir = Path(tmpdir)
    unlabelled_path = tmpdir / "unlabelled.PNG"
    arguments = get_tile_commands(media_info, unlabelled_path)
    yield arguments

    labelled_path = (tmpdir / "labelled").with_suffix(screencap_path.suffix)
    overlay_video_info(unlabelled_path, media_info, labelled_path)
    shutil.move(labelled_path, screencap_path)


def main():
    parser = argparse.ArgumentParser(
        formatter_class=RawDescriptionRichHelpFormatter, description=__doc__
    )
    parser.add_argument("video_path", metavar="VIDEO", type=Path)
    parser.add_argument("screencap_path", metavar="IMAGE", type=Path)
    args = parser.parse_args()
    screencap(args.video_path, args.screencap_path)
