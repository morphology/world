"""
Prepare to join multiple videos.
"""

import argparse
from collections import Counter, defaultdict
import fileinput
import logging
import os
import tempfile
from datetime import date, timedelta
from pathlib import Path
import shlex
from subprocess import run
import sys
from typing import Iterable, Optional

from rich.console import Console
from rich.logging import RichHandler
import rich.progress
from rich_argparse import RawDescriptionRichHelpFormatter

from ._version import __version__
from .ffprobe import ffprobe
from .ffmpeg import write_ffconcat
from .yolov5_detect import detect_animal

DAWN = timedelta(hours=5)

logger = logging.getLogger(__name__)


class VideoClip:
    def __init__(self, path):
        path = self.path = Path(path)
        self.info = ffprobe(path)
        if not self.info:
            raise ValueError(f"Extracting info failed for {path}")
        self.detections = []

    def get_date(self):
        if creation_time := self.info.get_creation_time():
            localtime = creation_time.astimezone()
            return (localtime - DAWN).date()

    def get_detections(self):
        return Counter(c for f, c in self.detections).most_common()


def get_playlist_name(clip: VideoClip):
    if date := clip.get_date():
        labels = [f"{date:%Y-%m-%d}"]
    else:
        labels = ["unknown"]
    detections = [
        label.replace(" ", "_")
        for label, freq in clip.get_detections()
        if label != "vehicle"
    ]
    if labels:
        return "-".join(labels + detections)


def get_detection_info(
    video_paths: Iterable[os.PathLike],
    output_dir: os.PathLike,
) -> dict | None:
    output_dir = Path(output_dir)
    detection_dir = output_dir / "detections"

    video_info = {}
    for path in video_paths:
        video_info[path.resolve()] = VideoClip(path=path)
    if not video_info:
        return

    for path, frame_number, detection_classes in rich.progress.track(
        detect_animal(
            dataset=sorted(video_info),
            model=Path("/app/MegaDetector_v5b.0.0.pt"),
            save_dir=detection_dir,
        ),
        description="Detecting animals (fps)",
    ):
        info = video_info[path]
        if detection_classes:
            # unflatten list
            info.detections.extend((frame_number, c) for c in detection_classes)
    return video_info


def join(video_paths: Iterable[os.PathLike], output_dir: os.PathLike):
    if output_dir is None:
        output_dir = tempfile.mkdtemp()
    output_dir = Path(output_dir)
    script_path = output_dir / "join.sh"

    playlist_contents = defaultdict(list)
    for _, clip in get_detection_info(video_paths, output_dir=output_dir).items():
        if playlist_name := get_playlist_name(clip):
            playlist_contents[playlist_name].append(clip)
    for name, contents in playlist_contents.items():
        contents.sort(key=lambda c: c.get_date())
        ffconcat_path = (output_dir / name).with_suffix(".ffconcat")
        write_ffconcat(ffconcat_path, [clip.path for clip in contents])

    script_contents = f"""\
#! /bin/sh
dest="$PWD"
cd {shlex.quote(str(output_dir.resolve()))}
for list_file in *.ffconcat; do
  name=$( basename "${{list_file}}" )
  output_video="${{dest}}/${{name}}.MP4"

# Crop to a size and upper-left coordinate
# This selects the lower-left 4/9 of the screen
#  ffmpeg -f concat -safe 0 -i "$list_file" -filter:v 'crop=in_w*2/3:in_h*2/3:0:in_h*1/3' "$output_video"
# This selects the lower-right:
#  ffmpeg -f concat -safe 0 -i "$list_file" -filter:v 'crop=in_w*2/3:in_h*2/3:in_w*1/3:in_h*1/3' "$output_video"

# Convert to grayscale using the format=gray or hue=s=0 video filter
#  ffmpeg -f concat -safe 0 -i "$list_file" -filter:v 'format=gray' "$output_video"

# A custom command might combine those into one arg:
#  ffmpeg -f concat -safe 0 -i "$list_file" "$@" "$output_video" 

# The fastest way is to avoid transcoding:
  ffmpeg -f concat -safe 0 -i "$list_file" -c copy "$output_video"
done
"""
    if not script_path.exists():
        script_path.write_text(script_contents)
    else:
        logger.info(f"Refusing to overwrite {script_path}")
    return {"script": script_path}


def main():
    parser = argparse.ArgumentParser(
        formatter_class=RawDescriptionRichHelpFormatter, description=__doc__
    )
    parser.add_argument(
        "video_paths", nargs="+" if sys.stdin.isatty() else "*", metavar="VIDEO", type=Path
    )
    parser.add_argument("-o", "--output-dir", type=Path)
    parser.add_argument("-v", "--verbose", action="count", default=0)
    parser.add_argument(
        "--version", action="version", version=f"%(prog)s {__version__}"
    )
    args = parser.parse_args()
    interpreter = ["sh", "-x"] if args.verbose else ["sh"]
    logger.setLevel(logging.WARNING - 10 * args.verbose)

    results = join(
        args.video_paths or [Path(line.rstrip()) for line in fileinput.input()],
        output_dir=args.output_dir,
    )

    console = Console(stderr=True)
    script_path = results["script"].resolve()
    console.status("Joining videos...")
    command = ["ionice", *interpreter, script_path]
    run(command, check=True, text=True)


if __name__ == "__main__":
    logging.basicConfig(level=log_level, format="%(message)s", handlers=[RichHandler()])
