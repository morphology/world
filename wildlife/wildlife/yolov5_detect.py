# Derived from YOLOv5 by Ultralytics, AGPL-3.0 license
import sys
import tempfile
from pathlib import Path
from typing import Optional

import torch


from ultralytics.utils.plotting import Annotator, colors, save_one_box

from models.common import DetectMultiBackend
from utils.dataloaders import (
    LoadImages,
    LoadScreenshots,
    LoadStreams,
)
from utils.general import (
    LOGGER,
    Profile,
    check_img_size,
    cv2,
    non_max_suppression,
    scale_boxes,
    strip_optimizer,
)
from utils.torch_utils import smart_inference_mode

LOGGER.setLevel("WARNING")


@smart_inference_mode()
def detect_animal(
    dataset: LoadImages | LoadStreams | LoadScreenshots | Path,
    model: torch.nn.Module | Path,
    model_args: Optional[dict] = None,
    non_max_suppression_args: Optional[dict] = None,
    imgsz=(640, 640),  # inference size (height, width)
    device=None,  # cuda device, i.e. 0 or 0,1,2,3 or cpu
    save_dir=None,
    line_thickness=3,  # bounding box thickness (pixels)
    update=False,  # update all models
):
    if device is None:
        device = torch.device("cuda")

    if not isinstance(model, torch.nn.Module):
        weights = model
        if model_args is None:
            model_args = dict(
                dnn=False,
                data="usr/src/app/data/coco128.yaml",
                fp16=False,
            )
        model = DetectMultiBackend(weights, device=device, **model_args)
    is_openvino = bool(model.xml)
    stride, names, pt = model.stride, model.names, model.pt
    imgsz = check_img_size(imgsz, s=stride)  # check image size

    bs = 1  # batch_size
    if not isinstance(dataset, (LoadImages, LoadStreams, LoadScreenshots)):
        source = dataset
        dataset = LoadImages(
            source, img_size=imgsz, stride=stride, auto=pt, vid_stride=1
        )

    if non_max_suppression_args is None:
        non_max_suppression_args = dict(
            conf_thres=0.75,  # confidence threshold
            iou_thres=0.45,  # NMS IOU threshold
            classes=None,  # filter by class: --class 0, or --class 0 2 3
            agnostic=False,  # class-agnostic NMS
            max_det=3,  # maximum detections per image
        )

    if save_dir is None:
        save_dir = tempfile.mkdtemp()
        LOGGER.info(f"Saving results to {save_dir}")
    save_dir = Path(save_dir)

    # Run inference
    model.warmup(imgsz=(1 if pt or model.triton else bs, 3, *imgsz))  # warmup
    dt = Profile(device=device), Profile(device=device), Profile(device=device)

    vid_path, vid_writer = [None] * bs, [None] * bs
    for path, im, im0s, vid_cap, s in dataset:
        path = Path(path)
        if vid_cap:  # video
            fps = vid_cap.get(cv2.CAP_PROP_FPS)
            w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
            h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        with dt[0]:
            im = torch.from_numpy(im).to(model.device)
            im = im.half() if model.fp16 else im.float()  # uint8 to fp16/32
            im /= 255  # 0 - 255 to 0.0 - 1.0
            if len(im.shape) == 3:
                im = im[None]  # expand for batch dim
            if is_openvino and im.shape[0] > 1:
                ims = torch.chunk(im, im.shape[0], 0)

        # Inference
        with dt[1]:
            model_run_args = dict(
                augment=False,  # augmented inference
                visualize=False,
            )

            if is_openvino and im.shape[0] > 1:
                pred = None
                for image in ims:
                    if pred is None:
                        pred = model(image, **model_run_args).unsqueeze(0)
                    else:
                        pred = torch.cat(
                            (pred, model(image, **model_run_args).unsqueeze(0)), dim=0
                        )
                pred = [pred, None]
            else:
                pred = model(im, **model_run_args)
        # NMS
        with dt[2]:
            pred = non_max_suppression(pred, **non_max_suppression_args)

        # Second-stage classifier (optional)
        # pred = utils.general.apply_classifier(pred, classifier_model, im, im0s)

        # Process predictions
        save_video_path = (save_dir / path.stem).with_suffix(".mp4") if save_dir else None
        for i, det in enumerate(pred):  # per image
            im0 = im0s.copy()
            frame_number = getattr(dataset, "frame", 0)

            s += "%gx%g " % im.shape[2:]  # print string

            imc = im0.copy()  # would be modified by annotator
            annotator = Annotator(
                im0,
                line_width=line_thickness,
                example=str(names),  # non-ASCII treatment is determined from this
            )

            detection_classes = []
            if det.nelement() > 0:  # if non-empty
                det[:, :4] = scale_boxes(
                    img1_shape=im.shape[2:], boxes=det[:, :4], img0_shape=im0.shape
                ).round()

                for detection_number, (*xyxy, conf, cls) in enumerate(reversed(det)):
                    c = int(cls)  # integer class
                    name = names[c]
                    label = f"{name} {conf:.2f}"
                    save_image_path = (
                        save_dir
                        / f"{path.stem}-{frame_number:04d}-{detection_number:03d}.jpg"
                    ) if save_dir else None

                    annotator.box_label(xyxy, label, color=colors(c, True))
                    if save_image_path:
                        save_one_box(
                            xyxy,
                            imc,
                            file=save_image_path,
                            BGR=True,
                        )
                    detection_classes.append(name)
                s += ", ".join(detection_classes) + " "
            else:
                s += "(no detections) "
            yield path, frame_number, detection_classes

            if save_video_path:
                if vid_path[i] != save_video_path:  # new video
                    vid_path[i] = save_video_path
                    if isinstance(vid_writer[i], cv2.VideoWriter):
                        vid_writer[i].release()  # release previous video writer
                    vid_writer[i] = cv2.VideoWriter(
                        str(save_video_path),
                        cv2.VideoWriter_fourcc(*"mp4v"),
                        fps,
                        (w, h),
                    )
                vid_writer[i].write(annotator.result())

        # Print time (inference-only)
        LOGGER.info(
            f"{s}{'' if len(det) else '(no detections), '}{dt[1].dt * 1E3:.1f}ms"
        )

    if update:
        strip_optimizer(weights[0])  # update model (to fix SourceChangeWarning)
