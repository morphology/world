import argparse
import collections
import fileinput
import json
import logging
import os
import re
import subprocess
import sys
import tempfile
from datetime import datetime, timedelta
from pathlib import Path
from typing import Iterable, Optional, Callable

import pytz
from rich.logging import RichHandler
from rich.progress import Progress
from rich_argparse import RawDescriptionRichHelpFormatter

logger = logging.getLogger(__name__)


class ProbeResult:
    def __init__(self, ffprobe_result: dict):
        self.probe_result = ffprobe_result
        self.n_audio_streams = sum(
            1 for s in self._get_streams() if s["codec_type"] == "audio"
        )
        self.n_video_streams = sum(
            1 for s in self._get_streams() if s["codec_type"] == "video"
        )

    def _get_format(self):
        return self.probe_result["format"]

    def get_file_size(self):
        return int(self._get_format()["size"])

    def get_title(self):
        if "tags" not in self._get_format():
            return
        tags = self._get_format()["tags"]
        title = tags.get("title")
        if title and (ccount := collections.Counter(title)):
            if ccount.keys() == {"?"}:
                logger.warning(f"Ignoring title '{title}'")
                title = None
        return title

    def _get_streams(self):
        return self.probe_result["streams"]

    def get_creation_time(self, timestamp_format="%Y-%m-%dT%H:%M:%S.%fZ"):
        if tags := self._get_format().get("tags"):
            if creation_time := tags.get("creation_time"):
                return datetime.strptime(creation_time, timestamp_format).astimezone(
                    pytz.UTC
                )

    def get_data_hashes(self):
        return [(s["codec_type"], s["extradata_hash"]) for s in self._get_streams()]

    def get_path(self, absolute=True):
        path = Path(self._get_format()["filename"])
        if absolute:
            path = path.resolve()
        return path

    def get_format_name(self):
        return self._get_format()["format_name"]

    def get_bit_rate(self):
        return float(self._get_format()["bit_rate"]) / 1e6

    def get_duration(self):
        duration_s = self._get_format()["duration"]
        return timedelta(seconds=float(duration_s))

    def get_primary_video_stream(self):
        def key_func(stream):
            n_pixels = stream["width"] * stream["height"]
            bit_rate = float(stream.get("bit_rate", 0))
            return bit_rate, n_pixels, -int(stream["index"])

        video_streams = [s for s in self._get_streams() if s["codec_type"] == "video"]
        return max(video_streams, key=key_func)

    def get_dimensions(self):
        s = self.get_primary_video_stream()
        width, height = s["width"], s["height"]
        if self.get_format_name() == "mov,mp4,m4a,3gp,3g2,mj2":
            if (width % 2 != 0) or (height % 2 != 0):
                logger.error(
                    "Adjust height and width to be multiples of 2. "
                    "This appears only common with Quicktime. "
                    'Use ["-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2"] .'
                )
        return width, height


class AudioResult(ProbeResult):
    def get_audio_codecs(self, good_codecs=("aac", "flac", "mp3", "vorbis", "opus")):
        good_streams, bad_streams = {}, {}
        for stream in self._get_streams():
            if stream["codec_type"] == "audio":
                index = stream["index"]
                codec_name = stream["codec_name"]
                if codec_name in good_codecs:
                    good_streams[index] = codec_name
                else:
                    bad_streams[index] = codec_name
        if bad_streams:
            logger.error(
                "Poorly-supported audio codec(s): %r",
                bad_streams,
            )
        elif not good_streams:
            logger.error("No audio streams")
        return {**good_streams, **bad_streams}


class VideoResult(AudioResult):
    def get_video_codecs(self, good_codecs=("h264", "hevc", "vp8", "vp9")):
        audio_streams = self.get_audio_codecs()  # trigger above
        good_streams, bad_streams = {}, {}
        for stream in self._get_streams():
            if stream["codec_type"] == "video":
                index = stream["index"]
                codec_name = stream["codec_name"]
                if codec_name in good_codecs:
                    good_streams[index] = codec_name
                else:
                    bad_streams[index] = codec_name
        if bad_streams:
            logger.error(
                "Poorly-supported video codec(s): %r",
                bad_streams,
            )
        elif not good_streams:
            logger.error("No video streams")
        return {**good_streams, **bad_streams}

    def get_nb_frames(self):
        n_frames_counter = collections.Counter()
        for stream in self._get_streams():
            if stream["codec_type"] == "video":
                if "nb_frames" in stream:
                    n_frames_counter[int(stream["nb_frames"])] += 1
                elif "nb_read_frames" in stream:
                    n_frames_counter[int(stream["nb_read_frames"])] += 1
                elif self.get_format_name() in {
                    "gif",
                    "image2",
                    "png_pipe",
                    "ppm_pipe",
                    "tiff_pipe",
                    "webp_pipe",
                }:
                    logger.debug(
                        "Assuming stream #%d is not a video but a still-image",
                        stream["index"],
                    )
                else:
                    logger.error(
                        "Stream #%d: number-of-frames not provided", stream["index"]
                    )
        if n_frames_counter:
            if len(n_frames_counter) > 1:
                logger.warning(
                    "Multiple number of frames: %r",
                    n_frames_counter.most_common(),
                )
            ((n_frames, _),) = n_frames_counter.most_common(1)
            return n_frames


def _guess_result_class(ffprobe_result: dict):
    probe_result = ProbeResult(ffprobe_result)
    if probe_result.n_video_streams > 0:
        video_result = VideoResult(ffprobe_result)
        # stream attribute disposition.still_image is misleading
        if nb_frames := video_result.get_nb_frames():
            if nb_frames > 1:
                return video_result
        elif probe_result.get_format_name() == "matroska,webm":
            logger.info(
                "Container doesn't provide nb_frames without '-count_frames'"
            )  # but it does provide duration
            return video_result
    elif probe_result.n_audio_streams > 0:
        return AudioResult(ffprobe_result)
    return probe_result


def ffprobe(
    media_path: os.PathLike,
    ffprobe_args: Iterable = (
        "-show_format",
        "-show_streams",
        "-show_chapters",
        "-show_data_hash",
        "MD5",
    ),
    interactive=sys.stderr.isatty(),
    result_factory: Callable = _guess_result_class,
):
    media_path = Path(media_path)
    if interactive:
        ffprobe_args = [*ffprobe_args, "-hide_banner"]

    command = [
        "ffprobe",
        "-print_format",
        "json",
        *ffprobe_args,
        media_path,
    ]
    proc = subprocess.run(command, check=True, text=True, stdout=subprocess.PIPE)
    if proc.returncode == 0:
        logger.debug(f"{command} exited {proc.returncode}")
        probe_result = result_factory(json.loads(proc.stdout))
        if proc.stderr is not None:
            logger.debug(proc.stderr)
        return probe_result
    else:
        logger.error(f"{command} exited {proc.returncode}")
        if proc.stderr is not None:
            logger.error(proc.stderr)


def main():
    interactive = sys.stdin.isatty()
    parser = argparse.ArgumentParser(
        formatter_class=RawDescriptionRichHelpFormatter, description=__doc__
    )
    parser.add_argument(
        "media_paths",
        nargs="+" if interactive else "*",
        type=Path,
        metavar="FILE",
    )
    parser.add_argument("-v", "--verbose", action="count", default=0)
    args = parser.parse_args()

    log_level = logging.WARNING - 10 * args.verbose
    logging.basicConfig(level=log_level, format="%(message)s", handlers=[RichHandler()])

    paths = args.media_paths or [Path(line.rstrip()) for line in fileinput.input()]
    n_problem_files = 0
    with (progress := Progress(transient=True)):
        for path in progress.track(paths, description="Collecting metadata..."):
            media_info = ffprobe(path)
            if not media_info:
                logger.error(f"{path}: [red]Problem")
                n_problem_files += 1
                continue
            match media_info:
                case VideoResult():
                    width, height = media_info.get_dimensions()
                    progress.console.print(
                        f"{path}: [green]{media_info.get_bit_rate():.1f} Mbps {width}x{height} {media_info.get_duration()}"
                    )
                case AudioResult():
                    progress.console.print(
                        f"{path}: [green]{media_info.get_bit_rate():.1f} Mbps {media_info.get_duration()}"
                    )
                case _:
                    progress.console.print(f"{path}: [green]... sure")
            if args.verbose:
                progress.console.print(
                    json.dumps(
                        media_info.probe_result,
                        indent=2,
                        sort_keys=True,
                        default=str,
                    )
                )
    if n_problem_files != 0:
        sys.exit("Not all files converted")
