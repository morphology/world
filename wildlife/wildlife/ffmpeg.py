import csv
import logging
import os
import re
import subprocess
import tempfile
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path
from typing import Iterable, List, Optional, Tuple

from . import ffprobe

logger = logging.getLogger(__name__)


@dataclass(order=True, frozen=True)
class FFmpegMetadataFrame:
    """
    When doing a selection, the frame number does not necessarily correspond to the original video.
    """

    number: int
    pts: int
    pts_time: float

    @classmethod
    def match(cls, text: str):
        begin_re = re.compile(
            r"(frame:(?P<number>\d+)\s+pts:(?P<pts>\d+)\s+pts_time:(?P<pts_time>[0-9.]+))"
        )
        if m := begin_re.match(text.strip()):
            return cls(
                number=int(m.group("number")),
                pts=int(m.group("pts")),
                pts_time=float(m.group("pts_time")),
            )


def _parse_metadata_output(path: os.PathLike):
    path = Path(path)
    lines = iter(path.read_text().strip().split("\n"))
    location = FFmpegMetadataFrame.match(next(lines))
    if location is not None:
        attribs = {}
        for line in lines:
            if new_frame := FFmpegMetadataFrame.match(line):
                yield location, attribs
                location, attribs = new_frame, {}
            elif line.strip():
                key, _, value = line.partition("=")
                attribs[key] = value
        yield location, attribs


def _parse_segment_list(
    path: os.PathLike,
) -> Iterable[Tuple[Tuple[float, float], dict]]:
    path = Path(path)
    with path.open("r") as fi:
        reader = csv.reader(fi)
        for row in reader:
            filename = row[0]
            span = (float(row[1]), float(row[2]))
            yield span, {"filename": filename}


def detect_scenes(input_path: os.PathLike):
    input_path = Path(input_path)

    combined_attrs = defaultdict(dict)
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)
        (log_path := tmpdir / "logs").mkdir()
        command = [
            "ffmpeg",
            "-hide_banner",
            "-i",
            input_path,
            "-an",
            "-filter:v",
            "blackdetect,"
            f"metadata=print:file={log_path / 'blackdetect'},"
            "scdet,"
            f"metadata=print:file={log_path / 'scdet'}",
            "-frame_pts",
            "true",
            "-f",
            "null",
            "-",
        ]
        logger.debug("Running %r", command)
        subprocess.run(command, check=True, text=True)
        for log_file in log_path.glob("*"):
            for location, attrs in _parse_metadata_output(log_file):
                combined_attrs[location].update(attrs)
    return sorted(combined_attrs.items())


def segment_video(
    input_path: os.PathLike,
    scene_timecodes: List,
    output_pattern: str,
    media_info: Optional[ffprobe.ProbeResult] = None,
    all_streams: bool = True,
    extra_ffmpeg_arguments: Iterable = (),
    verbose: bool = False,
) -> List[Tuple[Tuple[float, float], dict]]:
    input_path = Path(input_path)
    extra_ffmpeg_arguments = list(extra_ffmpeg_arguments)

    count_pattern = f"%0{len(str(len(scene_timecodes) + 1))}d"
    ext = ".MKV"
    default_output_pattern = f"{input_path.stem}-{count_pattern}{ext}"
    if output_pattern is None:
        output_pattern = default_output_pattern
    output_pattern = Path(output_pattern)
    if output_pattern.is_dir():
        output_dir = output_pattern
        output_pattern = output_dir / default_output_pattern
    else:
        output_dir = output_pattern.parent

    if verbose:
        subprocess_args = {"check": True, "capture_output": True}
    else:
        # Disable stderr redirection for both ffprobe and ffmpeg
        subprocess_args = {"check": True, "stderr": subprocess.DEVNULL}

    media_info = media_info or ffprobe.ffprobe(input_path)

    if all_streams:
        extra_ffmpeg_arguments += ["-map", "0"]
    if media_info.get_format_name() == "mov,mp4,m4a,3gp,3g2,mj2":
        extra_ffmpeg_arguments += ["-movflags", "faststart"]
    scene_timecodes.sort()
    segment_times = ",".join(str(t) for t in scene_timecodes)
    assert segment_times
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)
        segment_list_path = tmpdir / "segments.csv"
        command = [
            "ffmpeg",
            "-hide_banner",
            "-i",
            input_path,
            *extra_ffmpeg_arguments,
            "-f",
            "segment",
            "-segment_times",
            segment_times,
            "-segment_start_number",
            "1",
            "-reset_timestamps",
            "true",
            "-segment_list",
            segment_list_path,
            "-segment_list_type",
            "csv",
            output_pattern,
        ]
        proc = subprocess.run(command, text=True, **subprocess_args)
        if proc.stderr is not None:
            assert "[q] command received. Exiting." not in proc.stderr
        if proc.returncode == 0:
            logger.debug(f"{command} exited {proc.returncode}")
            if proc.stderr is not None:
                logger.debug(proc.stderr)
        else:
            logger.error(f"{command} exited {proc.returncode}")
            if proc.stderr is not None:
                logger.error(proc.stderr)
        produced_segments = list(_parse_segment_list(segment_list_path))
    for span, attribs in produced_segments:
        attribs["path"] = output_dir / attribs["filename"]
    return produced_segments


def write_ffconcat(path, entries):
    path = Path(path)
    with path.open("w") as fo:
        fo.writelines(["ffconcat version 1.0\n", "\n"])
        for entry in entries:
            if isinstance(entry, (os.PathLike, str)):
                path = Path(entry)
                fo.write(f"file {path.resolve()}\n")
            else:
                fo.write(f"# {entry.get_creation_time()} UTC\n")
                fo.write(f"file {entry.get_path()}\n")
