import os
from dataclasses import dataclass
from pathlib import Path
import sqlite3
from typing import Optional

DB = sqlite3.connect("file:/var/lib/navidrome/navidrome.db?mode=ro", uri=True)


@dataclass
class File:
    path: os.PathLike
    size: Optional[int]
    rating: Optional[float]


def fetch_rated_songs(con=DB):
    cur = con.cursor()
    cur.execute(
        """\
select path, rating, size from
annotation inner join media_file on
annotation.item_id = media_file.id where rating > 0 order by rating DESC;
"""
    )
    for path, rating, size in cur:
        yield File(Path(path), size, rating)


def fetch_rated_totals(con=DB):
    cur = con.cursor()
    cur.execute(
        """\
select rating, count(*) as n_songs, sum(size) as total_size from
annotation inner join media_file on
annotation.item_id = media_file.id where rating > 0
group by rating;
"""
    )
    return list(cur)
