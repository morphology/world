import argparse
import logging
from pathlib import Path
import shutil
from subprocess import run

from rich.console import Console
from rich.logging import RichHandler
from rich.progress import Progress
from rich.prompt import Confirm

from . import navidrome

BLURAY = int(24411.5e6)
DVDMINUSR = 2297888 * 2048
DVDPLUSR = 2295104 * 2048

SERVER_ROOT = Path("/home/Music")

logger = logging.getLogger(__name__)


def copyfile(src, dest):
    dest = Path(dest)
    dest.parent.mkdir(exist_ok=True, parents=True)
    shutil.copyfile(src, dest)


def trash(path):
    run(["gio", "trash", path], check=True)


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("-n", "--dry-run", action="store_true")
    parser.add_argument("target", type=Path)
    parser.set_defaults(dry_run=False)
    args = parser.parse_args()
    console = Console(stderr=True)

    total_backup_size = 0
    total_deletion_size = 0
    console.status("Fetching sizes...")
    for rating, _, size in navidrome.fetch_rated_totals():
        match rating:
            case 5 | 4 | 3:
                total_backup_size += size
            case 2 | 1:
                total_deletion_size += size
    console.print(f"[blue]Total backup size: {total_backup_size / 1e9:.1f} GB")
    console.print(f"[blue]Total deletion size: {total_deletion_size / 1e9:.1f} GB")
    songs = list(navidrome.fetch_rated_songs())
    for entry in songs:
        src = entry.path
        dest = args.target / entry.path.relative_to(SERVER_ROOT)
        match entry.rating:
            case 5 | 4 | 3:
                console.print(f"Would [green]copy[/] {src} to {dest}")
            case 2:
                console.print(f"Would [red]delete[/] {dest}")
            case 1:
                console.print(f"Would [red]delete[/] {src}")
                console.print(f"Would [red]delete[/] {dest}")
            case _:
                logger.warning("Ignoring %r", entry.path)
    if args.dry_run or not Confirm.ask("[blue]Continue?"):
        console.status("Dry run complete")
        return

    progress = Progress()
    with progress:
        for entry in progress.track(songs):
            src = entry.path
            dest = args.target / entry.path.relative_to(SERVER_ROOT)
            match entry.rating:
                case 5 | 4 | 3:
                    copyfile(src, dest)
                case 2:
                    if dest.is_file():
                        trash(dest)
                case 1:
                    if src.is_file():
                        trash(src)
                    if dest.is_file():
                        trash(dest)


if __name__ == "__main__":
    import sys

    logging.basicConfig(
        level=logging.DEBUG if __debug__ else logging.WARNING,
        format="%(levelname)s: %(message)s",
        handlers=[RichHandler()],
    )

    sys.exit(main())
