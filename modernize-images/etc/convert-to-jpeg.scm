(define (convert-to-jpeg filename outpath)
    (let* (
            (backgroundColor (car (gimp-context-get-background)))
            (image (car (gimp-file-load RUN-NONINTERACTIVE filename filename )))
            (drawable (car (gimp-image-merge-visible-layers image CLIP-TO-IMAGE)))
            )
        (begin (display "Exporting ")(display filename)(display " -> ")(display outpath)(newline))
        (file-jpeg-save RUN-NONINTERACTIVE image drawable outpath outpath 0.66 0.01 1 0 "" 2 1 0 0)
        (gimp-image-delete image)
        (gimp-context-set-background backgroundColor)
    )
)

(gimp-message-set-handler ERROR-CONSOLE)

