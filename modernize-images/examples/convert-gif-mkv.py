# you probably need a shebang when using shiv #!
import fileinput
from pathlib import Path
from subprocess import run

from modernize_images.ffprobe import ffprobe, VideoResult

import sys


def trash(paths):
    return run(["gio", "trash", *paths])


args = sys.argv[1:] or [line.rstrip() for line in fileinput.input()]
paths = []
for arg in args:
    path = Path(arg)
    if path.is_dir():
        paths.extend(path.glob("*.gif"))
    else:
        paths.append(path)
for input_path in paths:
    output_path = input_path.with_suffix(".mkv")
    if output_path.exists():
        continue
    if isinstance(probe_result := ffprobe(input_path), VideoResult):
        command = ["ffmpeg", "-i", input_path, "-codec:v", "libx265", output_path]
        if run(command).returncode == 0:
            if output_path.stat().st_size < input_path.stat().st_size:
                trash([input_path])
            else:
                trash([output_path])
