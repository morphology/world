import pytest

from pathlib import Path

from modernize_images.ffprobe import ffprobe, AudioResult, ProbeResult, VideoResult

SAMPLES_DIR = Path(__file__).parent / "../samples"


@pytest.mark.parametrize(
    "suffix,expected_class",
    [
        (".animated.gif", VideoResult),
        (".jpg", ProbeResult),
        (".m4a", AudioResult),
        (".mov", VideoResult),
        (".mp3", AudioResult),
        (".png", ProbeResult),
        (".static.gif", ProbeResult),
        (".webp", ProbeResult),
    ],
)
def test_probe(suffix, expected_class, request):
    """
    Should return a result object of the correct type.
    """
    input_path = SAMPLES_DIR / f"{request.node.name}{suffix}"
    assert input_path.is_file()
    result = ffprobe(input_path)
    assert type(result) is expected_class
