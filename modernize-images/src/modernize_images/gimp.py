#! /usr/bin/env python3
"""
Convert layered images to JPEG
"""
import argparse
import fileinput
import sys

import logging
from subprocess import run
import os
from pathlib import Path
from typing import Dict, Iterable, Union

logger = logging.getLogger(__name__)


def _create_scheme(path_map: Dict[os.PathLike, os.PathLike]):
    if not path_map:
        return

    lines = []
    for input_path, output_path in path_map.items():
        lines.append(f'(convert-to-jpeg "{input_path}" "{output_path}")')
    if lines:
        return "\n".join([*lines, "(gimp-quit 0)"]) + "\n"


def convert(path_map: Union[Iterable[os.PathLike], Dict[os.PathLike, os.PathLike]]):
    if not isinstance(path_map, dict):
        paths, path_map = path_map, {}
        for input_path in paths:
            input_path = Path(input_path)
            assert input_path.is_file()
            for output_path in [
                input_path.with_suffix(x) for x in (".jpg", ".JPG", ".jpeg", ".JPEG")
            ]:
                if output_path.exists():
                    break
            else:
                output_path = input_path.with_suffix(".jpeg")
            if not output_path.exists():
                path_map[input_path] = output_path
            elif output_path.stat().st_mtime < input_path.stat().st_mtime:
                logger.debug(f"Overwriting {output_path}")
                path_map[input_path] = output_path
            else:
                logger.warning(f"Skipping {input_path}")
    if script := _create_scheme(path_map):
        command = ["flatpak", "run", "org.gimp.GIMP", "-i", "-b", "-"]
        run(command, text=True, input=script)
    return path_map


def main():
    interactive = sys.stdin.isatty()
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter, description=__doc__
    )
    parser.add_argument(
        "input_paths",
        nargs="+" if interactive else "*",
        metavar="IMAGE OR FOLDER",
        type=Path,
    )
    args = parser.parse_args()
    paths = []
    if args.input_paths:
        for path in args.input_paths:
            if path.is_dir():
                for pattern in {"*.psd", "*.xcf"}:
                    paths.extend(path.glob(pattern))
            else:
                paths.append(path)
    else:
        paths = [Path(line.rstrip()) for line in fileinput.input()]
    if not convert(paths):
        return "No files converted"


if __name__ == "__main__":
    sys.exit(main())
