#! /usr/bin/env python3
"""
Use dezgo.com to upscale images.
"""
import argparse
import fileinput
import logging
import sys

import os
from datetime import datetime
from pathlib import Path

import exif
import httpx
from pgmagick.api import Image
from rich.progress import Progress

logger = logging.getLogger(__name__)


def get_creation_time(path: os.PathLike, timestamp_format: str = "%Y:%m:%d %H:%M:%S"):
    i = exif.Image(path)
    if i.has_exif:
        exif_creation_time = (
            i.get("datetime_original")
            or i.get("datetime_digitized")
            or i.get("datetime")
        )
        exif_timezone = datetime.strptime(i.get("offset_time"), "%z").tzinfo
        return datetime.strptime(exif_creation_time, timestamp_format).astimezone(
            exif_timezone
        )


class DezgoRunner:
    def __init__(self, token=os.getenv("DEZGO_TOKEN")):
        self.token = token
        self.client = httpx.Client(
            headers={
                "Accept": "application/json",
                "X-Dezgo-Key": self.token,
            },
            base_url="https://api.dezgo.com/",
        )

    def __del__(self):
        self.client.close()

    def post_image(
        self, api, input_path: os.PathLike, output_path: os.PathLike, data=None
    ):
        with open(input_path, "rb") as fi:
            response = self.client.post(
                api,
                files={"image": fi},
                data=data,
                timeout=20,
            )
            if response.status_code == httpx.codes.OK:
                with output_path.open("wb") as fo:
                    for data in response.iter_bytes():
                        fo.write(data)
            response.raise_for_status()

    def upscale(self, input_path: os.PathLike, output_path: os.PathLike = None):
        input_path = Path(input_path)
        if output_path is None:
            timestamp = get_creation_time(input_path)
            output_path = (
                input_path.parent
                / f"upscaled,{timestamp:%Y%m%d}"
                / input_path.with_suffix(".png").name
            )
        output_path.parent.mkdir(parents=True, exist_ok=True)
        self.post_image("upscale", input_path, output_path)


def main():
    interactive = sys.stdin.isatty()
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter, description=__doc__
    )
    parser.add_argument(
        "input_paths", nargs="+" if interactive else "*", metavar="IMAGE", type=Path
    )

    args = parser.parse_args()
    with (progress := Progress()):
        paths = []
        for path in progress.track(
            args.input_paths or [Path(line.rstrip()) for line in fileinput.input()],
            description="Checking dimensions...",
        ):
            image = Image(path)
            dimensions = (image.width, image.height)
            if max(dimensions) > 768:
                logger.info(f"Skipping {dimensions} {path}")
            else:
                paths.append(path)

        runner = DezgoRunner()
        n_upscaled = 0
        for path in progress.track(paths, description="Upscaling..."):
            runner.upscale(path)
            n_upscaled += 1
    return n_upscaled > 0


if __name__ == "__main__":
    sys.exit(main())
