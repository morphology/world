"""
Batch converter for images.
"""

import logging
import os
import subprocess
import sys
from pathlib import Path
from typing import Iterable

import anyio

from . import client
from . import convert

logger = logging.getLogger(__name__)

if (selected_file_paths := os.getenv("NAUTILUS_SCRIPT_SELECTED_FILE_PATHS")) is not None:
    selected_file_paths = [Path(p).absolute() for p in selected_file_paths.split("\n") if p]


async def thumbnail(input_path, output_path, size) -> None:
    async with client.GimpClient(scripts=("mask.scm", "thumbnail.scm")) as gimp:
        is_error, message = await gimp.dispatch(
            """\
            (create-thumbnail "{input_path}" "{output_path}" {size})
            """.format(
                input_path=client.scheme_path(input_path),
                output_path=client.scheme_path(output_path),
                size=size,
            )
        )
    assert not is_error, message


def shell_thumbnail() -> None:
    input_path, output_path, size = sys.argv[1:]
    input_path = Path(input_path).absolute()
    output_path = Path(output_path).absolute()
    anyio.run(thumbnail, input_path, output_path, size)


def trash_files_sync(paths: Iterable[os.PathLike]) -> None:
    command = ["gio", "trash", *paths]
    subprocess.run(command, check=True)


def nautilus_batch_compress_lossy():
    global selected_file_paths
    if selected_file_paths:
        anyio.run(convert.batch_compress_lossy, selected_file_paths, lambda f: trash_files_sync([f]))
    subprocess.Popen(["notify-send", "Compression complete"])
