# TODO: asserts
import decimal
import json
import logging
import os
import subprocess
from pathlib import Path
from typing import Literal, Optional, TypedDict

import anyio

__all__ = ["get_ffprobe", "compress_video"]

FFMPEG_ENCODING_ARGS: tuple[str] = (
    "-hide_banner",
    "-vf",
    "scale=ceil(iw/2)*2:ceil(ih/2)*2",
    "-pix_fmt",
    "yuv420p",
    "-crf",
    "18",
)
logger = logging.getLogger(__name__)


class FormatMetadata(TypedDict, total=False):
    duration: str
    format_name: str
    size: str


class StreamMetadata(TypedDict, total=False):
    index: int
    codec_type: Literal["video", "audio", "subtitle"]


class VideoMetadata:
    def __init__(self):
        self.format: Optional[FormatMetadata] = None
        self.audio_streams: list[StreamMetadata] = []
        self.video_streams: list[StreamMetadata] = []

    @classmethod
    def from_ffprobe(cls, text):
        result = cls()
        for section, content in json.loads(text).items():
            match section:
                case "format":
                    result.format = FormatMetadata(**content)
                case "streams":
                    for stream in content:
                        stream = StreamMetadata(**stream)
                        match stream["codec_type"]:
                            case "audio":
                                result.audio_streams.append(stream)
                            case "video":
                                result.video_streams.append(stream)
                            case _:
                                logger.warning(f"Skipping stream {stream}")
                case _:
                    logger.log(
                        level=logging.WARNING if content else logging.DEBUG,
                        msg=f"Skipping {section}: {content}",
                    )
        return result

    def to_text(self) -> str:
        return json.dumps(
            {
                "format": self.format,
                "streams": [*self.video_streams, *self.audio_streams],
            },
            indent=2,
        )

    def get_resolution(self) -> Optional[str]:
        stream = max(self.video_streams, key=lambda s: int(s["width"]) * int(s["height"]))
        return int(stream["width"]), int(stream["height"])

    def get_duration(self) -> Optional[decimal.Decimal]:
        try:
            return decimal.Decimal(self.format["duration"])
        except Exception as e:
            logger.exception(e)

    def get_bit_rate(self) -> Optional[float]:
        try:
            return float(self.format["bit_rate"]) / 1e6  # Mbps
        except Exception as e:
            logger.exception(e)

    def __repr__(self):
        width, height = self.get_resolution()
        bit_rate = self.get_bit_rate()
        n_streams = len(self.video_streams) + len(self.audio_streams)
        for stream in self.video_streams:
            if "nb_frames" in stream:
                n_frames = int(stream["nb_frames"])
                break
        else:
            n_frames = None
        is_image = self.format["format_name"] == "image2"
        if is_image:
            if bit_rate is None or n_frames is None or n_frames == 1:
                blurb = f"{width}x{height} {self.format['format_name']} image"
        else:
            if duration := self.get_duration():
                duration = f"{duration:.2f} s"
                if bit_rate:
                    bit_rate = f"{bit_rate:.1f} Mbps"
            blurb = (
                f"{width}x{height} {bit_rate=} {self.format['format_name']} video "
                f"{duration=} {n_frames=} {n_streams=} "
            )
            for stream in self.video_streams:
                if codec_name := stream.get("codec_name"):
                    blurb += f"{codec_name} "
            for stream in self.audio_streams:
                if codec_name := stream.get("codec_name"):
                    blurb += f"{codec_name} "
        return f"<VideoMetadata {blurb.strip()}>"


async def get_ffprobe(
    path: os.PathLike,
    ffprobe_args: tuple = (
        "-hide_banner",
        "-count_packets",
        "-show_entries",
        "chapters:format:stream",
    ),
) -> VideoMetadata | None:
    command = [
        "ffprobe",
        *ffprobe_args,
        "-print_format",
        "json",
        str(path),
    ]
    result = await anyio.run_process(command, check=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    assert result.returncode == 0, f"Error running ffprobe {path}: {result.stderr.decode()}"
    return VideoMetadata.from_ffprobe(text=result.stdout.decode())


async def compress_video(
    path: os.PathLike,
    output_path: Optional[os.PathLike] = None,
    encoding_args: tuple[str] = FFMPEG_ENCODING_ARGS,
    signature_path: Optional[os.PathLike] = None,
    vmafmotion_path: Optional[os.PathLike] = None,
) -> os.PathLike:
    path = Path(path)
    output_path = Path(output_path or path.with_suffix(".mkv"))
    assert path != output_path
    null_filters = []
    if signature_path:
        null_filters.append(f"signature=filename={signature_path}")
    if vmafmotion_path:
        null_filters.append(f"vmafmotion=stats_file={vmafmotion_path}")
    command = [
        "ffmpeg",
        "-nostdin",
        "-i",
        str(path),
    ]
    if output_path:
        command.extend(encoding_args)
        command.append(str(output_path))
    else:
        assert null_filters
    if null_filters:
        command.extend(["-vf", ",".join(null_filters), "-f", "null", "-"])
    result = await anyio.run_process(command, check=False, stderr=subprocess.PIPE)
    assert result.returncode == 0, f"Error running ffmpeg {path}: {result.stderr.decode()}"
    return output_path


async def main():
    import sys

    async def write_info(video_path, metadata_path):
        info = await get_ffprobe(video_path)
        print(info.format["filename"], info)
        metadata_path.write_text(info.to_text())

    n_errors = 0
    try:
        async with anyio.create_task_group() as tg:
            for arg in sys.argv[1:]:
                arg = Path(arg)
                tg.start_soon(write_info, arg, Path(f"{arg.with_suffix('')}-info.json"))
    except* Exception as excgroup:
        n_errors += len(excgroup.exceptions)
        for exc in excgroup.exceptions:
            logger.exception(exc)
    if n_errors > 0:
        sys.exit(10)


if __name__ == "__main__":
    anyio.run(main)
