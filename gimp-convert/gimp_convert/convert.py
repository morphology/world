"""
Batch converter for .XCF GIMP layers
"""

import logging
import os
import subprocess
import tempfile
import warnings
from pathlib import Path
from typing import Callable, Iterable, Optional

import anyio
import pyvips

from . import client
from . import ffmpeg

logger = logging.getLogger(__name__)


async def compress_photo(path: os.PathLike, output_path: Optional[os.PathLike] = None) -> os.PathLike:
    ((_, output_path),) = await compress_photos({path: output_path} if output_path else [path])
    return output_path


async def compress_photos(paths: Iterable[os.PathLike]) -> list[tuple[os.PathLike, os.PathLike]]:
    if not isinstance(paths, dict):
        path_list = paths
        paths = {p: Path(p).with_suffix(".jxl") for p in path_list}
    xcf_path_pairs, other_path_pairs = [], []
    for path, output_path in paths.items():
        match path.suffix.lower():
            case ".xcf":
                xcf_path_pairs.append((path, output_path))
            case _:
                other_path_pairs.append((path, output_path))
    for input_path, output_path in other_path_pairs:
        result = await anyio.run_process(
            ["cjxl", str(input_path), str(output_path)], check=False, stderr=subprocess.PIPE
        )
        assert result.returncode == 0, f"Error compressing {input_path}: {repr.stderr.decode()}"
    if xcf_path_pairs:
        async with client.GimpClient() as gimp:
            for input_path, output_path in xcf_path_pairs:
                is_error, message = await gimp.convert(input_path=input_path, output_path=output_path)
                assert not is_error, f"Error converting {input_path}: {message}"
    return [*other_path_pairs, *xcf_path_pairs]


async def uncompress_photo(path: os.PathLike, output_path: os.PathLike, quality: Optional[float] = None) -> os.PathLike:
    path = Path(path)
    output_path = Path(output_path)
    assert path != output_path
    djxl_args = []
    if quality:
        djxl_args.append(f"--jpeg_quality={quality}")
    result = await anyio.run_process(["djxl", *djxl_args, str(path), str(output_path)], check=False)
    assert result.returncode == 0, f"Error uncompressing {path}: {result.stderr.decode()}"
    return output_path


async def compress_lossy(
    paths: Iterable[os.PathLike], jpeg_quality: Optional[float] = 68
) -> list[tuple[os.PathLike, os.PathLike]]:
    if not isinstance(paths, dict):
        path_list = paths
        paths = {p: None for p in path_list}
    animation_path_pairs, photo_path_pairs = [], []
    for input_path, output_path in paths.items():
        if not output_path:
            match input_path.suffix.lower():
                case ".xcf":
                    output_path = input_path.with_suffix(".jxl")
                case _:
                    image = await anyio.to_thread.run_sync(pyvips.Image.new_from_file, input_path)
                    if image.get_n_pages() > 1:
                        output_path = input_path.with_suffix(".mkv")
                    else:
                        output_path = input_path.with_suffix(".jxl")
        match output_path.suffix.lower():
            case ".mkv":
                animation_path_pairs.append((input_path, output_path))
            case _:
                photo_path_pairs.append((input_path, output_path))
    photo_path_results = await compress_photos(dict(photo_path_pairs))
    animation_path_results = []
    for input_path, output_path in animation_path_pairs:
        result = await ffmpeg.compress_video(
            input_path,
            signature_path=f"{output_path.with_suffix('')}-signature.bin",
            vmafmotion_path=f"{output_path.with_suffix('')}-vmafmotion.txt",
        )
        assert result.is_file(), f"Error compressing {input_path}: {result}"
        animation_path_results.append((input_path, output_path))
    return [*photo_path_results, *animation_path_results]


async def batch_compress_lossy(args: list[os.PathLike], trash: Optional[Callable] = None) -> None:
    paths = []
    for path in args:
        path = Path(path)
        if path.is_file():
            paths.append(path)
        else:
            paths.extend(path.glob("**/*.gif"))
            paths.extend(path.glob("**/*.png"))
            paths.extend(path.glob("**/*.xcf"))
    with tempfile.TemporaryDirectory() as td:
        td = Path(td)
        for input_path, output_path in await compress_lossy(paths):
            keep_original = input_path.suffix.lower() == ".xcf"
            is_animation = output_path.suffix.lower() == ".mkv"
            if not is_animation:
                for ext in (".jpg", ".jpeg", ".jfif"):
                    if (photo_path := input_path.with_suffix(ext)).exists():
                        break
                else:
                    photo_path = input_path.with_suffix(".jpeg")
                if input_path == photo_path and keep_original:
                    continue
                elif photo_path.exists():
                    new_photo = td / "temp.jpeg"
                    await uncompress_photo(output_path, new_photo)
                    if new_photo.stat().st_size < photo_path.stat().st_size:
                        os.rename(new_photo, photo_path)
                else:
                    await uncompress_photo(output_path, photo_path)
            if input_path.stat().st_size > output_path.stat().st_size:
                if callable(trash) and not keep_original:
                    try:
                        trash(input_path)
                    except Exception as e:
                        logger.exception(e)
            else:
                logger.warning(f"Lossy compression on {str(input_path)} did not result in a smaller file")


if __name__ == "__main__":
    import sys

    logging.basicConfig(level=logging.DEBUG if __debug__ else logging.WARNING, format="%(message)s")
    logging.getLogger("httpcore").setLevel(logging.WARNING)
    logging.getLogger("pyvips").setLevel(logging.INFO)
    warnings.simplefilter("ignore", ResourceWarning)
    anyio.run(batch_compress_lossy, sys.argv[1:])
