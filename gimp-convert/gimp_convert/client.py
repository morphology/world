import importlib.metadata
import logging
import os
import socket
import struct
import subprocess
import tempfile
import uuid
from dataclasses import dataclass
from pathlib import Path
from textwrap import dedent
from typing import Iterable, Mapping, Optional

import anyio
import pyvips

JXL_DISTANCE = 3.0  # 0 for lossless
PACKAGE = importlib.metadata.distribution("gimp_convert")
PACKAGED_SCRIPT_FU = PACKAGE.locate_file("gimp_convert/script-fu")

logger = logging.getLogger(__name__)


def header_encode(content):
    header = struct.pack(">c H", b"G", len(content))
    return header


def header_decode(payload, header_unpacker=struct.Struct(">c b H")):
    magic, error_code, content_len = header_unpacker.unpack(payload[:4])
    assert magic == b"G"
    return {
        "error": error_code,
        "content_len": content_len,
    }, payload[4:]


def scheme_map(m: Mapping):
    return " ".join(f"({k} . {v})" for k, v in m.items())


def scheme_path(path):
    return str(Path(path).resolve()).replace('"', '""')


@dataclass
class GimpLayerInfo:
    size: tuple[float, float]
    offset: Optional[tuple[float, float]] = None
    mask: Optional[pyvips.Image] = None

    @classmethod
    def from_layer(cls, other):
        return cls(size=other.size, offset=other.offset, mask=other.mask.copy_memory() if other.mask else None)

    def to_plist(self, path):
        offset = self.offset or (0, 0)
        return f"""\
            '(
              (path . "{scheme_path(path)}")
              (offset-x . {round(offset[0])})
              (offset-y . {round(offset[1])})
            )
            """

    def resize(self, scale):
        new_mask = None
        if self.mask:
            new_mask = self.mask.copy_memory()
            if scale != 1.0:
                new_mask = new_mask.resize(scale)
        return GimpLayerInfo(
            size=tuple(c * scale for c in self.size), offset=tuple(c * scale for c in self.offset), mask=new_mask
        )


def find_unused_port(start_port=49152, end_port=65535):
    for port in range(start_port, end_port + 1):
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.bind(("localhost", port))
                return port
        except OSError as e:
            if e.errno == socket.errno.EADDRINUSE:  # Port is already in use
                continue
            else:
                raise


class GimpClient:
    def __init__(self, scripts=("mask.scm",), port=None):
        self.host, self.port = "localhost", port
        self.scripts = []
        for script_path in scripts:
            if Path(script_path).is_absolute():
                self.scripts.append(Path(script_path))
            else:
                self.scripts.append(PACKAGED_SCRIPT_FU / script_path)

    async def dispatch(self, scheme) -> tuple[int, str]:
        async with await anyio.connect_tcp(self.host, self.port) as client:
            logger.debug(f"Running {scheme}")
            scheme = scheme.encode()
            await client.send(header_encode(scheme) + scheme)
            payload = b""
            while len(payload) < 4:
                if fragment := await client.receive(4):
                    payload += fragment
                else:
                    break
            header, content = header_decode(payload)
            while len(content) < header["content_len"]:
                if fragment := await client.receive(1024):
                    content += fragment
                else:
                    break
        return header["error"], content.decode()

    async def check(self):
        try:
            is_error, message = await self.dispatch("#t")  # True
            if is_error:
                logger.error(message)
            else:
                return True
        except OSError as e:
            logger.debug(f"Suppressed {type(e).__name__}")
        except Exception as e:
            logger.exception(e)
        return False

    async def __aenter__(self):
        if self.port is None:
            await self.start()
        if self.scripts:
            await self.load_paths(self.scripts)
        await self.set_jpeg_options("")  # implants function
        await self.set_png_options("")  # implants function
        await self.set_webp_options("")  # implants function
        return self

    async def start(self):
        self.port = find_unused_port()
        log_path = ""
        command = [
            "gimp-console",
            "-icd",
            "-b",
            f'( plug-in-script-fu-server RUN-NONINTERACTIVE "{self.host}" {self.port} "{"" if log_path is None else str(log_path)}" )',
        ]
        command = [
            "systemd-run",
            "--user",
            "--collect",
            f"--unit=gimp-{uuid.uuid1()}",
            f"--description=GIMP server under process {os.getpid()}",
            *command,
        ]
        logger.debug(f"Running {command}")
        await anyio.run_process(command, check=True)
        for retry in range(10):
            if await self.check():
                logger.debug(f"Starting {self} took {retry} s")
                break
            await anyio.sleep(1.0)
        else:
            raise TimeoutError(f"{command} failed to connect after {retry} s")

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.stop(force_quit=True)

    async def stop(self, force_quit=None):
        try:
            is_error, message = await self.dispatch(f"( gimp-quit {'TRUE' if force_quit else 'FALSE'} )")
        except anyio.EndOfStream:
            logger.debug("Successfully quit GIMP")
        else:
            assert not is_error, message

    async def load_paths(self, paths: Iterable[os.PathLike]) -> bool:
        """Load Scheme syntax from file(s)"""
        assert not isinstance(paths, str)
        n_failures = 0
        for path in paths:
            is_error, result = await self.dispatch(path.read_text())
            if is_error:
                logger.error(f"Failed to import '{path}': {result}")
                n_failures += 1
        return n_failures == 0

    async def extract_layer(self, path, workdir, do_merge_visible=True) -> tuple[pyvips.Image, GimpLayerInfo]:
        workdir = Path(tempfile.mkdtemp(dir=workdir, prefix="extract_layer-"))
        is_error, message = await self.dispatch(
            dedent(
                r"""
                (let* (
                        (image (car (gimp-file-load RUN-NONINTERACTIVE "{input_path}" "{input_path}")))
                        (top-layer (car (gimp-image-get-active-layer image)))
                        (info (extract-mask image top-layer "{mask_path}"))
                        (layer (if {do_merge_visible}
                                    (car (gimp-image-merge-visible-layers image CLIP-TO-IMAGE))
                                    top-layer
                                 ))
                        (tab-out (open-output-file "{info_tab_path}"))
                      )
                  (begin
                    (gimp-file-save RUN-NONINTERACTIVE image layer "{output_path}" "{output_path}")
                    (display (cdr (assoc 'has-mask (cdr info))) tab-out)
                    (write-char #\tab tab-out)
                    (display (cdr (assoc 'offset-x (cdr info))) tab-out)
                    (write-char #\tab tab-out)
                    (display (cdr (assoc 'offset-y (cdr info))) tab-out)
                    (newline tab-out)
                  )
                )
                """.format(
                    input_path=scheme_path(path),
                    mask_path=scheme_path(mask_path := workdir / "mask.png"),
                    output_path=scheme_path(transfer_path := workdir / "transfer.png"),
                    info_tab_path=scheme_path(info_tab_path := workdir / "info.tab"),
                    do_merge_visible="#t" if do_merge_visible else "#f",
                )
            )
        )
        assert not is_error, message
        has_mask, offset_x, offset_y = info_tab_path.read_text().rstrip().split("\t")
        has_mask = {"#t": True, "#f": False}.get(has_mask)
        image = pyvips.Image.new_from_file(str(transfer_path))
        return image, GimpLayerInfo(
            size=(image.width, image.height),
            offset=(int(offset_x), int(offset_y)),
            mask=pyvips.Image.new_from_file(str(mask_path)) if has_mask else None,
        )

    async def set_jpeg_options(self, options: str | Mapping):
        """Define or re-define the function save-jpeg"""
        if not isinstance(options, str):
            options = scheme_map(options)
        is_error, message = await self.dispatch(
            dedent(
                f"""
                (define (save-jpeg image layer output-path)
                  (let (
                         (options '(
                                     ;; given:
                                     {options}
                                     ;; defaults:
                                     (baseline . 1)
                                     (comment . "")
                                     (dct . 'float-dct)
                                     (optimize . 1)
                                     (progressive . 1)
                                     (quality . 0.66)
                                     (restart . 0)
                                     (smoothing . 0.05)
                                     (subsmp . 'horizontal-chroma-halved)
                                     )
                           )
                         (orig-color (car (gimp-context-get-background)))
                         )
                    (file-jpeg-save RUN-NONINTERACTIVE
                      image
                      (or layer (car (gimp-image-merge-visible-layers image CLIP-TO-IMAGE)))
                      output-path output-path
                      (cdr (assoc 'quality options))
                      (cdr (assoc 'smoothing options))
                      (cdr (assoc 'optimize options))
                      (cdr (assoc 'progressive options))
                      (cdr (assoc 'comment options))
                      (cdr (assoc
                             (cdr (assoc 'subsmp options))
                             '(
                                ('chroma-quartered . 0)
                                ('chroma-halved . 1)
                                ('horizontal-chroma-halved . 1)
                                ('best-quality . 2)
                                ('vertical-chroma-halved . 3)
                                )
                             ))
                      (cdr (assoc 'baseline options))
                      (cdr (assoc 'restart options))
                      (cdr (assoc
                             (cdr (assoc 'dct options))
                             '(
                                ('integer-dct . 0)
                                ('fixed-dct . 1)
                                ('float-dct . 2)
                                )
                             ))
                      )
                    (gimp-image-delete image)
                    (gimp-context-set-background orig-color)
                    (gimp-displays-flush)
                    )
                  (unless (file-exists? output-path) (throw 'programming-error "Output file missing"))
                )"""
            )
        )
        assert not is_error, message

    async def set_png_options(self, options: str | Mapping):
        """Define or re-define the function save-png"""
        if not isinstance(options, str):
            options = scheme_map(options)
        is_error, message = await self.dispatch(
            dedent(
                f"""
                (define (save-png image layer output-path)
                  (let (
                         (options '(
                              ;; given:
                              {options}
                              ;; defaults:
                              (bkgd . 0)
                              (comment . 0)
                              (compression . 9)
                              (gama . 0)
                              (include-time . 0)
                              (interlace . 0)
                              (offs . 0)
                              (phys . 0)
                              (svtrans . 1)
                              )
                          )
                         (layer (or layer (car (gimp-image-merge-visible-layers image CLIP-TO-IMAGE))))
                         (orig-color (car (gimp-context-get-background)))
                         )
                    (file-png-save2 RUN-NONINTERACTIVE
                      image layer output-path output-path
                      (cdr (assoc 'interlace options))
                      (cdr (assoc 'compression options))
                      (cdr (assoc 'bkgd options))
                      (cdr (assoc 'gama options))
                      (cdr (assoc 'offs options))
                      (cdr (assoc 'phys options))
                      (cdr (assoc 'include-time options))
                      (cdr (assoc 'comment options))
                      (cdr (assoc 'svtrans options))
                      )
                    (gimp-image-delete image)
                    (gimp-context-set-background orig-color)
                    (gimp-displays-flush)
                    )
                  (unless (file-exists? output-path) (throw 'programming-error "Output file missing"))
                )"""
            )
        )
        assert not is_error, message

    async def set_webp_options(self, options: str | Mapping):
        """Define or re-define the function save-png"""
        if not isinstance(options, str):
            options = scheme_map(options)
        is_error, message = await self.dispatch(
            dedent(
                f"""
                (define (save-webp image layer output-path)
                  (let (
                         (options '(
                                     ;; given:
                                     {options}
                                     ;; defaults:
                                     (alpha-quality . 33)
                                     (animation . 0)
                                     (anim-loop . 0)
                                     (delay-time . 0)
                                     (exif . 0)
                                     (force-delay . 0)
                                     (iptc . 0)
                                     (kf-distance . 0)
                                     (lossless . 0)
                                     (minimize-size . 1)
                                     (preset . 'Photo)
                                     (quality . 33)
                                     (xmp . 0)
                                     ))
                         (orig-color (car (gimp-context-get-background)))
                         )
                    (file-webp-save RUN-NONINTERACTIVE
                      image
                      (or layer (car (gimp-image-merge-visible-layers image CLIP-TO-IMAGE)))
                      output-path output-path
                      (cdr (assoc
                             (cdr (assoc 'preset options))
                             '(
                                ('Default . 0)
                                ('Picture . 1)
                                ('Photo . 2)
                                ('Drawing . 3)
                                ('Icon . 4)
                                ('Text . 5)
                                )
                             ))
                      (cdr (assoc 'lossless options))
                      (cdr (assoc 'quality options))
                      (cdr (assoc 'alpha-quality options))
                      (cdr (assoc 'animation options))
                      (cdr (assoc 'anim-loop options))
                      (cdr (assoc 'minimize-size options))
                      (cdr (assoc 'kf-distance options))
                      (cdr (assoc 'exif options))
                      (cdr (assoc 'iptc options))
                      (cdr (assoc 'xmp options))
                      (cdr (assoc 'delay-time options))
                      (cdr (assoc 'force-delay options))
                      )
                    (gimp-image-delete image)
                    (gimp-context-set-background orig-color)
                    (gimp-displays-flush)
                    )
                  (unless (file-exists? output-path) (throw 'programming-error "Output file missing"))
                )"""
            )
        )
        assert not is_error, message

    async def convert(self, input_path, output_path, file_type: Optional[str] = None):
        input_path = Path(input_path)
        output_path = Path(output_path)
        if output_path.exists():
            output_path.rename(f"{output_path}~")
        match file_type or output_path.suffix.lstrip(".").lower():
            case "jpg" | "jpeg":
                is_error, message = await self.dispatch(
                    """
                    (save-jpeg (car (gimp-file-load RUN-NONINTERACTIVE "{input_path}" "{input_path}")) #f "{output_path}")
                    """.format(input_path=scheme_path(input_path), output_path=scheme_path(output_path))
                )
            case "jxl":
                with tempfile.NamedTemporaryFile(suffix=".png") as tf:
                    is_error, message = await self.dispatch(
                        """
                        (save-png (car (gimp-file-load RUN-NONINTERACTIVE "{input_path}" "{input_path}")) #f "{output_path}")
                        """.format(input_path=scheme_path(input_path), output_path=scheme_path(tf.name))
                    )
                    if not is_error:
                        subprocess.run(
                            ["cjxl", f"--distance={JXL_DISTANCE:0f}", tf.name, output_path], check=True, text=True
                        )
            case "png":
                is_error, message = await self.dispatch(
                    """
                    (save-png (car (gimp-file-load RUN-NONINTERACTIVE "{input_path}" "{input_path}")) #f "{output_path}")
                    """.format(input_path=scheme_path(input_path), output_path=scheme_path(output_path))
                )
            case "webp":
                is_error, message = await self.dispatch(
                    """
                    (save-webp (car (gimp-file-load RUN-NONINTERACTIVE "{input_path}" "{input_path}")) #f "{output_path}")
                    """.format(input_path=scheme_path(input_path), output_path=scheme_path(output_path))
                )
            case _:
                raise NotImplementedError()
        return is_error, message
