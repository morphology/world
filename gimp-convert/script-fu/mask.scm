(define (import-mask layer mask-path)
  (let* (
          (mask-image (car (gimp-file-load RUN-NONINTERACTIVE mask-path mask-path)))
          (mask-layer (car (gimp-image-get-active-layer mask-image)))
          )
    (add-mask layer mask-layer)
    (gimp-image-delete mask-image)
    )
  (gimp-displays-flush)
  )

(define (add-mask layer mask-layer)
  (unless (gimp-drawable-is-gray mask-layer) (gimp-image-convert-grayscale mask-image))

  (let (
         (new-mask (car (gimp-layer-create-mask layer ADD-WHITE-MASK)))
         )
    (gimp-layer-add-mask layer new-mask)
    (gimp-edit-named-copy mask-layer "mask")
    (gimp-floating-sel-anchor (car (gimp-edit-named-paste new-mask "mask" TRUE)))
    (gimp-buffer-delete "mask")
    )
  (unless (not (equal? (gimp-layer-get-mask layer) -1)) (throw 'programming-error "Failed to create mask"))
  )

(define (import-into-group image name layer-plists mask-path)
  (let (
          (new-group-layer (car (gimp-layer-group-new image)))
          )
    (begin
      (gimp-item-set-name new-group-layer (or name "Imported"))
      (gimp-image-insert-layer image new-group-layer 0 0)
      (for-each (lambda (plist) (let* (
                                            (layer-path (cdr (assoc 'path plist)))
                                            (offset-x (cdr (assoc 'offset-x plist)))
                                            (offset-y (cdr (assoc 'offset-y plist)))
                                            (new-layer (car (gimp-file-load-layer RUN-NONINTERACTIVE image layer-path)))
                                            )
                                       (begin
                                         (gimp-layer-set-offsets new-layer offset-x offset-y)
                                         (gimp-image-insert-layer image new-layer new-group-layer -1)
                                         )
                                       )) layer-plists)
      (gimp-image-resize-to-layers image)
      )
    (unless (not mask-path) (let*
                              (
                                (mask-image (car (gimp-file-load RUN-NONINTERACTIVE mask-path mask-path)))
                                (mask-layer (car (gimp-image-get-active-layer mask-image)))
                                )
                              (begin
                                (add-mask new-group-layer mask-layer)
                                (gimp-image-delete mask-image)
                                )
                              )
      )
    new-group-layer
    )
  )

(define (extract-mask image layer mask-path)
  (let* (
          (options '(
                      (bkgd . 1)
                      (comment . 0)
                      (compression . 9)
                      (gama . 0)
                      (include-time . 0)
                      (interlace . 0)
                      (offs . 0)
                      (phys . 0)
                      (svtrans . 0)
                      ))
          (width (car (gimp-image-width image)))
          (height (car (gimp-image-height image)))
          (layer (or layer (car (gimp-image-get-active-layer image))))
          (layer-coords (gimp-drawable-offsets layer))
          (mask (car (gimp-layer-get-mask layer)))
          (has-mask (not (equal? mask -1)))
          (info (list
                  (cons 'image-width width)
                  (cons 'image-height height)
                  (cons 'layer-width (car (gimp-drawable-width layer)))
                  (cons 'layer-height (car (gimp-drawable-height layer)))
                  (cons 'offset-x (car layer-coords))
                  (cons 'offset-y (cadr layer-coords))
                  (cons 'has-mask has-mask)
                  )
            )
          )

    (if has-mask
      (let* (
              (new-image (car (gimp-image-new width height GRAY)))
              (mask-layer (car (gimp-layer-new-from-drawable mask new-image)))
              )
        (begin
          (gimp-image-insert-layer new-image mask-layer 0 0)
          (file-png-save2 RUN-NONINTERACTIVE
            new-image mask-layer mask-path mask-path
            (cdr (assoc 'interlace options))
            (cdr (assoc 'compression options))
            (cdr (assoc 'bkgd options))
            (cdr (assoc 'gama options))
            (cdr (assoc 'offs options))
            (cdr (assoc 'phys options))
            (cdr (assoc 'include-time options))
            (cdr (assoc 'comment options))
            (cdr (assoc 'svtrans options))
            )
          (gimp-image-delete new-image)
          )
        )
      (gimp-message "No mask")
      )
    info
    )
  )
