;
; NB: The Script-Fu gimp-image-thumbnail was crashing for me
;
(define (get-thumbnail-dimensions image new-size)
  (let* (
          (width (car (gimp-image-width image)))
          (height (car (gimp-image-height image)))
          (aspect-ratio (/ width height))
          (scale (if (> aspect-ratio 1) (/ new-size width) (/ new-size height)))
        )
    (if (< scale 1)
      (list
        (round (* width scale))
        (round (* height scale))
      )
      (list width height)
    )
  )
)
(define (create-thumbnail input-path output-path size)
  (let* (
          (image (car (gimp-file-load RUN-NONINTERACTIVE input-path input-path)))
          (layer (car (gimp-image-merge-visible-layers image CLIP-TO-IMAGE)))
          (thumbnail-dimensions (get-thumbnail-dimensions image size))
          (options '(
                      (bkgd . 0)
                      (comment . 0)
                      (compression . 9)
                      (gama . 0)
                      (include-time . 1)
                      (interlace . 0)
                      (offs . 0)
                      (phys . 0)
                      (svtrans . 1)
                    )
          )
        )
    (begin
      (gimp-image-scale image (car thumbnail-dimensions) (cadr thumbnail-dimensions))
      (file-png-save2 RUN-NONINTERACTIVE
        image
        layer
        output-path
        output-path
        (cdr (assoc 'interlace options))
        (cdr (assoc 'compression options))
        (cdr (assoc 'bkgd options))
        (cdr (assoc 'gama options))
        (cdr (assoc 'offs options))
        (cdr (assoc 'phys options))
        (cdr (assoc 'include-time options))
        (cdr (assoc 'comment options))
        (cdr (assoc 'svtrans options))
      )
      (gimp-image-delete image)
      (gimp-displays-flush)
      (unless (file-exists? output-path) (throw 'programming-error "Output file missing"))
    )
  )
)