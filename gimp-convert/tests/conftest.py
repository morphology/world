from pathlib import Path

import pytest
from gimp_convert.client import GimpClient

TEST_INPUTS = Path("tests/test-inputs")


@pytest.fixture
async def gimp_client_instance():
    async with GimpClient() as gimp:
        yield gimp
