import pytest

import logging


from .conftest import *

logger = logging.getLogger(__name__)


@pytest.mark.anyio
async def test_good_scripts(gimp_client_instance):
    success = await gimp_client_instance.load_paths([Path("script-fu/mask.scm")])
    assert success


@pytest.mark.anyio
async def test_bad_scripts(gimp_client_instance):
    success = await gimp_client_instance.load_paths(
        [TEST_INPUTS / "script-fu/throw.scm", TEST_INPUTS / "script-fu/garbage.scm"]
    )
    assert not success


@pytest.mark.anyio
async def test_extract_top_layer(gimp_client_instance, tmp_path):
    image, layer_info = await gimp_client_instance.extract_layer(
        TEST_INPUTS / "layer-cropped/restaurant-1200-layer-cropped.xcf", do_merge_visible=False, workdir=tmp_path
    )
    assert layer_info.mask is not None
    assert layer_info.size == (image.width, image.height)


@pytest.mark.anyio
async def test_extract_merged_layers(gimp_client_instance, tmp_path):
    image, info = await gimp_client_instance.extract_layer(
        TEST_INPUTS / "layer-cropped/restaurant-1200-layer-cropped.xcf", do_merge_visible=True, workdir=tmp_path
    )
    assert (image.width, image.height) == (1200, 793)
