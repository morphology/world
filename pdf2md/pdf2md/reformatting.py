import re
import textwrap
from functools import cache

all_dashes = [
    "\N{EM DASH}",
    "\N{EN DASH}",
    "\N{FIGURE DASH}",
    "\N{HORIZONTAL BAR}",
    "\N{HYPHEN}",
    "\N{HYPHEN-MINUS}",
    "\N{NON-BREAKING HYPHEN}",
]

unicode_bullets = "".join(
    [
        "\N{BLACK CIRCLE}",  # ●
        "\N{BLACK SMALL SQUARE}",  # ▪
        "\N{BLACK SQUARE}",  # ■
        "\N{BULLET}",  # •
        "\N{MIDDLE DOT}",  # ·
        "\N{WHITE BULLET}",  # ◦
        "\uf0b7",  # 
        "\uf8e7",  # 
        "➢",
    ]
)
ascii_bullets = "*.-"  # -, if present, must be last
indent_re = re.compile(rf"^\s*[{unicode_bullets}{ascii_bullets}]\s*")


@cache
def _get_wrapper(width=150, initial_indent="", subsequent_indent=""):
    return textwrap.TextWrapper(
        width=width,
        initial_indent=initial_indent,
        subsequent_indent=subsequent_indent,
        expand_tabs=False,
        fix_sentence_endings=True,
    )


def reformat_paragraphs(paragraphs):
    markdown = ""
    for paragraph in paragraphs:
        markdown += "\n"
        if paragraph.strip():
            markdown += maybe_unordered_list(paragraph)
    return markdown + "\n"


def maybe_unordered_list(text):
    if m := indent_re.match(text):
        indent = "".join(" " if c == " " else "-" for c in m.group(0))
        text = text[len(indent) :]
        wrapper = _get_wrapper(
            initial_indent=indent, subsequent_indent=" " * len(indent)
        )
    else:
        wrapper = _get_wrapper()
    return wrapper.fill(text) + "\n"
