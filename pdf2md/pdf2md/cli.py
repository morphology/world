import argparse
from pathlib import Path

from .converter import docx_to_md, pdf_to_md


def setup_parser(parser):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__,
        prog="pdf2md",
    )
    parser.add_argument("--outdir", "-o", type=Path)
    parser.add_argument("paths", nargs="+", type=Path, metavar="FILE")
    return parser


def run_convert(args):
    for arg in args.paths:
        input_path = Path(arg)
        name = input_path.stem
        output_dir = args.outdir or input_path.parent / f"{name.replace(' ', '_')}"
        output_dir.mkdir(exist_ok=True, parents=True)

        if input_path.suffix in {".docx", ".odt"}:
            docx_to_md(input_path, output_dir)
        elif input_path.suffix == ".pdf":
            pdf_to_md(input_path, output_dir)
        else:
            raise NotImplementedError(arg)

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__,
        prog="pdf2md",
    )
    setup_parser(parser)
    args = parser.parse_args()
    return run_convert(args)
