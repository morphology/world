import gsapi
import os
import shutil
import sys
import tempfile
from pathlib import Path
from tqdm import tqdm
from typing import Dict, Tuple

if "TESSDATA_PREFIX" not in os.environ:
    os.environ["TESSDATA_PREFIX"] = "/usr/local/share/tessdata"


class GsAPIHandler:
    def __init__(self, caller_handle=None):
        self.start(caller_handle=caller_handle)

    def start(self, caller_handle=None):
        self.instance = gsapi.gsapi_new_instance(caller_handle=caller_handle)
        gsapi.gsapi_set_arg_encoding(self.instance, gsapi.GS_ARG_ENCODING_UTF8)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def stop(self):
        gsapi.gsapi_exit(self.instance)
        try:
            gsapi.gsapi_delete_instance(self.instance)
        # gsapi.GSError is weird. Some do not descend from BaseException.
        except gsapi.GSError as e:
            if "no error" not in str(e):
                raise
        finally:
            del self.instance

    def restart(self):
        self.stop()
        self.start()


def _ocr_pdf(
    instance,
    input_path: os.PathLike,
    output_path: os.PathLike,
    depth: int = 8,
    resolution: float = 600,
    downscale_factor: float = 3,
):
    params = [
        f"-sDEVICE=pdfocr{depth}",
        "-o",
        str(output_path),
        f"-r{resolution}",
        f"-dDownScaleFactor={downscale_factor}",
        "-f",
        str(input_path),
    ]
    gsapi.gsapi_add_control_path(
        instance,
        gsapi.GS_PERMIT_FILE_READING,
        str(input_path),
    )
    gsapi.gsapi_add_control_path(
        instance,
        gsapi.GS_PERMIT_FILE_WRITING,
        str(output_path),
    )
    gsapi.gsapi_init_with_args(instance, ["gs", *params])
    gsapi.gsapi_purge_control_paths(instance, gsapi.GS_PERMIT_FILE_READING)


def ocr_pdfs(params_by_path: Dict[Tuple, Dict]):
    show_progress = len(params_by_path) > 1 and sys.stderr.isatty()

    n_digits = len(str(len(params_by_path)))
    with GsAPIHandler() as handler, tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = Path(tmpdir)

        for n, ((input_path, output_path), params) in tqdm(
            enumerate(params_by_path.items()), disable=not show_progress
        ):
            tmp_path = tmpdir / f"{n:0{n_digits}d}.pdf"
            try:
                _ocr_pdf(handler.instance, input_path, tmp_path, **params)
            except gsapi.GSError as e:
                print()
                print(f"Caught {e}, restarting GhostScript")
                handler.restart()
            else:
                shutil.move(tmp_path, output_path)
            print()


if __name__ == "__main__":
    import sys

    params_by_path = {}
    Path("out").mkdir(exist_ok=True)
    for input_path in sys.argv[1:]:
        input_path = Path(input_path)
        output_path = f"out/{input_path.name.replace(' ', '_')}"
        params_by_path[(input_path, output_path)] = {}
    ocr_pdfs(params_by_path)
