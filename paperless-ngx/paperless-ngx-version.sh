#! /bin/sh
set -eu

package_name=paperless-ngx
anitya_project_id=370459
  
curl -fL "https://release-monitoring.org/api/v2/versions/?project_id=${anitya_project_id}" | \
	jq -r '.latest_version' | \
	tr -d '\r'  # something about Windows commandeering `curl`
