#! /bin/sh
set -eu
source_dir="$1"


install -D -b "${source_dir}/docker/compose/.env" \
	"${source_dir}/docker/compose/docker-compose.env" \
	.

install -b "${source_dir}/docker/compose/docker-compose.sqlite-tika.yml" \
	docker-compose.yml

read -p "Hit enter after editing config" REPLY
docker compose run --rm webserver createsuperuser
