#! /usr/bin/env python3
import argparse
import logging
import shlex
import sys
from pathlib import Path

from rich.console import Console
from rich.logging import RichHandler
from rich_argparse import RawDescriptionRichHelpFormatter

from . import gimp
from ._version import __version__

logger = logging.getLogger(__name__)


def setup_parser(parser: argparse.ArgumentParser, command):
    match command:
        case None:
            # Common
            sg = parser.add_argument_group(
                "Common to all functions",
            )
            sg.add_argument("--version", action="version", version=f"%(prog)s {__version__}")
        case "combine":
            parser.add_argument("-o", "--output", type=Path)
            parser.add_argument("backdrop_path", metavar="IMAGE", type=Path)
            parser.add_argument("layer_paths", metavar="IMAGE", nargs="+", type=Path)
        case "convert":
            parser.add_argument("--cleanup-script", type=Path)
            parser.add_argument("-t", "--file-type", choices=["jpg", "jpeg", "png", "webp"], default="webp")
            parser.add_argument("--options-scm", metavar="FILE", type=Path)
            parser.add_argument("-d", "--output-dir", type=Path)
            parser.add_argument("-v", "--verbose", action="count", default=0)
            parser.add_argument("input_paths", metavar="IMAGE", nargs="+" if sys.stdin.isatty() else "*", type=Path)
            parser.set_defaults(from_stdin=not sys.stdin.isatty())


def main():
    console = Console(stderr=True)
    logging.basicConfig(level=logging.DEBUG if __debug__ else logging.WARNING, handlers=[RichHandler(console=console)])
    parser = argparse.ArgumentParser(formatter_class=RawDescriptionRichHelpFormatter, description=__doc__)
    parser.add_argument("-v", "--verbose", action="count", default=0)
    setup_parser(parser, command=None)  # Common group
    subparsers = parser.add_subparsers(dest="command", required=True)
    setup_parser(subparsers.add_parser("combine", formatter_class=parser.formatter_class), "combine")
    setup_parser(subparsers.add_parser("convert", formatter_class=parser.formatter_class), "convert")
    args = parser.parse_args()
    logger.setLevel(logging.WARNING - 10 * args.verbose)
    match args.command:
        case "combine":
            run_combine(args)
        case "convert":
            run_convert(args, console)


def run_combine(args):
    backdrop_path = args.backdrop_path
    layer_paths = list(args.layer_paths)
    if len(args.layer_paths) == 1 and args.layer_paths[0].is_dir():
        input_dir = args.layer_paths[0]
        layer_paths = sorted(input_dir.glob("*.png"))
    output_path = args.output_path or (backdrop_path.parent / f"{backdrop_path.stem}.xcf")
    if output_path.exists:
        output_path.rename(f"{output_path}~")
    gimp.combine(layer_paths, backdrop_path=backdrop_path, scale=1.0, output_path=output_path)


def run_convert(args, console):
    batch_convert_args = []
    paths = args.input_paths
    if args.from_stdin:
        if stdin_paths := [Path(line.rstrip("\n")) for line in sys.stdin]:
            paths = stdin_paths
    for input_path in paths:
        if args.output_dir:
            output_dir = args.output_dir
        else:
            output_dir = input_path.parent
        match args.file_type:
            case "jpg" | "jpeg":
                for output_path in [
                    output_dir / f"{input_path.stem}.jpg",
                    output_dir / f"{input_path.stem}.jpeg",
                ]:
                    if output_path.exists():
                        break
            case _:
                suffix = f".{args.file_type}"
                output_path = output_dir / f"{input_path.stem}{suffix}"
        batch_convert_args.append((input_path, output_path))
        if output_path.exists():
            output_path.rename(f"{output_path}~")
    with console.status(f"Exporting {len(batch_convert_args)} image(s)"):
        results = gimp.batch_convert(
            batch_convert_args,
            options=args.options_scm.read_text() if args.options_scm else "",
        )
    cleanup_script_lines = []
    n_failures = 0
    for input_path, output_path in batch_convert_args:
        status = results.pop(output_path, "missing")
        if status == "success":
            cleanup_script_lines.append(
                f"if [ -f {shlex.quote(str(output_path))} ]; then gio trash {shlex.quote(str(input_path))}; fi"
            )
        else:
            logger.error(f"{status}: {input_path} -> {output_path}")
            n_failures += 1
    for input_path, output_path in results.items():
        logger.error(f"failed: {input_path} -> {output_path}")
        n_failures += 1
    if args.cleanup_script and cleanup_script_lines:
        logger.info(f"Writing cleanup script '{args.cleanup_script}")
        cleanup_script_lines = ["#!/bin/sh", *cleanup_script_lines]
        args.cleanup_script.write_text("\n".join(cleanup_script_lines) + "\n")
    if n_failures > 0:
        sys.exit(f"{n_failures}/{len(batch_convert_args)} conversion(s) failed")


if __name__ == "__main__":
    main()
