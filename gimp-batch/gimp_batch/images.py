"""
Image utilities
"""
import logging
import os
import tempfile
from pathlib import Path

import pyvips

from . import gimp

logger = logging.getLogger(__name__)


def load_image_info(arg, max_dimension=None):
    info = {}
    if isinstance(arg, os.PathLike):
        image_path = Path(arg)
        info["original_suffix"] = image_path.suffix
        match image_path.suffix:
            case ".xcf":
                image, info["mask"], info["layer"] = load_xcf(image_path)
            case _:
                image = pyvips.Image.new_from_file(image_path)
    elif isinstance(arg, bytes):
        image_raw = arg
        image = pyvips.Image.new_from_buffer(image_raw, options="")
    else:
        image = arg
    size = info["original_size"] = image.width, image.height
    if max_dimension:
        largest_dimension = max(size)
        if (transfer_scale := max_dimension / largest_dimension) != 1.0:
            image = image.resize(transfer_scale)
            if mask := info.get("mask"):
                info["mask"] = mask.resize(transfer_scale)
            if layer_info := info.get("layer"):
                offset = info["original_offset"] = layer_info["offset"]
                layer_info["offset"] = tuple(c * transfer_scale for c in offset)
            info["scale"] = transfer_scale
    return image, info


def load_xcf(path):
    with tempfile.TemporaryDirectory() as td:
        td = Path(td)
        extracted_path = td / "extracted.png"
        extracted_mask_path = td / "extracted-mask.png"
        layer_info = gimp.extract_mask(path, extracted_path, extracted_mask_path)
        image = pyvips.Image.new_from_file(extracted_path).copy_memory()
        if layer_info["has_mask"]:
            mask = pyvips.Image.new_from_file(extracted_mask_path).copy_memory()
        else:
            mask = None
    return image, mask, layer_info
