#! /usr/bin/env python3
"""
Single-file script for batch conversion
"""
import logging
import os
import subprocess
import tempfile
import textwrap
from collections import Counter
from pathlib import Path
from shutil import which
from typing import Iterable

logger = logging.getLogger(__name__)

if GIMP_EXE := which("gimp-console"):
    GIMP = [GIMP_EXE]
elif which("flatpak"):
    GIMP = ["flatpak", "run", "org.gimp.GIMP"]
if __debug__:
    GIMP.append("--verbose")


def _scheme_repr(value):
    if isinstance(value, (int, float)):
        return value
    elif isinstance(value, (tuple, list)):
        return f'\'( {" ".join(_scheme_repr(i) for i in value)} )'
    elif isinstance(value, Path):
        value = str(value)
    return '"{}"'.format(str(value).replace('"', '""'))


class GimpBatchFailure(RuntimeError):
    pass


def run_batch(scheme, params=None, force_quit=True):
    command = [*GIMP, "-cdi", "-b", "-"]
    scheme = textwrap.dedent(scheme)
    if params:
        quoted_kwargs = {k: _scheme_repr(v) for k, v in dict(params).items()}
        scheme = scheme.format(**quoted_kwargs)
    if force_quit is not None:
        scheme += f"\n(gimp-quit {str(force_quit).upper()})\n"
    logger.debug(f"Running {command} with {scheme}")
    proc = subprocess.run(command, text=True, input=scheme, stdout=subprocess.PIPE)
    match proc.returncode:
        case 0:
            logger.debug(proc.stdout)
        case _:
            logger.error(f"{command} exited {proc.returncode}")
            logger.error(proc.stdout)
    if proc.returncode != 0:
        raise GimpBatchFailure()


def convert(input_path, output_path):
    ((_, status),) = batch_convert([(input_path, output_path)]).items()
    match status:
        case "success":
            return
        case _:
            raise GimpBatchFailure(status)


def _clean_input_output_args(path_args):
    path_args = list(path_args)
    input_paths = {p for p, _ in path_args}
    output_path_counter = Counter(p for _, p in path_args)
    duplicate_output_paths = {p for p, f in output_path_counter.most_common() if f > 1}
    for input_path, output_path in iter(path_args):
        if output_path in input_paths:
            logger.warning(f"Refusing to overwrite '{input_path}'")
            path_args.remove((input_path, output_path))
        elif output_path in duplicate_output_paths:
            logger.warning(f"Refusing colliding operations '{input_path}' -> '{output_path}'")
            path_args.remove((input_path, output_path))
        elif not input_path or not output_path:
            logger.warning(f"Refusing invalid '{input_path}' -> '{output_path}'")
            path_args.remove((input_path, output_path))
    return path_args


def batch_convert(
    path_args: Iterable[tuple[os.PathLike, os.PathLike]], background_color=(0, 0, 0), options=""
) -> dict[Path, str]:
    if not path_args:
        return {}
    if not (path_args := _clean_input_output_args(path_args)):
        return {}
    suffixes = {Path(p).suffix.lower() for _, p in path_args}
    if suffixes <= {".jpeg", ".jpg"}:
        callback = "save-jpeg"
    elif suffixes == {".png"}:
        callback = "save-png"
    elif suffixes == {".webp"}:
        callback = "save-webp"
    else:
        raise NotImplementedError(f"File type(s) {suffixes=}")
    if not isinstance(background_color, str):
        background_color = f"'({background_color[0]} {background_color[1]} {background_color[2]})"
    with tempfile.TemporaryDirectory() as td:
        td = Path(td)
        transfer_file = td / "gimp.tab"
        run_batch(
            scheme=rf"""
                (define options {options or "#f"})
                
                (define (export-batch-convert plist filename)
                  (let ((out (open-output-file filename)))
                    (for-each (lambda (item)
                                (display (car item) out)
                                (write-char #\tab out)
                                (display (if (cdr item) "success" "failure") out)
                                (newline out))
                              plist)
                    (close-output-port out))
                  )

                (export-batch-convert
                  (batch-convert
                    '( {" ".join(_scheme_repr(p) for p, _ in path_args)} )
                    '( {" ".join(_scheme_repr(p) for _, p in path_args)} )
                    {callback}
                    {background_color}
                    options
                    )
                  {_scheme_repr(transfer_file)}
                  )
"""
        )
        results = {}
        with open(transfer_file) as fi:
            for line in fi:
                path, _, status = line.rstrip().partition("\t")
                results[Path(path)] = status
    return results


def extract_mask(input_path, output_path, mask_path):
    ((_, info),) = batch_extract_masks([(input_path, output_path, mask_path)]).items()
    return info


def batch_extract_masks(path_args: Iterable[tuple[os.PathLike, os.PathLike, os.PathLike]]):
    if not path_args:
        return
    with tempfile.TemporaryDirectory() as td:
        td = Path(td)
        transfer_file = td / "gimp.tab"
        run_batch(
            scheme=rf"""
                (define (export-batch-extract-masks plist filename)
                  (let (
                         (out (open-output-file filename))
                         )
                    (for-each (lambda (item)
                                (display (car item) out)
                                (write-char #\tab out)
                                (display (cdr (assoc 'has-mask (cdr item))) out)
                                (write-char #\tab out)
                                (display (cdr (assoc 'offset-x (cdr item))) out)
                                (write-char #\tab out)
                                (display (cdr (assoc 'offset-y (cdr item))) out)
                                (newline out))
                      plist)
                    (close-output-port out))
                  )

                (export-batch-extract-masks
                  (batch-extract-masks
                    '( {" ".join(_scheme_repr(p) for p, _, _ in path_args)} )
                    '( {" ".join(_scheme_repr(p) for _, p, _ in path_args)} )
                    '( {" ".join(_scheme_repr(p) for _, _, p in path_args)} )
                    )
                  {_scheme_repr(transfer_file)}
                  )
"""
        )
        results = {}
        with open(transfer_file) as fi:
            for line in fi:
                image_path, has_mask, offset_x, offset_y = line.rstrip().split("\t")
                results[Path(image_path)] = dict(
                    has_mask={"#t": True, "#f": False}.get(has_mask), offset=(int(offset_x), int(offset_y))
                )
    return results


class CombineLayer(dict):
    def to_plist(self):
        return (
            f"'( (path . {_scheme_repr(self['path'])}) "
            f"(offset-x . {_scheme_repr(self.get('offset-x') or 0)}) "
            f"(offset-y . {_scheme_repr(self.get('offset-y') or 0)}) )"
        )


def combine(
    layers: Iterable[os.PathLike | CombineLayer], backdrop_path: os.PathLike, scale: float, output_path: os.PathLike
):
    layers = [CombineLayer({"path": layer} if isinstance(layer, os.PathLike) else layer) for layer in layers]
    backdrop_path = Path(backdrop_path)
    output_path = Path(output_path)
    if output_path.exists():
        output_path.rename(f"{output_path}~")
    if layers:
        run_batch(
            scheme=f"""\
                (create-layered-image-from-upscaled
                  (list
                      {" ".join(layer.to_plist() for layer in layers)}
                     )
                  {{backdrop_path}}
                  {{scale}}
                  {{output_path}}
                  )
                (if (not (file-exists? {{output_path}})) (throw 'programming-error "create-layered-image-from-upscaled failed") )
                """,
            params=dict(backdrop_path=backdrop_path, scale=float(scale), output_path=output_path),
        )
    return output_path.exists()


def mask(image_path, mask_path, output_path):
    image_path = Path(image_path)
    mask_path = Path(mask_path)
    output_path = Path(output_path)
    run_batch(
        scheme="""\
            (define input-image (car (gimp-file-load RUN-NONINTERACTIVE {image_path} {image_path})))
            (define top-layer (car (gimp-image-get-active-layer input-image)))
            (import-mask input-image top-layer {mask_path})
            ;; for some reason, I see "Error: syntax error: illegal token 1"
            (gimp-file-save RUN-NONINTERACTIVE
              input-image top-layer
              {output_path} {output_path})
              )
            (if (not (file-exists? {{output_path}})) (throw 'programming-error "import-mask failed") )
            """,
        params=dict(image_path=image_path, mask_path=mask_path, output_path=output_path)
    )
    assert output_path.exists()
