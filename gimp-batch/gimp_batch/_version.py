from importlib.metadata import version

__version__ = version("gimp_batch")
