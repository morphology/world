import pytest

from pathlib import Path

TEST_INPUTS = Path("tests/test-inputs")
KNOWN_GOOD_MASKED_XCF = sorted(TEST_INPUTS.glob("masked/*"))
KNOWN_BAD_XCF = sorted(TEST_INPUTS.glob("bad/*"))
