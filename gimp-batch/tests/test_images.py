import pytest

from .conftest import KNOWN_GOOD_MASKED_XCF

from gimp_batch import images


def test_load_xcf(tmp_path):
    """
    Brittle sanity check
    """
    input_path = KNOWN_GOOD_MASKED_XCF[0]
    image, mask, info = images.load_xcf(input_path)
    assert image and mask
