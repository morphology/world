import logging

import pytest

from gimp_batch import gimp

from .conftest import KNOWN_GOOD_MASKED_XCF, KNOWN_BAD_XCF, TEST_INPUTS

supported_formats = pytest.mark.parametrize("output_format", [".jpeg", ".png", ".webp"])


def test_combine(tmp_path, caplog):
    backdrop_path = TEST_INPUTS / "combine/backdrop.jpeg"
    layer1_path = TEST_INPUTS / "combine/layer-1.jpg"
    output_path = tmp_path / "combined.xcf"
    with caplog.at_level(logging.DEBUG):
        gimp.combine([layer1_path], backdrop_path, 1.0, output_path)
    assert output_path.exists()


@supported_formats
def test_convert_scm(output_format, tmp_path, caplog):
    """
    Brittle sanity check
    """
    input_path = KNOWN_GOOD_MASKED_XCF[0]
    output_path = tmp_path / f"output{output_format}"
    scheme = f"""\
        (file-exists? "{input_path}")
        (batch-convert
          '("{input_path}")
          '("{output_path}")
          save-{output_format.lstrip('.')}
          #f ; background color
          #f ; options
          )
        (file-exists? "{output_path}")
    """
    with caplog.at_level(logging.DEBUG):
        gimp.run_batch(scheme=scheme, force_quit=True)
    for record in caplog.records:
        lines = record.msg.splitlines()
        if f'(("{output_path}" . #t))' in lines:
            break
    else:
        pytest.fail("Expected return value not found in log")
    assert output_path.exists()


@supported_formats
def test_convert_good(output_format, tmp_path):
    input_path = KNOWN_GOOD_MASKED_XCF[0]
    output_path = tmp_path / f"output{output_format}"
    gimp.convert(input_path, output_path)
    assert output_path.exists()


@supported_formats
def test_convert_bad(output_format, tmp_path):
    input_path = KNOWN_BAD_XCF[0]
    output_path = tmp_path / f"output{output_format}"
    with pytest.raises(Exception):
        gimp.convert(input_path, output_path)


def test_extract_mask_good(tmp_path):
    input_path = KNOWN_GOOD_MASKED_XCF[0]
    output_path = tmp_path / "output.png"
    mask_path = tmp_path / "mask.png"
    info = gimp.extract_mask(input_path, output_path, mask_path)
    assert output_path.exists()
    assert info["has_mask"]


def test_extract_mask_bad(tmp_path):
    input_path = KNOWN_BAD_XCF[0]
    output_path = tmp_path / "output.png"
    mask_path = tmp_path / "mask.png"
    with pytest.raises(Exception):
        gimp.extract_mask(input_path, output_path, mask_path)


def test_import_mask_scm(tmp_path, caplog):
    input_path = TEST_INPUTS / "separate/image.png"
    mask_path = TEST_INPUTS / "separate/mask.png"
    output_path = tmp_path / "output.xcf"
    with caplog.at_level(logging.DEBUG):
        gimp.run_batch(
            scheme=f"""\
            (file-exists? "{input_path}")
            (file-exists? "{mask_path}")
            (define input-image (car (gimp-file-load RUN-NONINTERACTIVE "{input_path}" "{input_path}")))
            (define top-layer (car (gimp-image-get-active-layer input-image)))
            (import-mask
              input-image
              top-layer
              "{mask_path}"
              )
            (gimp-file-save RUN-NONINTERACTIVE
              input-image top-layer
              "{output_path}" "{output_path}")
              )
            (file-exists? "{output_path}")
    """,
            force_quit=True,
        )
    for record in caplog.records:
        lines = record.msg.splitlines()
        for line in lines:
            print(line)
    assert output_path.exists()


def test_import_mask(tmp_path, caplog):
    output_path = tmp_path / "output.xcf"
    with caplog.at_level(logging.DEBUG):
        gimp.mask(
            image_path=TEST_INPUTS / "separate/image.png",
            mask_path=TEST_INPUTS / "separate/mask.png",
            output_path=output_path,
        )
    for record in caplog.records:
        lines = record.msg.splitlines()
        for line in lines:
            print(line)
    assert output_path.exists()
