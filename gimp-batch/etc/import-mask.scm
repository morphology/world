(define (import-mask image layer mask-file-path)
  (let* (
          (mask-image (car (gimp-file-load RUN-NONINTERACTIVE mask-file-path mask-file-path)))
          (mask-layer (car (gimp-image-get-active-layer mask-image)))
          )
    (unless (gimp-drawable-is-gray mask-layer) (gimp-image-convert-grayscale mask-image))

    (let (
           (layer-mask (car (gimp-layer-create-mask layer ADD-WHITE-MASK)))
           )
      (gimp-layer-add-mask layer layer-mask)
      (gimp-edit-named-copy mask-layer "mask")
      (gimp-floating-sel-anchor (car (gimp-edit-named-paste layer-mask "mask" TRUE)))
      )
    (if (equal? (gimp-layer-get-mask layer) -1) (throw 'programming-error "Failed to create mask"))
    (gimp-image-delete mask-image)
    )
  (gimp-displays-flush)
  )
