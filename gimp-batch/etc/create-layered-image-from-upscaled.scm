(define (create-layered-image-from-upscaled layer-plists backdrop-path scale output-path)
  (let* (
          (backdrop-image (car (gimp-file-load RUN-NONINTERACTIVE backdrop-path backdrop-path)))
          (backdrop-layer (car (gimp-image-get-active-layer backdrop-image)))
          (original-width (car (gimp-image-width backdrop-image)))
          (original-height (car (gimp-image-height backdrop-image)))
          )
    (begin
      (unless (equal? scale 1.0)
        (gimp-image-scale backdrop-image (* original-width scale) (* original-height scale))
        )
      (define results (map (lambda (plist) (
                                             let* (
                                                    (layer-path (cdr (assoc 'path plist)))
                                                    (offset-x (cdr (assoc 'offset-x plist)))
                                                    (offset-y (cdr (assoc 'offset-y plist)))
                                                    (new-layer (car (gimp-file-load-layer RUN-NONINTERACTIVE backdrop-image layer-path)))
                                                    )
                                             (begin
                                               (gimp-layer-set-offsets new-layer offset-x offset-y)
                                               (gimp-image-insert-layer backdrop-image new-layer 0 0)
                                               (cons layer-path #t)
                                               )
                                             )
                             ) layer-plists
                        ))
      (gimp-image-resize-to-layers backdrop-image)
      (gimp-file-save RUN-NONINTERACTIVE
        backdrop-image backdrop-layer
        output-path output-path)
      )
    (gimp-image-delete backdrop-image)
    results
    )
  )
