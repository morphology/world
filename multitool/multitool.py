#! /usr/bin/env python3
"""
Helpful text
"""
import argparse
import logging
import warnings

from rich.console import Console
from rich.logging import RichHandler
from rich_argparse import RawDescriptionRichHelpFormatter

import dezgo.cli
import gimp_batch.cli
import ocr.cli
import rembg.cli

logging.getLogger("httpcore").setLevel(logging.ERROR)
logging.getLogger("pyvips").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)


console = Console(stderr=True)
logging.basicConfig(level=logging.DEBUG if __debug__ else logging.WARNING, handlers=[RichHandler(console=console)])
warnings.simplefilter("ignore", ResourceWarning)
parser = argparse.ArgumentParser(formatter_class=RawDescriptionRichHelpFormatter, description=__doc__)
parser.add_argument("-v", "--verbose", action="count", default=0)

subparsers = parser.add_subparsers(dest="command", required=True)
dezgo.cli.setup_parser(subparsers.add_parser("controlnet", formatter_class=parser.formatter_class), "controlnet")
dezgo.cli.setup_parser(subparsers.add_parser("image2image", formatter_class=parser.formatter_class), "image2image")
dezgo.cli.setup_parser(subparsers.add_parser("inpaint", formatter_class=parser.formatter_class), "inpaint")
dezgo.cli.setup_parser(subparsers.add_parser("text2image", formatter_class=parser.formatter_class), "text2image")
gimp_batch.cli.setup_parser(subparsers.add_parser("convert-gimp", formatter_class=parser.formatter_class), "convert")
ocr.cli.setup_parser(subparsers.add_parser("image2text", formatter_class=parser.formatter_class), "detect")
rembg.cli.setup_parser(
    subparsers.add_parser("remove-background", formatter_class=parser.formatter_class), "remove-background"
)
args = parser.parse_args()
logger.setLevel(logging.WARNING - 10 * args.verbose)
match args.command:
    case "controlnet" | "image2image" | "inpaint" | "text2image":
        dezgo.cli.run_interactive(args.command, args, console)
    case "gimp-convert":
        gimp_batch.cli.convert(args)
    case "image2text":
        ocr.cli.run_detect(args, console)
    case "remove-background":
        rembg.cli.run_remove_background(args, console)
