#! /usr/bin/env python3
import decimal
import json
import os
import re
import subprocess
import tempfile
from abc import ABC
from dataclasses import dataclass
from fractions import Fraction
from pathlib import Path
from typing import Iterable, Literal, Optional, TypedDict

import anyio
from loguru import logger

__all__ = ["FFMPEG", "VideoMetadata", "VideoError", "get_video_info"]

DEFAULT_TILE = (6, 5)
FFMPEG = (
    "ffmpeg",
    "-hide_banner",
    "-loglevel", "debug" if __debug__ else "warning",
)  # fmt: skip
FFPROBE = (
    "ffprobe",
    "-hide_banner",
    "-loglevel", "debug" if __debug__ else "warning",
)  # fmt: skip


@dataclass(frozen=True, order=True)
class FrameInfo:
    """
    Flat ffprobe frame record.

    I intend only to ingest this format.
    """

    timestamp: decimal.Decimal
    pkt_pos: Optional[int]  # position in file


class FormatMetadata(TypedDict, total=False):
    """
    Structured record for ffprobe format section.
    """

    duration: str
    format_name: str
    size: str


class StreamMetadata(TypedDict, total=False):
    """
    Structured record for each ffprobe stream section.
    """

    index: int
    codec_type: Literal["video", "audio", "subtitle"]


class VideoError(Exception):
    pass


class VideoMetadata:
    def __init__(self):
        self.format: Optional[FormatMetadata] = None
        self.audio_streams: list[StreamMetadata] = []
        self.video_streams: list[StreamMetadata] = []
        self.chapters: Optional[Iterable] = None
        self.key_frames: list[FrameInfo] = []

    @classmethod
    def from_ffprobe(cls, text):
        """
        Constructor when the input is text.

        Make sure to coordinate with .to_text() below.
        """
        result = cls()
        for section, content in json.loads(text).items():
            match section:
                case "format":
                    result.format = FormatMetadata(**content)
                case "streams":
                    for stream in content:
                        stream = StreamMetadata(**stream)
                        match stream["codec_type"]:
                            case "audio":
                                result.audio_streams.append(stream)
                            case "video":
                                result.video_streams.append(stream)
                            case _:
                                logger.warning(f"Skipping stream {stream}")
                #
                # Not expecting to keep frames section intact
                #
                case "frames":
                    # The input might be skipping frames, so this is not guaranteed to be the
                    # actual frame number
                    for n, frame in enumerate(content):
                        if frame.get("key_frame") == 1:
                            if "best_effort_timestamp_time" in frame:
                                timestamp = decimal.Decimal(frame.pop("best_effort_timestamp_time"))
                            else:
                                #
                                # This seems pretty rare.
                                #
                                logger.warning(f"Skipping key frame {n}: {frame} without timestamp.")
                                continue
                            if "pkt_pos" in frame:
                                pkt_pos = int(frame.pop("pkt_pos"))
                            else:
                                logger.info(f"key frame {n}: {frame} without file position.")
                                pkt_pos = None
                            result.key_frames.append(FrameInfo(timestamp=timestamp, pkt_pos=pkt_pos))

                case "chapters":
                    if len(content) > 1:
                        logger.warning(f"Skipping {section}: {content}")
                    elif content:
                        logger.info(f"Skipping {section}: {content}")
                case _:
                    if content:
                        logger.warning(f"Skipping {section}: {content}")
        return result

    @property
    def streams(self):
        return sorted(self.audio_streams + self.video_streams, key=lambda s: s["index"])

    def to_text(self) -> str:
        ffprobe = {
            "format": self.format,
            "streams": self.streams,
        }
        if self.chapters:
            ffprobe["chapters"] = self.chapters
        return json.dumps(
            ffprobe,
            indent=2,
        )

    def get_primary_video_stream(self):
        for stream in self.video_streams:
            if stream["disposition"].get("default") == 1:
                return stream
        logger.warning("No default video stream indicated")

        def key_func(s):
            n_pixels = s["width"] * s["height"]
            bit_rate = float(s.get("bit_rate", 0))
            return bit_rate, n_pixels, -int(s["index"])

        return max(self.video_streams, key=key_func)

    def get_fps(self) -> Optional[Fraction]:
        s = self.get_primary_video_stream()
        if fps_s := s.get("r_frame_rate"):
            return Fraction(fps_s)

    def get_resolution(self) -> tuple[int, int]:
        s = self.get_primary_video_stream()
        width, height = int(s["width"]), int(s["height"])
        match self.format["format_name"]:
            case "mov,mp4,m4a,3gp,3g2,mj2":
                if (width % 2 != 0) or (height % 2 != 0):
                    # This appears to be common with Quicktime.
                    logger.warning(
                        "Be sure to adjust height and width to be multiples of 2. "
                        'Use ["-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2"] .'
                    )
        return width, height

    def get_duration(self) -> Optional[decimal.Decimal]:
        return decimal.Decimal(self.format["duration"])

    def get_bit_rate(self) -> Optional[float]:
        return float(self.format["bit_rate"]) / 1e6  # Mbps

    def get_size(self) -> Optional[int]:
        return int(self.format["size"])

    def __repr__(self):
        width, height = self.get_resolution()
        bit_rate = self.get_bit_rate()
        n_streams = len(self.video_streams) + len(self.audio_streams)
        for stream in self.video_streams:
            if "nb_frames" in stream:
                n_frames = int(stream["nb_frames"])
                break
        else:
            n_frames = None
        is_image = self.format["format_name"] == "image2"
        if is_image:
            if bit_rate is None or n_frames is None or n_frames == 1:
                blurb = f"{width}x{height} {self.format['format_name']} image"
        else:
            if duration := self.get_duration():
                duration = f"{duration:.2f} s"
                if bit_rate:
                    bit_rate = f"{bit_rate:.1f} Mbps"
            blurb = (
                f"{width}x{height} {bit_rate=} {self.format['format_name']} video "
                f"{duration=} {n_frames=} {n_streams=} "
            )
            for stream in self.video_streams:
                if codec_name := stream.get("codec_name"):
                    blurb += f"{codec_name} "
            for stream in self.audio_streams:
                if codec_name := stream.get("codec_name"):
                    blurb += f"{codec_name} "
        return f"<VideoMetadata {blurb.strip()}>"

    def get_path(self) -> os.PathLike:
        return Path(self.format["filename"]).resolve()


async def get_video_info(
    path, show_frames: Optional[Literal["all", "key", "none"]] = None, metadata_class=VideoMetadata
) -> Optional[VideoMetadata]:
    """
    show_frames:
        all: slowest, if you need every timestamp for every frame
        key: faster, provides accurate timestamps but not frame numbers!
        none: fastest, crudest metadata only
    """
    entries = [
        "format",
        "streams",
        "chapters",
    ]
    match show_frames:
        case "all" | "key":
            frame_fields = ("key_frame", "pkt_pos", "best_effort_timestamp_time")
            entries.append("frame=" + ",".join(frame_fields) if frame_fields else "frame")
        case "none" | None:
            frame_fields = None
    ffprobe_args = ["-show_entries", ":".join(entries)]
    match show_frames:
        case "key":
            if "coded_picture_number" not in frame_fields:
                ffprobe_args += ["-skip_frame", "nokey"]
                logger.debug("Frame numbers refer to counting key frames only!")
    command = [
        *FFPROBE,
        *ffprobe_args,
        "-print_format", "json",
        path,
    ]  # fmt: skip
    logger.debug(f"Running {command}")
    proc = await anyio.run_process(command, stdout=subprocess.PIPE)
    return metadata_class.from_ffprobe(proc.stdout.decode())


class screencap_tiles:
    tile_dimensions = DEFAULT_TILE

    def __init__(
        self,
        video_info: VideoMetadata,
        image_pattern: str,
        duration=None,
    ):
        if Path(image_pattern).exists():
            image_pattern = Path(image_pattern)
        if isinstance(image_pattern, Path):
            image_pattern = f"{image_pattern}/" if image_pattern.is_dir() else str(image_pattern)
        self.image_pattern = image_pattern
        self.filters = self._tile_filters(video_info=video_info, duration=duration)

    @classmethod
    def _tile_filters(cls, video_info, interval=None, duration=None):
        filters = []
        if interval is None:
            tile_x, tile_y = cls.tile_dimensions
            n_tiles = tile_x * tile_y
            duration = duration or video_info.get_duration()
            if duration:
                interval = duration / (n_tiles + 1)
        if float(interval) * video_info.get_fps() > 1:
            #
            # Without this select=, all frames are output
            #
            filters.append(rf"select=isnan(prev_selected_t)+gte(t-prev_selected_t\,{interval})")

        if video_info:
            width, height = video_info.get_resolution()
            if min(height, width) > 256:
                filters.append("scale=w=-1:h=256")
        if any(cls.tile_dimensions):
            tile_x, tile_y = cls.tile_dimensions
            filters.append(f"tile={tile_x}x{tile_y}")
        else:
            filters.append("tile")
        return filters

    async def __aenter__(self):
        self.ffmpeg_input_args = ["-an", "-sn", "-dn"]
        self.ffmpeg_output_args = [
            "-filter:v", ",".join(self.filters),
            "-frames:v", "1",                     # see also vsync
            "-f", "image2",
            self.image_pattern,
        ]  # fmt: skip
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if Path(self.image_pattern).exists():
            logger.debug(f"Created {self.image_pattern}")
        else:
            logger.debug(f"Check for matches to '{self.image_pattern}'")


@dataclass(order=True, frozen=True)
class MetadataPoint:
    number: int
    pts: int
    pts_time: decimal.Decimal

    @classmethod
    def match(cls, text: str):
        begin_re = re.compile(r"(frame:(?P<number>\d+)\s+pts:(?P<pts>\d+)\s+pts_time:(?P<pts_time>[0-9.]+))")
        if m := begin_re.match(text.strip()):
            return cls(
                number=int(m.group("number")),
                pts=int(m.group("pts")),
                pts_time=decimal.Decimal(m.group("pts_time")),
            )

    def __str__(self):
        return f"frame:{self.number} pts:{self.pts} pts_time:{self.pts_time}"


class MetadataFilterBase(ABC):
    def __init__(
        self,
        filters: Optional[Iterable] = None,
        video_info: Optional[VideoMetadata] = None,
        workdir=None,
    ):
        self.workdir = anyio.Path(workdir or tempfile.mkdtemp())
        self.filters = list(filters or ())
        self.results = {}
        self.video_info = video_info

    async def __aenter__(self):
        self.ffmpeg_input_args = []
        self.ffmpeg_output_args = []
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if paths := list(self.workdir.glob("*")):
            logger.warning(f"Ignoring {len(paths)} file(s) produced: {paths}")


def parse_ffmpeg_txt(text) -> Iterable[tuple[MetadataPoint, dict]]:
    entries = []

    point, attr = None, {}
    for line in text.splitlines():
        if new_point := MetadataPoint.match(line):
            if point is not None:
                entries.append((point, attr))
            elif attr:
                logger.info(f"Ignoring {attr}")
            point, attr = new_point, {}
        elif line.strip():
            key, _, value = line.partition("=")
            attr[key] = decimal.Decimal(value)
    if point is not None:
        entries.append((point, attr))
    return entries


class transition_detector(MetadataFilterBase):
    async def __aenter__(self):
        self.filters += [
            "blackdetect",
            "blurdetect",
            "freezedetect",
            "scdet",
            f"metadata=print:file={self.workdir / 'transition_frames.txt'}",
        ]
        self.ffmpeg_input_args = ["-an", "-sn", "-dn"]
        self.ffmpeg_output_args = [
            "-filter:v", ",".join(self.filters),
            "-f", "null",
            "-"]  # fmt: skip
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self.results = {"metadata_text": await anyio.Path(self.workdir / "transition_frames.txt").read_text()}


def args_to_select_segment(segment_start, segment_end) -> list[str]:
    ffmpeg_args = []
    if segment_start is not None:
        ffmpeg_args.extend(["-ss", str(segment_start)])
    if segment_end is not None:
        ffmpeg_args.extend(["-to", str(segment_end)])
    return ffmpeg_args
