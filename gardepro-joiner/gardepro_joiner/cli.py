"""
Join motion-capture wildlife videos.

This assumes videos follow some GardePro format.
"""

import argparse
import asyncio
import datetime
import itertools
import os
import sys
import tempfile
from pathlib import Path
from typing import Iterable

import loguru
import rich.console
import rich.progress
import rich_argparse

import gardepro_joiner
import gardepro_joiner.formats
import gardepro_joiner.gardepro

logger = loguru.logger


def get_day_sort_order(metadata: gardepro_joiner.gardepro.GardeProMetadata) -> tuple:
    _, info = metadata.parse_GardePro_comment()
    is_daylight = info.get("IRLED") == 0.0
    begins, ends = metadata.get_timespan()
    if is_daylight:
        return (begins.date(), is_daylight)
    else:
        last_full_day = (ends - datetime.timedelta(hours=8)).date()
        return (last_full_day, is_daylight)


async def group_videos(paths, progress_handler=None):
    metadata_task = progress_handler.add_task("Analyzing video(s)", total=len(paths)) if progress_handler else None
    task_by_path = {}
    async with asyncio.TaskGroup() as tg:
        open_file_limiter = asyncio.Semaphore(4)

        async def get_info(path):
            async with open_file_limiter:
                try:
                    info = await gardepro_joiner.gardepro.get_video_info(path)
                except Exception as e:
                    logger.error(f"Skipping {path}: {e}")
                    return None
            if metadata_task is not None:
                progress_handler.update(metadata_task, advance=1)
            return info

        for path in paths:
            task_by_path[path] = tg.create_task(get_info(path))
    assert task_by_path
    if metadata_task is not None:
        progress_handler.remove_task(metadata_task)
    results = []
    for path, task in task_by_path.items():
        if info := task.result():
            try:
                info.parse_GardePro_comment()
            except gardepro_joiner.gardepro.IncompleteFile as e:
                logger.debug(f"Skipping {path}: {e}")
                continue
            results.append(info)
    assert results
    results.sort(key=lambda info: info.get_timespan())
    mtimes = [datetime.datetime.fromtimestamp(info.get_path().stat().st_mtime) for info in results]
    for i in range(1, len(results)):
        if mtimes[i] < mtimes[i - 1]:
            logger.warning(f"{results[i]} appears copied out-of-order")
    groups = []
    for grouping, results in itertools.groupby(results, key=get_day_sort_order):
        groups.append((grouping, list(results)))
    return groups


async def join_videos(input_args, output_path, overwrite=None):
    output_path = Path(output_path)
    ffconcat_path = output_path.with_suffix(".ffconcat")
    if overwrite is None:
        assert not output_path.exists(), f"Refusing to choose to overwrite {output_path}"

    await gardepro_joiner.formats.write_ffconcat(ffconcat_path, input_args)
    command = [
        "ffmpeg",
        "-nostdin",
        "-f", "concat",
        "-safe", "0",  # Allow absolute paths
        "-i", str(ffconcat_path),
        "-c", "copy",
        "-y" if overwrite else "-n",
        str(output_path),
    ]  # fmt: skip
    with tempfile.NamedTemporaryFile(prefix="ffmpeg-", suffix=".log", delete=not __debug__) as logfile:
        logger.debug(f"Running {command}, see {logfile.name}")
        return await asyncio.create_subprocess_exec(
            *command,
            stdin=asyncio.subprocess.DEVNULL,
            stderr=logfile,
        )


def _setup_parser(parser: argparse.ArgumentParser):
    parser.add_argument(
        "--output-pattern", "-o",
        type=str,
        # Escape printf tokens:
        help="Use a path and format like `foo/bar_{date:%%Y-%%m-%%d}-{daylight}.mkv`",
    )  # fmt: skip
    parser.add_argument("--verbose", "-v", action=argparse.BooleanOptionalAction)
    parser.add_argument("--version", action="version", version=gardepro_joiner.__version__)
    parser.add_argument("paths", type=Path, nargs="+" if sys.stdin.isatty() else "*")
    parser.set_defaults(output_pattern="combined_{date:%Y-%m-%d}-{daylight}.mkv")
    return parser


async def combine_days(paths: Iterable[os.PathLike], output_pattern: str | os.PathLike, progress_handler=None) -> bool:
    output_pattern = str(output_pattern)
    groups = list(await group_videos(paths, progress_handler=progress_handler))
    if not groups:
        return False

    total_size = 0
    for (date, is_daylight), infos in groups:
        for info in infos:
            total_size += info.get_size()
    logger.info(f"Expected average video size: {total_size/1e6/len(groups):,.1f} MB")
    if progress_handler:
        join_task = progress_handler.add_task("Joining videos", total=total_size)
    else:
        join_task = None

    n_failures = 0
    for (date, is_daylight), infos in groups:
        combined_path = Path(output_pattern.format(date=date, daylight="day" if is_daylight else "night"))
        group_size = sum(info.get_size() for info in infos)
        proc = await join_videos(infos, combined_path, overwrite=True)
        await proc.communicate()
        if join_task is not None:
            progress_handler.update(join_task, advance=group_size)
        if proc.returncode != 0:
            logger.error(f"{proc} returned non-zero exit status {proc.returncode}")
            n_failures += 1
    if join_task is not None:
        progress_handler.remove_task(join_task)
    if n_failures > 0:
        logger.warning(f"Skipped {n_failures} video(s)")
    return n_failures == 0


def main():
    arguments = _setup_parser(
        argparse.ArgumentParser(description=__doc__, formatter_class=rich_argparse.RawDescriptionRichHelpFormatter)
    ).parse_args()
    loguru.logger.enable("gardepro_joiner")
    loguru.logger.remove()  # Remove all handlers added so far, including the default one.
    loguru.logger.add(
        sys.stderr,
        format="{message}",
        colorize=True,
        level="DEBUG" if arguments.verbose else "WARNING",
        diagnose=False,  # Appears to leak sensitive memory contents
    )
    paths = arguments.paths or [Path(p.rstrip("\n")) for p in sys.stdin if p.strip()]
    with (
        progress_handler := rich.progress.Progress(
            console=rich.console.Console(quiet=arguments.verbose is False, stderr=True),
            disable=not sys.stderr.isatty(),
            expand=True,
            transient=not __debug__,
        )
    ):
        return asyncio.run(
            combine_days(paths=paths, output_pattern=arguments.output_pattern, progress_handler=progress_handler)
        )
