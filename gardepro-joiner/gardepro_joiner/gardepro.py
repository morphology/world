import datetime
import re

import loguru

from gardepro_joiner import ffmpeg

logger = loguru.logger


class IncompleteFile(Exception):
    pass


class GardeProMetadata(ffmpeg.VideoMetadata):
    def get_timespan(self) -> tuple[datetime.datetime, datetime.datetime]:
        if time_str := self.format["tags"].get("creation_time"):
            begins = datetime.datetime.strptime(time_str, r"%Y-%m-%dT%H:%M:%S.%fZ").replace(
                tzinfo=datetime.timezone.utc
            )
            return (begins, begins + datetime.timedelta(seconds=float(self.get_duration())))

    def parse_GardePro_comment(self):
        video_path = self.get_path()
        whole_comment = self.format["tags"]["comment"]
        line1, line2, *csv_lines = whole_comment.splitlines()
        tokens = re.split(
            r"(trigerType|mode|PV|burst|videoLen|videoLenNight|nightOverExp|dayOverExp|detectionDelay|pirSense|sidePir|timelapseInterval|loopRecording|operationHour|tempture)[:]",
            f"{line1}\n{line2}",
        )
        line1 = tokens.pop(0).strip()
        info = [(key.strip(), value.strip()) for key, value in zip(tokens[0::2], tokens[1::2])]

        tokens = []
        v_array = []
        for text in ("\n".join(csv_lines)).split(","):
            key, sep, value = text.strip().partition(":")
            if not sep:
                logger.log("DEBUG" if key == "end low_vol" else "ERROR", f"{video_path}: Ignoring {key=}")
                continue
            match key := key.strip():
                case "IRLED":
                    info.append((key, float(value)))
                case "v":
                    v_array.append(value.strip())
                case _:
                    info.append((key, value.strip()))

        logger.debug(f"{video_path}: {whole_comment}->{line1=} {info=} {v_array=}")

        if not v_array:
            # Apparently, some very short videos might result from low battery?
            raise IncompleteFile("Incomplete file")
        return line1, dict(info)


async def get_video_info(path):
    return await ffmpeg.get_video_info(path=path, metadata_class=GardeProMetadata)
