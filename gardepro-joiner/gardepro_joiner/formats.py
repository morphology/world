import os
import shlex
from pathlib import Path
from typing import Iterable

import anyio

from gardepro_joiner import ffmpeg
from gardepro_joiner import gardepro


async def write_ffconcat(
    ffconcat_path: os.PathLike, entries: Iterable[gardepro.GardeProMetadata | os.PathLike]
) -> None:
    lines = ["ffconcat version 1.0", ""]
    for entry in entries:
        if isinstance(entry, gardepro.GardeProMetadata):
            begins, ends = entry.get_timespan()
            lines.append(f"# {begins.isoformat()} - {ends.isoformat()} ({entry.get_duration():0.2f} s)")
            path = entry.get_path()
        elif isinstance(entry, ffmpeg.VideoMetadata):
            path = entry.get_path()
        elif isinstance(entry, os.PathLike):
            path = Path(entry)
        else:
            raise ValueError(entry)
        lines.append(f"file {shlex.quote(str(path))}")
    await anyio.Path(ffconcat_path).write_text("\n".join(lines))
