import sys
from pathlib import Path

import loguru
import pytest

import gardepro_joiner.cli

loguru.logger.enable("gardepro_joiner")
BACKENDS = ("asyncio",)

TEST_INPUT_DIR = (Path(__file__).parent / "test-inputs").resolve()
DAY_TEST_INPUTS = [str(p) for p in sorted((TEST_INPUT_DIR / "day").rglob("*.MP4"))]
NIGHT_TEST_INPUTS = [str(p) for p in sorted((TEST_INPUT_DIR / "night").rglob("*.MP4"))]
BAD_TEST_INPUTS = [str(p) for p in sorted((TEST_INPUT_DIR / "broken").rglob("*.MP4"))]


@pytest.mark.parametrize("anyio_backend", BACKENDS)
async def test_combine_days(anyio_backend, monkeypatch, tmp_path):
    """
    Should complete over all test inputs.
    """
    success = await gardepro_joiner.cli.combine_days(
        paths=[Path(p) for p in TEST_INPUT_DIR.rglob("*.MP4")],
        output_pattern=tmp_path / "combined_{date:%Y-%m-%d}-{daylight}.mkv",
    )
    videos_made = sorted(tmp_path.glob("*.mkv"))
    assert len(videos_made) > 1
