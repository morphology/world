import http.client
import json
import os
import urllib.parse
from contextlib import closing
from datetime import datetime

import rich.repr

api_key = os.getenv("BIZTOC_API_KEY") or os.getenv("RAPIDAPI_KEY")


class BadStatus(Exception):
    pass


@rich.repr.auto
class BiztocClient:
    def __init__(self):
        global api_key
        assert api_key, "Environment variable not set"
        self.conn = http.client.HTTPSConnection("biztoc.p.rapidapi.com")
        self.headers = {"x-rapidapi-key": api_key, "x-rapidapi-host": "biztoc.p.rapidapi.com"}

    def close(self):
        self.conn.close()

    @classmethod
    def _parse_story(cls, story_info):
        if "published" in story_info:
            for strp_format in ["%a, %d %b %Y %H:%M:%S %Z", "%Y-%m-%d %H:%M:%S.%f"]:
                try:
                    story_info["published"] = datetime.strptime(story_info["published"], strp_format)
                    break
                except:
                    pass
        if "topic" in story_info:
            story_info["topic"] = tuple(story_info["topic"])
        return story_info

    def search(self, query):
        self.conn.request("GET", "/search?q={}".format(urllib.parse.quote(query)), headers=self.headers)
        response = self.conn.getresponse()
        match http.client.responses[response.status]:
            case "OK":
                return [self._parse_story(story_info) for story_info in json.load(response)]
            case _:
                reason = response.reason
                content = response.read()  # clears the response
                raise BadStatus(reason or content)

    def get_topics(self):
        self.conn.request("GET", "/news/topics", headers=self.headers)
        response = self.conn.getresponse()
        match http.client.responses[response.status]:
            case "OK":
                return [
                    (
                        tuple(topic_info["topic"]),
                        [self._parse_story(story_info) for story_info in topic_info["stories"]],
                    )
                    for topic_info in json.load(response)
                ]
            case _:
                print(response.read())  # clears the response
                raise BadStatus(response.reason)


def get_client():
    return closing(BiztocClient())
