#! /usr/bin/env python3
import gzip
import hashlib
import http.client
import json
import os
import re
import readline  # noqa: F401
import subprocess
import sys
import tempfile
from contextlib import closing
from datetime import datetime
from pathlib import Path
from subprocess import run
from typing import Iterable, Optional

import rich.repr
from rich.console import Console
from rich.prompt import Confirm, Prompt
from rich.rule import Rule

__version__ = "0.0.3"

api_key = os.getenv("KAGI_API_KEY")
DEFAULT_LOG_DIR = Path.home() / ".var/fastgpt"

error_console = Console(stderr=True)


def fzf(choices, args=("-m", "--tmux")) -> Optional[Iterable[bytes]]:
    assert not isinstance(args, str)
    proc = run(["fzf", *args, "--read0", "--print0"], input=b"\x00".join(choices), stdout=subprocess.PIPE)
    if proc.returncode == 0:
        return proc.stdout.strip(b"\x00").split(b"\x00")


class BadStatus(Exception):
    pass

class OutOfCredits(Exception):
    pass

@rich.repr.auto
class FastGPTRunner:
    def __init__(self):
        self.conn = http.client.HTTPSConnection("kagi.com")
        self.base_url = "/api/v0"
        self.meta = None
        self.usage = 0
        self.out_of_credits = False

    def post(self, url, content):
        if self.out_of_credits:
            raise OutOfCredits()
        with error_console.status(":robot: answering...") as spinner:
            if __debug__:
                spinner.stop()
            headers = {"Authorization": f"Bot {api_key}"} if api_key else {}
            try:
                self.conn.request(
                    "POST",
                    f"{self.base_url}/{url}",
                    body=json.dumps(content).encode(),
                    headers={
                        "Content-Type": "application/json",
                        **headers,
                    },
                )
                response = self.conn.getresponse()
            except http.client.RemoteDisconnected:
                error_console.log("Restarting connection...")
                self.conn.connect()
                return self.post(url=url, content=content)
        match http.client.responses[response.status]:
            case "OK":
                result = json.load(response)
                try:
                    self.meta = result.pop("meta")
                except Exception as e:
                    error_console.print_exception(e)
                try:
                    self.usage += int(result["data"].pop("tokens", 0))
                except Exception as e:
                    error_console.print_exception(e)
                return result
            case _:
                reason = response.reason
                text = response.read()  # clears the response
                if text:
                    content = json.loads(text)
                    errors = content.get("error")
                    if errors:
                        reason = ";".join(str(e["msg"]) for e in errors)
                        if "Sorry, no transcript could be found for this video." in reason:
                            return None
                        elif reason.startswith("Insufficient credit to perform this request."):
                            self.out_of_credits = True
                            raise OutOfCredits(reason)
                        else:
                            raise BadStatus(reason)

    def close(self):
        self.conn.close()
        if self.meta:
            error_console.log(f"Balance: {self.meta.get('api_balance')} Usage: {self.usage}")

    def ask(self, text):
        answer = self.post("fastgpt", content={"query": text})["data"]
        if answer["output"].startswith("I apologize"):
            excuse = answer.pop("output")
            error_console.log(f"Suppressed {excuse=}")
        references = answer.pop("references", ())
        return answer, references

    def summarize(self, url) -> Optional[str]:
        return self.post("summarize", content={"url": url})["data"]["output"]

    @classmethod
    def format_answer(cls, answer, references):
        answer_markdown = answer["output"]
        if references:
            for n, ref in enumerate(references, start=1):
                ref_number_str = f"【{n}】"  # unicode brackets
                answer_markdown = answer_markdown.replace(ref_number_str, f"[link={ref['url']}]{ref_number_str}[/link]")
        return answer_markdown


def get_client():
    global api_key
    assert api_key is not None, "Unauthorized"
    return closing(FastGPTRunner())


def main():
    started = datetime.now()
    log_dir = DEFAULT_LOG_DIR
    save_log = log_dir.exists()

    results = {}
    with get_client() as runner:
        if sys.stdin.isatty():
            log_path = Path(
                tempfile.mktemp(
                    prefix=f"FastGPT-{started:%Y-%m-%d-%H-%M-%S}",
                    suffix=".json.gz",
                    dir=log_dir,
                )
            )
            try:
                while question := Prompt.ask("Enter prompt").strip():
                    if question in ["q", ":q"]:
                        break
                    answer, references = runner.ask(question)
                    error_console.print(Rule(":robot:"))
                    paragraphs = re.split(r"\n{2,}", runner.format_answer(answer=answer, references=references))
                    for paragraph in paragraphs:
                        error_console.print(paragraph.strip())
                    error_console.print()
                    if references and Confirm.ask("Follow a reference?", choices="ynq"):
                        choices = [
                            f"{n: 2d}|{ref.get('title') or ''}|{ref.get('snippet') or ''}".encode()
                            for n, ref in enumerate(references, start=1)
                        ]
                        if (selected_lines := fzf(choices=choices, args=("-m", "--height=~8"))) is None:
                            continue
                        for selected in selected_lines:
                            ref_n = choices.index(selected)
                            ref = references[ref_n]
                            error_console.print(Rule(ref.get("title") or str(ref_n)))
                            try:
                                ref["summary"] = runner.summarize(ref.get("url"))
                                error_console.print(ref["summary"])
                            except BadStatus:
                                error_console.print("[red]Sorry, summarization failed[/]")
                    results[hashlib.new("MD5", question.encode()).hexdigest()] = {
                        "question": question,
                        "answer": answer,
                        "references": references,
                    }
            except EOFError:
                pass
            if save_log:
                log_dir.mkdir(parents=True, exist_ok=True)
                if results:
                    with gzip.open(log_path, mode="wt") as fo:
                        json.dump(results, fo)
                    error_console.log(f"Log saved to {log_path}")
        else:
            for question in sys.stdin.read().strip().split("\n\n"):
                if question.strip():
                    answer, references = runner.ask(question)
                    results[hashlib.new("MD5", question.encode()).hexdigest()] = {
                        "question": question,
                        "answer": answer["output"],
                        "references": references,
                    }
            print(json.dumps(results, indent=4))


if __name__ == "__main__":
    main()
