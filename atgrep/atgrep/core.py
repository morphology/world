from dataclasses import dataclass
from datetime import datetime, timedelta
import io
import pathlib
import shlex
import string
import subprocess

queue_codes = string.ascii_letters  # coincidentally


def batch(arg, batch_args=(), **kwargs):
    return at(arg, at_args=["-b", *batch_args], **kwargs)


@dataclass
class Job:
    number: int
    timestamp: datetime
    queue: str
    user: str

    @classmethod
    def from_line(cls, line):
        job_number, rest = line.split(None, 1)
        timestamp, queue, user = rest.rsplit(None, 2)
        return cls(
            number=int(job_number),
            timestamp=datetime.strptime(timestamp, "%a %b %d %H:%M:%S %Y"),
            queue=queue,
            user=user,
        )

    def get_commands(self):
        return at(at_args=("-c", str(self.number))).stdout

    def cancel(self):
        # TODO: isn't this -r?
        at(at_args=("-d", str(self.number))).check_returncode()

    def is_running(self):
        return self.queue == "="

    def is_batched(self):
        return self.queue == "b"


def list_jobs(queue=None):
    jobs = {}
    for line in at(at_args=["-l"], queue=queue).stdout.decode().splitlines():
        job = Job.from_line(line)
        jobs[job.number] = job
    return jobs


def at(execute=None, at_args=(), when=None, queue=None, **run_args):
    at_args = list(at_args)
    if when:
        if isinstance(when, timedelta):
            when = datetime.now() + when
        if isinstance(when, datetime):
            when = when.strftime("%Y%m%d%H%M.%S")
        at_args += ["-t", when]
    if queue:
        if isinstance(queue, int):
            queue = queue_codes[queue]
        at_args += ["-q", str(queue)]

    # TODO: use case for structured matching when we pass 3.9
    if isinstance(execute, pathlib.PurePath):
        execute_path = execute
        return subprocess.run(["at", *at_args, "-f", execute_path], stdout=subprocess.PIPE, **run_args)
    elif isinstance(execute, io.FileIO):
        execute_path = execute.name
        return subprocess.run(["at", *at_args, "-f", execute_path], stdout=subprocess.PIPE, **run_args)
    # Soak up the execute argument
    elif execute is None or isinstance(execute, (str, bytes)):
        execute_script = execute
        return subprocess.run(["at", *at_args], input=execute_script, stdout=subprocess.PIPE, **run_args)
    else:  # Assume a split line
        execute_script = " ".join(shlex.quote(str(a)) for a in execute)
        return subprocess.run(["at", *at_args], input=execute_script, stdout=subprocess.PIPE, **run_args)
