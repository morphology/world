from datetime import datetime
import subprocess

import click
import humanize
import terminaltables

from . import core


def show_jobs(queue=None):
    header = ["ID", "waiting for", "user"]
    rows = [(str(n), humanize.naturaldelta(datetime.now() - j.timestamp), j.user)
            for n, j in sorted(core.list_jobs(queue=queue).items())]
    table = terminaltables.SingleTable([header, *rows])
    print(table.table)

@click.command(context_settings=dict(
    ignore_unknown_options=True,
    allow_extra_args=True,
))
@click.pass_context
def grep(ctx):
    if ctx.args:
        for n, job in sorted(core.list_jobs().items()):
            subprocess.run([
                "grep", "-H", f"--label={job.number}", *ctx.args
            ], input=job.get_commands())
    else:
        show_jobs()


def main():
    grep(obj={})  # pylint: disable=no-value-for-parameter,unexpected-keyword-arg
