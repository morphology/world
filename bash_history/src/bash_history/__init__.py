from datetime import datetime, timedelta
from functools import total_ordering
import re


def parse_text(text):
    timestamp = None
    for line_no, line in enumerate(text.splitlines(), start=1):
        if m := re.compile(r"#(\d+)").match(line):
            timestamp = m.group(1)
        elif line.strip():
            yield line, datetime.fromtimestamp(
                float(timestamp)
            ) if timestamp else line_no
            timestamp = None
        # drop empty lines


def break_into_sections(entries, last_timestamp=datetime.now()):
    section_entries = []
    last_order = None
    for command, order in entries:
        is_begin = command.startswith("pushd") or command.startswith("@") or command.startswith("git switch") or (last_order and order < last_order)
        is_end = command == "exit" or command.startswith("popd")
        if command.startswith("popd -n") or command.startswith("pushd -n"):
            continue
        if is_begin:
            if section_entries:
                yield HistorySection(section_entries)
            section_entries = [(command, order)]
        elif is_end:
            if section_entries:
                section_entries.append((command, order))
                yield HistorySection(section_entries)
            section_entries = []
        else:
            section_entries.append((command, order))
        last_order = order
    if section_entries:
        yield HistorySection(section_entries, last_timestamp=last_timestamp)


@total_ordering
class HistorySection:
    def __init__(self, entries, last_timestamp=None):
        self.entries = entries
        self.last_timestamp = last_timestamp

    def get_timespan(self):
        if self.entries:
            return self.entries[0][1], self.entries[-1][1]
        else:
            return None, None

    def get_duration(self) -> timedelta:
        start, end = self.get_timespan()
        end = end or self.last_timestamp
        if isinstance(start, datetime) and isinstance(end, datetime):
            return end - start

    def get_lines(self):
        return [entry[0] for entry in self.entries]

    def __le__(self, other):
        lhs_start, _ = self.get_timespan()
        rhs_start, _ = other.get_timespan()
        return lhs_start <= rhs_start
