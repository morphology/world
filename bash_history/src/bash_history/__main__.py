import argparse
from datetime import datetime, timedelta
import os
from pathlib import Path
import re

from rich.console import Console
from rich.table import Table

from . import parse_text, break_into_sections


def _timestamp_arg(text):
    if m := re.compile(r"[-]?\d+").match(text):
        return timedelta(days=int(m.group(0)))
    return datetime.fromisoformat(text)


def _get_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("--since", type=_timestamp_arg)
    parser.add_argument(
        "history_files",
        nargs="*",
        type=Path,
        default=[Path(os.getenv("HISTFILE") or "~/.bash_history").expanduser()],
        help="You must export HISTFILE if customized from the default"
    )
    return parser


args = _get_parser().parse_args()
since = args.since
if since:
    if isinstance(since, timedelta):
        since = datetime.now() + since
sections_by_file = {}
for history_file in args.history_files:
    last_timestamp = datetime.fromtimestamp(history_file.stat().st_mtime)
    sections_by_file[history_file] = break_into_sections(
        parse_text(history_file.read_text()), last_timestamp=last_timestamp
    )

table = Table()

table.add_column("Date", justify="right", style="cyan", no_wrap=True)
table.add_column(str(since.date()) if since else "Till", style="magenta", no_wrap=True)
table.add_column("Hours", justify="right", style="green")
table.add_column("in", style="green")
table.add_column("out", style="green")

total = timedelta()
last_date = None
sections = sorted(section for sections in sections_by_file.values() for section in sections)
for section in sections:
    begin, end = section.get_timespan()
    if since is not None and begin < since:
        continue
    if lines := section.get_lines():
        total += section.get_duration()
        if abs(section.get_duration()) > timedelta(minutes=1):
            begin_s = str(begin.date())
            if last_date is not None and begin.date() == last_date:
                begin_s = " " * len(begin_s)
            end_s = (
                " " * len(begin_s) if begin.date() == end.date() else str(end.date())
            )
            hours_s = f"{section.get_duration().total_seconds() / 60 / 60: .2f}"
            table.add_row(begin_s, end_s, hours_s, lines[0], lines[-1])
        last_date = begin.date()
print("Total:", total)
table.title = f"Total: {total}"

console = Console()
console.print(table)
