"""
Print text found in images.
"""

import argparse
import sys
from pathlib import Path

import rich.console
from rich.text import Text

from . import tesseract
from ._version import __version__


def run_detect(args: argparse.Namespace, console: rich.console.Console):
    palette = [f"rgb(255,{y},0)" for y in range(255 - 150, 256, 3)] + [f"rgb(255,255,{b})" for b in range(5, 256, 5)]
    paths = args.image_paths
    if args.from_stdin:
        paths = [Path(line.rstrip("\n")) for line in sys.stdin]
    results = tesseract.ocr_many(paths)
    with console.pager(styles=bool(console.is_interactive)):
        for path, result in sorted(results.items()):
            if console.is_interactive:
                console.rule(path.name)
            else:
                console.print()
                console.print(path.name)
            for _, paragraph in result.paragraphs.items():
                for detections in paragraph:
                    words = []
                    if args.confidence is not None:
                        detections = [w for w in detections if w.attr["x_wconf"] >= args.confidence]
                    for word in detections:
                        if text := word.text.strip():
                            words.append(Text(text, style=palette[int(word.attr["x_wconf"])]))
                    if words:
                        console.print(" ", Text(" ").join(words))


def setup_parser(parser: argparse.ArgumentParser, command):
    match command:
        case None:
            # Common
            sg = parser.add_argument_group("Common to all functions")
            sg.add_argument("--version", action="version", version=f"%(prog)s {__version__}")
        case "detect":
            parser.add_argument(
                "--confidence", "-C", type=float, metavar="0-100", help="Ignore confidence lower than this"
            )
            parser.add_argument("image_paths", nargs="+" if sys.stdin.isatty() else "*", metavar="IMAGE", type=Path)
            parser.set_defaults(from_stdin=not sys.stdin.isatty())
        case _:
            raise NotImplementedError(command)
