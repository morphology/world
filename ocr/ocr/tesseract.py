import os
import re
import tempfile
import xml.etree.ElementTree as ET
from dataclasses import dataclass
from pathlib import Path
from subprocess import run
from typing import Iterable

TESSERACT = ["tesseract"]


@dataclass
class Rectangle:
    left_top: tuple[float, float]
    width: float
    height: float

    @property
    def box(self):
        left, top = self.left_top
        right = left + self.width
        bottom = top + self.height
        return (self.left_top, (right, bottom))


@dataclass
class ImageResult:
    attr: dict
    paragraphs: dict[int, list]


@dataclass
class WordResult:
    text: str
    attr: dict

    def __bool__(self):
        return len(self.text.strip()) > 0


def tesseract(
    input_arg: os.PathLike | Iterable,
    output_prefix: os.PathLike,
    tesseract_args: Iterable[str] = ("--psm", "11"),
    tesseract_configs: Iterable[str] = ("hocr",),
):
    if not isinstance(input_arg, os.PathLike):
        input_text = "\n".join(str(p) for p in input_arg)
        input_arg = "-"
    else:
        input_text = None
    command = [
        *TESSERACT,
        input_arg,
        output_prefix,
        *tesseract_args,
        *tesseract_configs,
    ]
    return run(
        command,
        text=True,
        input=input_text,
    )


def _parse_title(text):
    title_regex = re.compile(r';(?=(?:[^"]*"[^"]*")*[^"]*$)\s*')
    title_parts = []
    for part in title_regex.split(text):
        key, _, value = part.partition(" ")
        match key:
            case "image":
                value = Path(value.strip('"'))
            case "bbox":
                left, top, width, height = [float(x) for x in value.split()]
                value = Rectangle(
                    left_top=(left, top),
                    width=width,
                    height=height,
                )
            case "ppageno":
                value = int(value)
            case "x_wconf":
                value = float(value)
        title_parts.append((key, value))
    return title_parts


def ocr_many(paths: Iterable[os.PathLike]):
    paths = [Path(p) for p in paths]
    with tempfile.TemporaryDirectory() as td:
        td = Path(td)
        proc = tesseract(paths, td / "detections")
        assert proc.returncode == 0
        hocr_content = (td / "detections.hocr").read_text()
    root = ET.fromstring(hocr_content)
    (body,) = root.findall("./{http://www.w3.org/1999/xhtml}body")
    pages = body.findall("./*[@class='ocr_page']")
    assert len(paths) == len(pages), "Missing or multi-page image(s)"

    results = {}
    for page in pages:
        page_title_parts = dict(_parse_title(page.get("title")))
        path = page_title_parts.pop("image")
        image_result = results[path] = ImageResult(attr=page_title_parts, paragraphs={})
        for par_num, par in enumerate(page.findall(".//*[@class='ocr_par']"), start=1):
            par_result = image_result.paragraphs.setdefault(par_num, [])
            for line in par.findall(".//*[@class='ocr_line']"):
                par_result.append(
                    [
                        WordResult(
                            text=word.text,
                            attr=dict(_parse_title(word.get("title"))),
                        )
                        for word in line
                    ]
                )
    assert set(paths) == results.keys(), "Not all paths succeeded"
    return results
